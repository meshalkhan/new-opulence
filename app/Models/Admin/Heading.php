<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Heading extends Model
{
    use SoftDeletes;
    //
    protected $fillable = ['id',
        'name',
        'user_id',
        'created_by',
        'updated_by', 'deleted_by'];
    protected $table = 'headings';
}

