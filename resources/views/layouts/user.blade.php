<!doctype html>
<html lang="{{ app()->getLocale() }}">

@include('partials.head')


<body class="layout-top-nav sidebar-closed sidebar-collapse" height="auto">
<div class="wrapper" >
@include('partials.leftsidebar')
    <div class="content-wrapper" style="margin-top: -80px;min-height: 900px;background-color: #faf8ff;padding-left: 20px;padding-right: 20px;">
        @yield('content')
    </div>

</div>
@include('partials.script')
<script type="text/javascript">
    $(document).ready(function(){
        $('#datatable').DataTable()
    });
</script>
<script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/fullcalendar/2.2.7/fullcalendar.min.js"></script>
<script>
    @yield('pageScript')
</script>
<script src="https://www.gstatic.com/firebasejs/7.9.1/firebase-app.js"></script>
<script src="https://www.gstatic.com/firebasejs/7.9.1/firebase-messaging.js"></script>

<script>

    function sendTokenToServer(token){
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: 'home/admin_token/'+token,
            type: 'GET',
            success: function( data){
                console.log('data 1 : ', data);
            }
        });

    }
</script>
</body>

</html>
