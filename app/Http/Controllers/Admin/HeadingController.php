<?php


namespace App\Http\Controllers\Admin;

use App\Helpers\PermissionHelper as Per;
use App\Models\Admin\Heading;
use App\User;
use Illuminate\Http\Request;
use DB;
use Auth;
use Form;

class HeadingController
{
    public function index()
    {
        if (! Per::has_permission('heading_details')) {
            return redirect()->route('home');
        }

        return view('admin.heading.index');
    }

    public function create(Request $request)
    {
        if (! Per::has_permission('heading_create')) {
            return redirect()->route('home');
        }

        $name=$request->name;
        $heading = Heading::where('name',$name)->get();
        if(count($heading)>0)
        {
            session()->flash('success', 'Process Name Already Exists.');
            return redirect()->route('admin.heading.index');
        }
        else
        {
            $data = $request->all();
            $current_date_time = \Carbon\Carbon::now();
            $current_date_time->timezone = config('global.time_zone');
            $data['created_by'] = Auth::user()->id;
            $data['created_at'] = $current_date_time;
            Heading::create($data);
            session()->flash('success', 'Record has been created successfully.');
            return redirect()->route('admin.heading.index');
        }

    }

    public function fetchdata(Request $request)
    {
        $data = Heading::where('headingID',$request['headingID'])->get();
        return response()->json($data);
    }

    public function editrow(Request $request)
    {
        if (! Per::has_permission('heading_edit')) {
            return redirect()->route('home');
        }
        $data = $request->all();
        $headingID = $request['headingID'];
        $current_date_time = \Carbon\Carbon::now();
        $current_date_time->timezone = config('global.time_zone');
        $data['updated_by'] = Auth::user()->id;
        $data['updated_at'] = $current_date_time;
        $heading_name=$request->name;
        $headingcount = Heading::where('headingID','!=',$headingID)->where('name',$heading_name)->get();
        if(count($headingcount)>0)
        {
            $response='repeat';
        }
        else if($data['name']=='' )
        {
            $response='null';
        }
        else
        {
            $datas['updated_by'] = Auth::user()->id;
            Heading::where('headingID',$headingID)->update([
                'name'=>$request['name']
            ]);
            $response='success';

        }
        return response()->json($response);

    }

    public function deleterow(Request $request)
    {
        if (! Per::has_permission('heading_delete')) {
            return redirect()->route('home');
        }

        $headingID = $request->headingID;
        $headings = Heading::where('headingID',$headingID)->forceDelete();
    }

    public function allheading(Request $request,$headingID)
    {
        $columns = array(
            0 =>'headingID',
            1 =>'name',
            2=> 'options',
        );

        $totalData = Heading::count();

        $totalFiltered = $totalData;

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');

        if(empty($request->input('search.value')))
        {
            $lists = Heading::offset($start)
                ->limit($limit)
                ->orderBy('name','asc')
                ->get();
        }
        else
        {
            $search = $request->input('search.value');
            $lists =  Heading::where('name', 'LIKE',"%{$search}%")
                ->offset($start)
                ->limit($limit)
                ->orderBy($order,$dir)
                ->get();

            $totalFiltered = Heading::where('name', 'LIKE',"%{$search}%")
                ->count();
        }

        $data = array();

        $i=1;
        if(!empty($lists)) {
            foreach ($lists as $list) {


                $nestedData['headingID'] = "&nbsp&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;".$i;

                $nestedData['name'] = $list->name;
                $options="";
                if (Per::has_permission('heading_delete')) {

                    $options .= "<a class='btn btn-sm btn-danger' style='cursor:pointer;color:white;margin-left:2px;' onclick='rowdelete(\"" . $list->headingID . "\",\"0\")' ><i class='fas fa-trash-alt' ></i></button>";

                }
                if (Per::has_permission('heading_edit')) {
                    $options .= "<a class='btn btn-sm btn-warning' role='button' data-toggle='modal' data-target='#modal-heading' style='cursor:pointer;margin-left:2px;' onclick='editrow(\"" . $list->headingID . "\")' ><i class='fas fa-edit' ></i></button>";
                }

                $nestedData['options'] = $options;
                $data[] = $nestedData;
                $i++;

            }
        }

        $json_data = array(
            "draw"            => intval($request->input('draw')),
            "recordsTotal"    => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data"            => $data
        );
        echo json_encode($json_data);

    }
}
