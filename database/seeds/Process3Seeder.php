<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class Process3Seeder extends Seeder
{
    public function run()
    {
        $MainPermission = Permission::create([
            'title' => 'Process 3',
            'name' => 'process3_manage',
            'guard_name' => 'web',
            'main_group' => 1,
            'parent_id' => 0,
        ]);
        Permission::insert([
            [
                'title' => 'Create',
                'name' => 'process3_create',
                'guard_name' => 'web',
                'main_group' => 0,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
                'parent_id' => $MainPermission->id,
            ],
            [
                'title' => 'Edit',
                'name' => 'process3_edit',
                'guard_name' => 'web',
                'main_group' => 0,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
                'parent_id' => $MainPermission->id,
            ],


            [
                'title' => 'Details',
                'name' => 'process3_details',
                'guard_name' => 'web',
                'main_group' => 0,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
                'parent_id' => $MainPermission->id,
            ],

            [
                'title' => 'Delete',
                'name' => 'process3_delete',
                'guard_name' => 'web',
                'main_group' => 0,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
                'parent_id' => $MainPermission->id,
            ],

        ]);

        // Assign Permission to 'administrator' role
        $role = Role::findById(1, 'web');
        $role->givePermissionTo('process3_manage');
        $role->givePermissionTo('process3_create');
        $role->givePermissionTo('process3_edit');
        $role->givePermissionTo('process3_details');
        $role->givePermissionTo('process3_delete');

        \App\Models\Admin\Process3::insert([
            [
                'name' => 'Sketching',
                'nameKey' => 'Sketching',
                'code' => 'A'
               ,'user_id' => '1'],
            [
                'name' => 'CAD Design',
                'nameKey' => 'CAD Design',
                'code' => 'A'
               ,'user_id' => '1'],
            [
                'name' => 'WAX Printing',
                'nameKey' => 'WAX Printing',
                'code' => 'A'
               ,'user_id' => '1'],
            [
                'name' => 'Casting',
                'nameKey' => 'Casting',
                'code' => 'A'
               ,'user_id' => '1'],
            [
                'name' => 'Handmade',
                'nameKey' => 'Handmade',
                'code' => 'A'
               ,'user_id' => '1'],
            [
                'name' => 'Assembling',
                'nameKey' => 'Assembling',
                'code' => 'A'
               ,'user_id' => '1'],
            [
                'name' => 'Cleaning',
                'nameKey' => 'Cleaning',
                'code' => 'A'
               ,'user_id' => '1'],
            [
                'name' => 'Resizing',
                'nameKey' => 'Resizing',
                'code' => 'C'
               ,'user_id' => '1'],
            [
                'name' => 'Wedding Band Turning',
                'nameKey' => 'Wedding Band Turning',
                'code' => 'A'
               ,'user_id' => '1'],
            [
                'name' => 'Pre-Polishing',
                'nameKey' => 'Pre-Polishing',
                'code' => 'A'
               ,'user_id' => '1'],
            [
                'name' => 'Stone Setting',
                'nameKey' => 'Stone Setting',
                'code' => 'D'
               ,'user_id' => '1'],
            [
                'name' => '	Polishing',
                'nameKey' => '	Polishing',
                'code' => 'A'
               ,'user_id' => '1'],
            [
                'name' => '	Rhodium',
                'nameKey' => '	Rhodium',
                'code' => 'A'
               ,'user_id' => '1'],
            [
                'name' => '	Guiding',
                'nameKey' => '	Guiding',
                'code' => 'A'
               ,'user_id' => '1'],
            [
                'name' => 'Setting Style',
                'nameKey' => 'S Style',
                'code' => 'A'
               ,'user_id' => '1'],
            [
                'name' => 'Repairing',
                'nameKey' => 'Repairing',
                'code' => 'A'
               ,'user_id' => '1'],
            [
                'name' => 'Ring Size',
                'nameKey' => 'Ring Size',
                'code' => 'B'
                ,'user_id' => '1'],
            [
                'name' => 'Alteration',
                'nameKey' => 'Alteration',
                'code' => 'A'
                ,'user_id' => '1'],
            [
                'name' => '	Restoration',
                'nameKey' => '	Restoration',
                'code' => 'A'
                ,'user_id' => '1'],
            [
                'name' => '	Retip Claws',
                'nameKey' => '	Retip Claws',
                'code' => 'A'
                ,'user_id' => '1'],
            [
                'name' => '	Modification',
                'nameKey' => '	Modification',
                'code' => 'A'
                ,'user_id' => '1'],
            [
                'name' => 'Polish Touch Up',
                'nameKey' => 'Polish Touch Up',
                'code' => 'A'
                ,'user_id' => '1'],
            [
                'name' => 'Laser Soldering',
                'nameKey' => 'Laser Soldering',
                'code' => 'A'
                ,'user_id' => '1'],
            [
                'name' => 'Clasp',
                'nameKey' => 'Clasp',
                'code' => 'A'
                ,'user_id' => '1'],
            [
                'name' => '	Swap Stone',
                'nameKey' => '	Swap Stone',
                'code' => 'A'
                ,'user_id' => '1'],
            [
                'name' => 'Engraving',
                'nameKey' => 'Engraving',
                'code' => 'A'
                ,'user_id' => '1'],



        ]);
    }
}
