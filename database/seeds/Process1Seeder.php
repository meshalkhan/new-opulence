<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class Process1Seeder extends Seeder
{
    public function run()
    {
        $MainPermission = Permission::create([
            'title' => 'Process 1',
            'name' => 'process1_manage',
            'guard_name' => 'web',
            'main_group' => 1,
            'parent_id' => 0,
        ]);
        Permission::insert([
            [
                'title' => 'Create',
                'name' => 'process1_create',
                'guard_name' => 'web',
                'main_group' => 0,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
                'parent_id' => $MainPermission->id,
            ],
            [
                'title' => 'Edit',
                'name' => 'process1_edit',
                'guard_name' => 'web',
                'main_group' => 0,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
                'parent_id' => $MainPermission->id,
            ],


            [
                'title' => 'Details',
                'name' => 'process1_details',
                'guard_name' => 'web',
                'main_group' => 0,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
                'parent_id' => $MainPermission->id,
            ],

            [
                'title' => 'Delete',
                'name' => 'process1_delete',
                'guard_name' => 'web',
                'main_group' => 0,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
                'parent_id' => $MainPermission->id,
            ],

        ]);

        // Assign Permission to 'administrator' role
        $role = Role::findById(1, 'web');
        $role->givePermissionTo('process1_manage');
        $role->givePermissionTo('process1_create');
        $role->givePermissionTo('process1_edit');
        $role->givePermissionTo('process1_details');
        $role->givePermissionTo('process1_delete');

        \App\Models\Admin\Process1::insert([
            [
                'name' => '	Ring',
                'nameKey' => 'Ring'
               ,'user_id' => '1'],
            [
                'name' => '		Wedding Band',
                'nameKey' => '	Wedding Band'
               ,'user_id' => '1'],
            [
                'name' => '	Earrings',
                'nameKey' => 'Earrings'
               ,'user_id' => '1'],
            [
                'name' => '	Bracelet',
                'nameKey' => 'Bracelet'
               ,'user_id' => '1'],
            [
                'name' => '	Pendant',
                'nameKey' => 'Pendant'
               ,'user_id' => '1'],
            [
                'name' => '	Necklace',
                'nameKey' => 'Necklace'
               ,'user_id' => '1'],
            [
                'name' => '	Bangle',
                'nameKey' => 'Bangle'
               ,'user_id' => '1'],
            [
                'name' => 'Cuff Links',
                'nameKey' => '	Cuff Links'
               ,'user_id' => '1'],
            [
                'name' => '	Brooch',
                'nameKey' => 'Brooch'
               ,'user_id' => '1'],
            [
                'name' => 'Tiara',
                'nameKey' => 'Tiara'
               ,'user_id' => '1'],
            [
                'name' => '	Body Piercing Jewellery',
                'nameKey' => 'Body Piercing Jewellery'
               ,'user_id' => '1'],
            [
                'name' => '	Crowns',
                'nameKey' => 'Crowns'
               ,'user_id' => '1'],
            [
                'name' => '		Gospel Bracelet',
                'nameKey' => '	Gospel Bracelet'
               ,'user_id' => '1'],
            [
                'name' => 'Friendship Bracelet',
                'nameKey' => 'Friendship Bracelet'
               ,'user_id' => '1'],
            [
                'name' => 'Choker',
                'nameKey' => 'Choker'
               ,'user_id' => '1'],
            [
                'name' => 'Bolo Tie',
                'nameKey' => 'Bolo Tie'
               ,'user_id' => '1'],
            [
                'name' => 'Hatpin',
                'nameKey' => 'Hatpin'
               ,'user_id' => '1'],
            [
                'name' => 'Nose Pin',
                'nameKey' => 'Nose Pin'
               ,'user_id' => '1'],
            [
                'name' => 'Hairpin',
                'nameKey' => 'Hairpin'
               ,'user_id' => '1'],
            [
                'name' => 'Toe Ring',
                'nameKey' => 'Toe Ring'
               ,'user_id' => '1'],
            [
                'name' => 'Anklet',
                'nameKey' => 'Anklet'
               ,'user_id' => '1'],
            [
                'name' => 'Unknown',
                'nameKey' => 'Unknown'
               ,'user_id' => '1'],
            [
                'name' => 'Chain',
                'nameKey' => 'Chain'
               ,'user_id' => '1'],
            [
                'name' => 'Full Eternity',
                'nameKey' => 'Full Eternity'
               ,'user_id' => '1'],
            [
                'name' => 'Laser Engraving',
                'nameKey' => 'Laser Engraving'
               ,'user_id' => '1'],
        ]);
    }
}
