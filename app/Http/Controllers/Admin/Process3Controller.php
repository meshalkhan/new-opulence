<?php


namespace App\Http\Controllers\Admin;

use App\Helpers\PermissionHelper as Per;
use App\Models\Admin\Code;
use App\Models\Admin\Process3;
use App\User;
use Illuminate\Http\Request;
use DB;
use Auth;
use Form;

class Process3Controller
{
    public function index()
    {
        if (! Per::has_permission('process3_details')) {
            return redirect()->route('home');
        }

        return view('admin.process3.index');
    }

    public function create(Request $request)
    {
        if (! Per::has_permission('process3_create')) {
            return redirect()->route('home');
        }

        $name=$request->name;
        $process3 = Process3::where('name',$name)->get();
        if(count($process3)>0)
        {
            session()->flash('success', 'Process Name Already Exists.');
            return redirect()->route('admin.process3.index');
        }
        else
        {
            $data = $request->all();
            $current_date_time = \Carbon\Carbon::now();
            $current_date_time->timezone = config('global.time_zone');
            $data['created_by'] = Auth::user()->id;
            $data['user_id'] = Auth::user()->id;
            $data['created_at'] = $current_date_time;
            Process3::create($data);
            session()->flash('success', 'Record has been created successfully.');
            return redirect()->route('admin.process3.index');
        }

    }

    public function fetchdata(Request $request)
    {
        $data = Process3::where('id',$request['id'])->get();
        return response()->json($data);
    }

    public function editrow(Request $request)
    {
        if (! Per::has_permission('process3_edit')) {
            return redirect()->route('home');
        }
        $data = $request->all();
        $id = $request['id'];
        $current_date_time = \Carbon\Carbon::now();
        $current_date_time->timezone = config('global.time_zone');
        $data['updated_by'] = Auth::user()->id;
        $data['updated_at'] = $current_date_time;
        $process3_name=$request->name;
        $process3count = Process3::where('id','!=',$id)->where('name',$process3_name)->get();
        if(count($process3count)>0)
        {
            $response='repeat';
        }
        else if($data['name']=='' || $data['nameKey']=='' || $data['code']=='' )
        {
            $response='null';
        }
        else
        {
            $datas['updated_by'] = Auth::user()->id;
            Process3::where('id',$id)->update([
                'name'=>$request['name'],
                'nameKey'=>$request['nameKey'],
                'code'=>$request['code']
            ]);
            $response='success';

        }
        return response()->json($response);

    }

    public function deleterow(Request $request)
    {
        if (! Per::has_permission('process3_delete')) {
            return redirect()->route('home');
        }

        $id = $request->id;
        $process3s = Process3::where('id',$id)->forceDelete();
    }

    public function allprocess3(Request $request,$id)
    {
        $columns = array(
            0 =>'id',
            1 =>'name',
            2 =>'nameKey',
            3 =>'code',
            4=> 'options',
        );

        $totalData = Process3::count();

        $totalFiltered = $totalData;

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');

        if(empty($request->input('search.value')))
        {
            $lists = Process3::offset($start)
                ->limit($limit)
                ->orderBy('name','asc')
                ->get();
        }
        else
        {
            $search = $request->input('search.value');
            $lists =  Process3::where('name', 'LIKE',"%{$search}%")
                ->offset($start)
                ->limit($limit)
                ->orderBy($order,$dir)
                ->get();

            $totalFiltered = Process3::where('name', 'LIKE',"%{$search}%")
                ->count();
        }

        $data = array();

        $i=1;
        if(!empty($lists)) {
            foreach ($lists as $list) {


                $nestedData['id'] = "&nbsp&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;".$i;

                $nestedData['name'] = $list->name;
                $nestedData['nameKey'] = $list->nameKey;
                $nestedData['code'] = $list->code;
                $options="";
                if (Per::has_permission('process3_delete')) {

                    $options .= "<a class='btn btn-sm btn-danger' style='cursor:pointer;color:white;margin-left:2px;' onclick='rowdelete(\"" . $list->id . "\",\"0\")' ><i class='fas fa-trash-alt' ></i></button>";

                }
                if (Per::has_permission('process3_edit')) {
                    $options .= "<a class='btn btn-sm btn-warning' role='button' data-toggle='modal' data-target='#modal-process3' style='cursor:pointer;margin-left:2px;' onclick='editrow(\"" . $list->id . "\")' ><i class='fas fa-edit' ></i></button>";
                }

                $nestedData['options'] = $options;
                $data[] = $nestedData;
                $i++;

            }
        }

        $json_data = array(
            "draw"            => intval($request->input('draw')),
            "recordsTotal"    => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data"            => $data
        );
        echo json_encode($json_data);

    }
}
