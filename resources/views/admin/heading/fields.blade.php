<div class="form-group  col-md-5 @if($errors->has('name')) has-error @endif">
    {!! Form::label('name', 'Name *', ['class' => 'control-label']) !!}
    {!! Form::text('name', old('name'), ['class' => 'form-control form-control-sm',  'placeholder' => 'Enter Name','required'=>'true','maxlength' =>100]) !!}
    {!! Form::text('update_id', null, ['class' => 'form-control form-control-sm', 'id'=>'update_id','hidden' => 'true']) !!}
    @if($errors->has('name'))
        <span class="help-block">
                                        {{ $errors->first('name') }}
                                    </span>
    @endif

</div>
