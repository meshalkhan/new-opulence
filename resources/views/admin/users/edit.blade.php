@extends('layouts.admin')

@section('content')
<!-- Content Header (Page header) -->
<section class="content-header" style="height: 50px;margin-left:30px ; ">
   
     @if($message = Session::get('success'))
       <div class="toast alert-info" style="padding: 3px;text-align: center;">{{ Session::get('success') }}</div>
    @endif
 
</section>

<!-- Main content -->
<section class="content">
    <div class="col-md-12">
        <!-- Horizontal Form -->
        <div class="card card-info">
            <div class="card-header">
            <h3 class="card-title">Update User</h3>
            </div>
            <!-- /.card-header -->
            <!-- form start -->
            {!! Form::model($user, ['method' => 'PUT', 'route' => ['admin.users.update', $user->id], 'data-parsley-validate']) !!}
                <div class="card-body">
                    @include('admin.users.fields')
                </div>
                <!-- /.card-body -->
                <div class="card-footer">
                    {!! Form::submit(trans('global.app_save'), ['class' => 'btn btn-outline-danger']) !!}
                    <a href="{{route('admin.users.index')}}" class="btn btn-outline-secondary float-right">{{trans('global.app_back')}}</a>
                </div>
                <!-- /.card-footer -->
            {!! Form::close() !!}
        </div>
        <!-- /.card -->
    </div>       
</section>
    <!-- /.content -->

@endsection