<?php


namespace App\Http\Controllers\Admin;

use App\Helpers\PermissionHelper as Per;
use App\Models\Admin\Heading;
use App\Models\Admin\SubHeading;
use App\User;
use Illuminate\Http\Request;
use DB;
use Auth;
use Form;

class SubHeadingController
{
    public function index()
    {
        if (! Per::has_permission('subheading_details')) {
            return redirect()->route('home');
        }

        $heading= Heading::get()->pluck('name','name');
        return view('admin.subheading.index',compact('heading'));
    }

    public function create(Request $request)
    {
        if (! Per::has_permission('subheading_create')) {
            return redirect()->route('home');
        }

        $name=$request->name;
        $subheading = SubHeading::where('name',$name)->where('heading',$request->heading)->get();
        if(count($subheading)>0)
        {
            session()->flash('success', 'Sub Heading with Associated Heading Already Exists.');
            return redirect()->route('admin.subheading.index');
        }
        else
        {
            $data = $request->all();
            $current_date_time = \Carbon\Carbon::now();
            $current_date_time->timezone = config('global.time_zone');
            $data['created_by'] = Auth::user()->id;
            $data['created_at'] = $current_date_time;
            SubHeading::create($data);
            session()->flash('success', 'Record has been created successfully.');
            return redirect()->route('admin.subheading.index');
        }

    }

    public function fetchdata(Request $request)
    {
        $data = SubHeading::where('subHeadingID',$request['subHeadingID'])->get();
        return response()->json($data);
    }

    public function editrow(Request $request)
    {
        if (! Per::has_permission('subheading_edit')) {
            return redirect()->route('home');
        }
        $data = $request->all();
        $subHeadingid = $request['subHeadingID'];
        $current_date_time = \Carbon\Carbon::now();
        $current_date_time->timezone = config('global.time_zone');
        $data['updated_by'] = Auth::user()->id;
        $data['updated_at'] = $current_date_time;
        $subheading_name=$request->name;
        $subheadingcount = SubHeading::where('subHeadingID','!=',$subHeadingid)->where('heading',$data['heading'])->where('name',$subheading_name)->get();
        if(count($subheadingcount)>0)
        {
            $response='repeat';
        }
        else if($data['name']=='' || $data['heading']=='' )
        {
            $response='null';
        }
        else
        {
            $datas['updated_by'] = Auth::user()->id;
            SubHeading::where('subHeadingID',$subHeadingid)->update([
                'name'=>$request['name'],
                'heading'=>$request['heading']
            ]);
            $response='success';

        }
        return response()->json($response);

    }

    public function deleterow(Request $request)
    {
        if (! Per::has_permission('subheading_delete')) {
            return redirect()->route('home');
        }

        $subHeading = $request->subHeadingID;
        $subheadings = SubHeading::where('subHeadingID',$subHeading)->forceDelete();
    }

    public function allsubheading(Request $request,$subHeading)
    {
        $columns = array(
            0 =>'subHeadingID',
            1 =>'name',
            2 =>'heading',
            3=> 'options',
        );

        $totalData = SubHeading::count();

        $totalFiltered = $totalData;

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');

        if(empty($request->input('search.value')))
        {
            $lists = SubHeading::offset($start)
                ->limit($limit)
                ->orderBy('name','asc')
                ->get();
        }
        else
        {
            $search = $request->input('search.value');
            $lists =  SubHeading::where('name', 'LIKE',"%{$search}%")
                ->offset($start)
                ->limit($limit)
                ->orderBy($order,$dir)
                ->get();

            $totalFiltered = SubHeading::where('name', 'LIKE',"%{$search}%")
                ->count();
        }

        $data = array();

        $i=1;
        if(!empty($lists)) {
            foreach ($lists as $list) {


                $nestedData['subHeadingID'] = "&nbsp&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;".$i;

                $nestedData['name'] = $list->name;
                $nestedData['heading'] = $list->heading;
                $options="";
                if (Per::has_permission('subheading_delete')) {

                    $options .= "<a class='btn btn-sm btn-danger' style='cursor:pointer;color:white;margin-left:2px;' onclick='rowdelete(\"" . $list->subHeadingID . "\",\"0\")' ><i class='fas fa-trash-alt' ></i></button>";

                }
                if (Per::has_permission('subheading_edit')) {
                    $options .= "<a class='btn btn-sm btn-warning' role='button' data-toggle='modal' data-target='#modal-subheading' style='cursor:pointer;margin-left:2px;' onclick='editrow(\"" . $list->subHeadingID . "\")' ><i class='fas fa-edit' ></i></button>";
                }

                $nestedData['options'] = $options;
                $data[] = $nestedData;
                $i++;

            }
        }

        $json_data = array(
            "draw"            => intval($request->input('draw')),
            "recordsTotal"    => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data"            => $data
        );
        echo json_encode($json_data);

    }
}
