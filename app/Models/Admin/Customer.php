<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Customer extends Model
{
    use SoftDeletes;
    //
    protected $fillable = ['id',
        'nickName',
        'firstName',
        'lastName',
        'companyName',
        'vatNumber',
        'email',
        'password',
        'phone',
        'mobile',
        'address',
        'creditLimit',
        'user_id',
        'created_by',
        'updated_by', 'deleted_by'];
    protected $table = 'customers';
}

