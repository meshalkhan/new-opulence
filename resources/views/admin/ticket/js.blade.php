<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/3/jquery.inputmask.bundle.js"></script>
<script type="text/javascript">

    $(".select2").select2({
        //placeholder: "Select a Name",
        allowClear: true
    });
</script>
<script type="text/javascript">

    $(document).ready(function ()
    {

        var table= $('#ticket_datatables').DataTable({
            "pageLength":10,
            "processing": true,
            "serverSide": true,
            "ajax":{
                "type": "GET",
                "url": "{{ route('admin.ticket.allticket',['archive']) }}'",
                "dataType": "json",
                "data":{ _token: "{{csrf_token()}}"}

            },
            "columnDefs": [{
                orderable: false,
                "targets": '_all',
                "createdCell": function (td, cellData, rowData, row, col) {
                    $(td).css('padding-top', '2px');
                    $(td).css('padding-left', '8px');
                    $(td).css('padding-bottom', '2px');
                    $(td).css('padding-right', '8px')
                }
            }],
            "columns": [
                {
                    "data": "barcode"
                },
                { "data": "customerID" },
                { "data": "image" },
                { "data": "total_amount" },
                { "data": "status" },
                { "data": "paidStatus" },
                { "data": "paidDue" },
                { "data": "complete_job" },
                { "data": "pay_now" },
                {
                    "data": "options"
                }
            ],
            "order": [[1, 'asc']],
        });
    });

    // Delete row

    var rowdelete = function (id,find)
    {
        if(find=='0')
        {
            var title='You will not be able to revert this !';

        }
        swal({
            title: "Are you sure?",
            text: title,
            icon: "warning",
            buttons: [
                'No, cancel it!',
                'Yes, I am sure!'
            ],
            dangerMode: true,
        }).then(function(isConfirm) {
            if (isConfirm) {

                $.ajax({
                    type: 'POST',
                    url: '{{ route('admin.ticket.deleterow') }}',
                    data: {
                        _token: '{{ csrf_token() }}',
                        id: id,
                    },
                    success: function (response) {
                        swal({
                            title: 'Deleted!',
                            text: 'Record has been Deleted Successfully.',
                            icon: 'success',
                            timer: 2000,
                            button: false,
                        }).then(function() {
                        });
                        clearall();
                    }
                });
            } else {
                swal("Cancelled", "Your data is safe :)", "error");
            }
        });
    };

    // Edit Row
    var updaterow = function ()
    {
        var id = $('#update_id').val();


        $.ajax({
            type: 'POST',
            url: '{{ route("admin.ticket.editrow") }}',
            data: {
                _token: '{{ csrf_token() }}',
                id: id
            },

            success: function (response)
            {
                if(response=='success')
                {
                    swal({
                        title: 'Data Updated',
                        type: 'success',
                        text: 'Record has been Updated Successfully',
                        icon: 'success',
                        timer: 2000,
                        button: false,
                    }).then(
                        function () {},
                        // handling the promise rejection
                        function (dismiss) {
                            if (dismiss === 'timer') {
                            }
                        }
                    )
                }
                else if(response=='repeat')
                {
                    var title='Email Already Exists.!';
                    swal("Type Again", title, "error");
                }
                else if(response=='null')
                {
                    var title='Some Fields Empty. Try Again!';
                    swal("Type Again", title, "error");
                }


                $('#ticket_datatables').DataTable().ajax.reload();
            }
        });
    };

    var editrow = function (id) {
        $('#save').attr('disabled', true);

        $.ajax({
            type: 'POST',
            url: '{{ route('admin.ticket.fetchdata') }}',
            data: {
                _token: '{{ csrf_token() }}',
                id: id,
            },

            success: function (response) {
                jQuery('html,body').animate({scrollTop:0},0);
                document.getElementById('add_ticket').style.display = "none";
                document.getElementById('edit_ticket').style.display = "block";
                document.getElementById('clearall').style.display = "block";
                $('#update_id').val(response[0].id);
//                $('#nickName').val(response[0].nickName);


            }
        });

    };

    var status_filter= function ()
    {
        $('#ticket_datatables').DataTable().ajax.reload();
    }

</script>
