<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">


    <title>Opulence Jewellery Services </title>
    <link rel="icon" href="{{ asset('public/uploads') }}/icon.png" type="image/icon type">
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />


    <link rel="stylesheet" href="{{ asset('public/AdminLTE/plugins/fontawesome-free/css') }}/all.min.css">
    <link rel="stylesheet" href="{{ asset('public/css') }}/parsley.css">
    <link rel="stylesheet" href="{{ asset('public/AdminLTE/plugins/overlayScrollbars/css') }}/OverlayScrollbars.min.css">
    <link rel="stylesheet" href="{{ asset('public/AdminLTE/dist/css') }}/adminlte.min.css">
    <link rel="stylesheet" href="{{ asset('public/AdminLTE/plugins/datatables') }}/dataTables.bootstrap4.css">
    <link rel="stylesheet" href="{{ asset('public/AdminLTE/plugins/daterangepicker') }}/daterangepicker.css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/2.1.4/toastr.min.css">

    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/fullcalendar/2.2.7/fullcalendar.min.css"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/fullcalendar/2.2.7/fullcalendar.min.js"></script>
    <link href="https://cdn.datatables.net/1.10.24/css/jquery.dataTables.min.css"/>
    <link href="https://cdn.datatables.net/buttons/1.7.0/css/buttons.dataTables.min.css"/>
    <script type="text/javascript" src="http://code.jquery.com/jquery-1.11.0.min.js"></script>



    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.js"></script>
    <script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
    <script src="https://code.jquery.com/ui/1.9.2/jquery-ui.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    <script src="http://parsleyjs.org/dist/parsley.js"></script>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.js"></script>


    <script src="{{ asset('public/AdminLTE/plugins/daterangepicker') }}/daterangepicker.js"></script>
    <script src="//code.jquery.com/jquery-1.10.2.js"></script>
    <script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>

    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.10.0/css/bootstrap-select.min.css" rel="stylesheet" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.6/js/standalone/selectize.min.js" integrity="sha256-+C0A5Ilqmu4QcSPxrlGpaZxJ04VjsRjKu+G82kl5UJk=" crossorigin="anonymous"></script>
    <link href='//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css' rel='stylesheet' type='text/css'>
    <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
    <script src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js"></script>
    <script src="https://markcell.github.io/jquery-tabledit/assets/js/tabledit.min.js"></script>

    <link href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" rel="stylesheet" />

    <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>

    <script src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>

    <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.flash.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>


    <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js"></script>

    <script>
    $(function() {
        $(".timedatepicker").timepicker({});
    });
    </script>

    <script>
    $(function() {
        $('#mess').hide();
        $(".datepicker").inputmask({"mask": "99/99/9999"});
        $(".phone").inputmask({"mask": "99 99 99 99 99"});
        $(".mobile").inputmask({"mask": "99 99 99 99 99"});
        $(".datepicker").datepicker({});
    });
    </script>

    <script type="text/javascript">

      $(document).ready(function(){
          $('#mess').hide();
          $(".toast").toast({ delay: 3000 });
            $('.toast').toast('show');

    });
    </script>


    <style>

        .swal-footer {
            text-align: center;
        }

        input:required {
            box-shadow: 1px 1px 5px rgba(151, 159, 161, 0.85);
        }

        select:required {
            box-shadow: 1px 1px 5px rgba(151, 159, 161, 0.85);
        }

        div.gfg {
            height: 390px;
            overflow: auto;
            text-align:justify;
        }

        td.details-control {
            cursor: pointer;
        }
        tr.shown td.details-control {
            cursor: pointer;
        }

        .headingrow {
            margin-top: 2%;
            background-color: rgba(138,120,189,0.81);
            color:white;
            font-size:14px;
            padding-left: 20px;
            padding-top: 5px;
            padding-bottom: 5px;
            margin-bottom: 2%;
        }

        .upload-btn-wrappers {
            position: relative;
            overflow: hidden;
            display: inline-block;
        }

        .btns {
            border: 1px solid gray;
            color: gray;
            background-color: white;
            padding: 8px 20px;
            border-radius: 8px;
            font-size: 12px;
            font-weight: bold;
        }

        .upload-btn-wrappers input[type=file] {
            font-size: 100px;
            position: absolute;
            left: 0;
            top: 0;
            opacity: 0;
        }

        .border {
            border:1px solid #E1E1E1;
            height: 25px;
        }

        * {
            box-sizing: border-box;
        }

        img {
            vertical-align: middle;
        }

        .opacity:hover  {
            background-color: #0a1520;
        }


        .container {
            position: relative;
        }
        .mySlides {
            display: none;
        }

        .mySlides1 {
            display: none;
        }
        .mySlides2 {
            display: none;
        }
        .mySlides3 {
            display: none;
        }
        .mySlides4 {
            display: none;
        }


        .cursor {
            cursor: pointer;
        }

        .prevv,
        .nextt {
            cursor: pointer;
            position: absolute;
            top: 40%;
            width: auto;
            padding: 16px;
            margin-top: -50px;
            color: white;
            font-weight: bold;
            font-size: 20px;
            border-radius: 0 3px 3px 0;
            user-select: none;
            -webkit-user-select: none;
        }


        .nextt {
            right: 0;
            border-radius: 3px 0 0 3px;
        }

        .prevv:hover,
        .nextt:hover {
            background-color: rgba(65, 149, 149, 0.8);
        }

        /* Number text (1/3 etc) */
        .numbertext {
            color: #f2f2f2;
            font-size: 12px;
            padding: 8px 12px;
            position: absolute;
            top: 0;
        }
        .numbertext1 {
            color: #f2f2f2;
            font-size: 12px;
            padding: 8px 12px;
            position: absolute;
            top: 0;
        }
        .numbertext2 {
            color: #f2f2f2;
            font-size: 12px;
            padding: 8px 12px;
            position: absolute;
            top: 0;
        }
        .numbertext3 {
            color: #f2f2f2;
            font-size: 12px;
            padding: 8px 12px;
            position: absolute;
            top: 0;
        }
        .numbertext4 {
            color: #f2f2f2;
            font-size: 12px;
            padding: 8px 12px;
            position: absolute;
            top: 0;
        }
        .row:after {
            content: "";
            display: table;
            clear: both;
        }

        .row1:after {
            content: "";
            display: table;
            clear: both;
        }
        .row2:after {
            content: "";
            display: table;
            clear: both;
        }
        .row3:after {
            content: "";
            display: table;
            clear: both;
        }
        .row4:after {
            content: "";
            display: table;
            clear: both;
        }

        .column {
            float: left;
            width: 16.66%;
        }

        .demo {
            opacity: 0.6;
        }

        .active,
        .demo:hover {
            opacity: 1;
        }
        .demo1 {
            opacity: 0.6;
        }

        .active,
        .demo1:hover {
            opacity: 1;
        }

        .demo2 {
            opacity: 0.6;
        }

        .active,
        .demo2:hover {
            opacity: 1;
        }

        .demo3 {
            opacity: 0.6;
        }

        .active,
        .demo3:hover {
            opacity: 1;
        }

        .demo4 {
            opacity: 0.6;
        }

        .active,
        .demo4:hover {
            opacity: 1;
        }

        .custom-file-input {
            color: transparent;
        }
        .custom-file-input::-webkit-file-upload-button {
            visibility: hidden;
        }
        .custom-file-input::before {
            content: 'Select some files';
            color: black;
            display: inline-block;
            background: -webkit-linear-gradient(top, #f9f9f9, #e3e3e3);
            border: 1px solid #999;
            border-radius: 3px;
            padding: 5px 8px;
            outline: none;
            white-space: nowrap;
            -webkit-user-select: none;
            cursor: pointer;
            text-shadow: 1px 1px #fff;
            font-weight: 700;
            font-size: 10pt;
        }
        .custom-file-input:hover::before {
            border-color: black;
        }
        .custom-file-input:active {
            outline: 0;
        }
        .custom-file-input:active::before {
            background: -webkit-linear-gradient(top, #e3e3e3, #f9f9f9);
        }

        .table {
            font-family: Arial, Helvetica, sans-serif;
            font-size: 13px;
            border-collapse: collapse;
            width: 100%;
        }

        .table td, #customers th {
            border: 1px solid #ddd;
            padding: 4px;
            padding-left:10px ;
        }

        .table tr:nth-child(even){background-color: #f2f2f2;}

        .table th {
            padding-top: 4px;
            padding-bottom: 4px;
            text-align: left;
            background-color: #a0acaf;
            color: white;
        }


    </style>

  </head>
