<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\User;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('user_index');
    }

    public function admin_index()
    {
        return view('index');
    }

    public function admin_token($token){
//        dd('ok');
        $user_id = Auth::user()->id;
        User::where('id', $user_id)->update(['device_token' => $token ]);
        return response()->json( ['result' => true] );
    }

}



