<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Process3 extends Model
{
    use SoftDeletes;
    //
    protected $fillable = ['id',
        'name',
        'nameKey',
        'code',
        'user_id',
        'created_by',
        'updated_by', 'deleted_by'];
    protected $table = 'process3';
}

