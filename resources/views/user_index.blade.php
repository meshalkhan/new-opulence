@inject('request', 'Illuminate\Http\Request')
<?php $per = PermissionHelper::getUserPermissions();?>
@extends('layouts.user')

@section('content')

    <?php

    $customer = \App\Models\Admin\Customer::get();
    $customercount=count($customer);

    ?>

    <section class="content-header" style="height: 50px;margin-left:30px ; ">
    </section>

    <section class="content">
        <div class="card card-secondary">

            <div class="card-header">
                <h3 style="font-weight: bold;font-size: 16px;" class="card-title"> SCAN TICKET</h3>
            </div>

            <div class="card-body">
                <div class="row">
                    <div class="col-12 col-sm-6 col-md-4">
                        <a href="{{route('admin.ticket.add')}}">
                            <div class="small-box" style="background-image:url({{url('images/2-back.png')}});background-repeat: no-repeat;background-attachment: fixed;">
                                <div class="inner">
                                    <h3 style="padding-left: 12px; color:#0e5457;font-weight: bold">N/A</h3>
                                    <p style="padding-left: 12px;color:#0e5457;font-weight: bold">Add New Ticket</p>
                                </div>
                                <div class="icon">
                                    <i class=""></i>
                                </div>

                            </div>
                        </a>
                    </div>
                    <div class="col-12 col-sm-6 col-md-4">
                        <a href="{{ route('admin.customer.index') }}">
                            <div class="small-box" style="background-image:url({{url('images/4-back.png')}});">
                                <div class="inner">
                                    <h3 style="padding-left: 12px; color:#0e5457;font-weight: bold">N/A</h3>
                                    <p style="padding-left: 12px;color:#0e5457;font-weight: bold">Add New Customer</p>
                                </div>
                                <div class="icon">
                                    <i class=""></i>
                                </div>

                            </div>
                        </a>
                    </div>
                    <div class="col-12 col-sm-6 col-md-4">
                        <a href="{{ route('admin.ticket.index') }}">
                            <div class="small-box" style="background-image:url({{url('images/2-back.png')}});">
                                <div class="inner">
                                    <h3 style="padding-left: 12px; color:#0e5457;font-weight: bold">N/A</h3>
                                    <p style="padding-left: 12px;color:#0e5457;font-weight: bold">Create Invoice</p>
                                </div>
                                <div class="icon">
                                    <i class=""></i>
                                </div>

                            </div>
                        </a>
                    </div>

                </div>
                <div class="row">
                    <div class="col-md-6" style="text-align:center;padding-top: 70px;">
                        <h2>
                            High Quality Casting Services
                        </h2>
                        <p style="align-items: center;text-align:center;">
                            Opulence Jewellery Services provides high
                            quality jewellery casting services for both custom designs and multiple orders,
                            including settings and other components for Jewellery.

                        </p>

                    </div>
                    <div class="col-md-6" >
                        <img src="{{url('images/1-back.png')}}" style="color:white;width: 100%;height: 80%">
                    </div>
                </div>



            </div>
        </div>
    </section>
    <br/>
@endsection

