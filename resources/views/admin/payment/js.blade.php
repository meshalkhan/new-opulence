<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script type="text/javascript">

    $(".select2").select2({
        //placeholder: "Select a Name",
        allowClear: true
    });


</script>
<script type="text/javascript">

    $(document).ready(function ()
    {
        document.getElementById('add_payment').style.display = "block";
        document.getElementById('edit_payment').style.display = "none";
        $('#clearall').show();

        var table= $('#payment_datatables').DataTable({
            "pageLength": 25,
            "processing": true,
            "serverSide": true,
            "ajax":{
                "type": "GET",
                "url": "{{ route('admin.payment.allpayment',['archive']) }}'",
                "dataType": "json",
                "data":{ _token: "{{csrf_token()}}"}

            },
            "columnDefs": [{
                orderable: false,
                "targets": '_all',
                "createdCell": function (td, cellData, rowData, row, col) {
                    $(td).css('padding-top', '2px')
                    $(td).css('padding-left', '8px')
                    $(td).css('padding-bottom', '2px')
                    $(td).css('padding-right', '8px')
                }
            }],

            "columns": [
                {
                    "className":      'details-control',
                    "data": "paymentID"
                },
                { "data": "reference" },
                { "data": "heading" },
                { "data": "subHeading" },
                { "data": "rate" },
                { "data": "quantity" },
                { "data": "amount" },
                { "data": "payment_option" },
                {
                    "data": "options"
                },
            ],
            "order": [[1, 'asc']]

        });
    });

    var clearall = function ()
    {

        $('#save').attr('disabled', false);
        document.getElementById('add_payment').style.display = "block";
        $('#clearall').show();
        document.getElementById('edit_payment').style.display = "none";
        $('#payment_datatables').DataTable().ajax.reload();
        $('#heading').val('');
        $('#subHeading').val('');
        $('#reference').val('');
        $('#rate').val('');
        $('#quantity').val('');
        $('#amount').val('');
        $('#payment_option').val('');
    };

    // Delete row

    var rowdelete = function (paymentID,find)
    {
        if(find=='0')
        {
            var title='You will not be able to revert this !';

        }
        swal({
            title: "Are you sure?",
            text: title,
            icon: "warning",
            buttons: [
                'No, cancel it!',
                'Yes, I am sure!'
            ],
            dangerMode: true,
        }).then(function(isConfirm) {
            if (isConfirm) {

                $.ajax({
                    type: 'POST',
                    url: '{{ route('admin.payment.deleterow') }}',
                    data: {
                        _token: '{{ csrf_token() }}',
                        paymentID: paymentID,
                    },
                    success: function (response) {
                        swal({
                            title: 'Deleted!',
                            text: 'Record has been Deleted Successfully.',
                            icon: 'success',
                            timer: 2000,
                            button: false,
                        }).then(function() {
                        });
                        clearall();
                    }
                });
            } else {
                swal("Cancelled", "Your data is safe :)", "error");
            }
        });
    };

    // Edit Row
    var updaterow = function ()
    {

        var paymentID = $('#update_id').val();
        var heading=$('#heading').val();
        var subHeading=$('#subHeading').val();
        var reference=$('#reference').val();
        var rate=$('#rate').val();
        var quantity=$('#quantity').val();
        var amount=$('#amount').val();
        var payment_option=$('#payment_option').val();

        $.ajax({
            type: 'POST',
            url: '{{ route("admin.payment.editrow") }}',
            data: {
                _token: '{{ csrf_token() }}',
                paymentID: paymentID,
                reference: reference,
                heading: heading,
                subHeading: subHeading,
                rate: rate,
                quantity: quantity,
                amount: amount,
                payment_option: payment_option
            },

            success: function (response)
            {
                if(response=='success')
                {
                    swal({
                        title: 'Data Updated',
                        type: 'success',
                        text: 'Record has been Updated Successfully',
                        icon: 'success',
                        timer: 2000,
                        button: false,
                    }).then(
                        function () {},
                        // handling the promise rejection
                        function (dismiss) {
                            if (dismiss === 'timer') {
                            }
                        }
                    )
                }
                else if(response=='repeat')
                {
                    var title='Process Name Already Exists.!';
                    swal("Type Again", title, "error");
                }
                else if(response=='null')
                {
                    var title='Some Fields Empty. Try Again!';
                    swal("Type Again", title, "error");
                }


                $('#payment_datatables').DataTable().ajax.reload();
            }
        });
    };

    var editrow = function (paymentID) {
        $('#save').attr('disabled', true);

        $.ajax({
            type: 'POST',
            url: '{{ route('admin.payment.fetchdata') }}',
            data: {
                _token: '{{ csrf_token() }}',
                paymentID: paymentID,
            },

            success: function (response) {
                jQuery('html,body').animate({scrollTop:0},0);
                document.getElementById('add_payment').style.display = "none";
                document.getElementById('edit_payment').style.display = "block";
                document.getElementById('clearall').style.display = "block";
                $('#update_id').val(response[0].paymentID);
                $('#heading').val(response[0].heading);
                $('#subHeading').val(response[0].subHeading);
                $('#reference').val(response[0].reference);
                $('#rate').val(response[0].rate);
                $('#quantity').val(response[0].quantity);
                $('#amount').val(response[0].amount);
                $('#payment_option').val(response[0].payment_option);

            }
        });

    };

    var status_filter= function ()
    {
        $('#payment_datatables').DataTable().ajax.reload();
    }

</script>
