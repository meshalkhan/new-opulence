@inject('request', 'Illuminate\Http\Request')


<?php $per = PermissionHelper::getUserPermissions();
?>

<nav class="main-header navbar navbar-expand-md navbar-light navbar-white" style="background-image:url({{url('images/nav-back.png')}});;background-repeat: no-repeat;background-attachment: fixed;background-size: cover;z-index: auto;height: 300px;margin-top: -90px">
    <div class="container" >
        <a >
            <img src="{{ asset('public/uploads') }}/icon.png" alt="Opulence Jewellery Services"
                 style="width: 10%;" ;  >
            <span class="brand-text"  style="color: white;font-weight: bold;font-size: 18px;">Opulence Jewellery Service</span>
        </a>

        <button style="color: white;background-color: white;margin-top: 5px;" class="navbar-toggler order-1" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse order-3" id="navbarCollapse" style="margin-top: 10px">
            <ul class="navbar-nav">

                <li>
                    <a class="btn btn-sm btn-secondary" style="background-color:#404040;width:100px;color:white;margin-right: 10px;font-weight: bold;" href="{{ route('admin.home') }}">Home</a>
                </li>
                @if(in_array('ticket_manage',$per) )
                <li>
                    <a href="{{ route('admin.ticket.index') }}" class="btn btn-sm btn-secondary" style="background-color:#404040;width:100px;font-weight: bold;margin-right: 10px" >Ticket</a>
                </li>
                @endif
                @if(in_array('customer_manage',$per) )
                <li>
                    <a href="{{ route('admin.customer.index') }}" class="btn btn-sm btn-secondary" style="background-color:#404040;width:100px;color:white;font-weight: bold;margin-right: 10px" >Customers</a>
                </li>
                @endif
                @if(in_array('users_manage',$per) )
                    <li>
                        <a class="btn btn-sm btn-secondary" style="background-color:#404040;width:100px;color:white;margin-right: 10px;font-weight: bold;" href="{{ route('admin.admin.home') }}">Dashboard</a>
                    </li>

                @endif
            </ul>
        </div>
    </div>
</nav>
