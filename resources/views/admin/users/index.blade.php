@inject('request', 'Illuminate\Http\Request')
<?php $per = PermissionHelper::getUserPermissions();?>
@extends('layouts.admin')

@section('content')
<section class="content-header" style="height: 50px;margin-left:30px ; ">

     @if($message = Session::get('success'))
       <div class="toast alert-info" style="padding: 3px;text-align: center;">{{ Session::get('success') }}</div>
    @endif

</section>

<!-- Main content -->
<section class="content">
   <div class="card card-secondary" style="overflow-x:auto;">
            <div class="card-header">
              <h3 style="font-weight: bold;font-size: 16px;" class="card-title">Users Management</h3>
              <div class="card-tools">
                  @if(in_array('users_create',$per))
                <a style="color: white;cursor: pointer;background-color: #069ba9;border:1px solid #069ba9 " class="btn btn-sm"  href="{{route('admin.users.create')}}" class="btn btn-danger btn-sm pull-right">New User</a>
                  @endif
              </div>
            </div>
            <!-- /.card-header -->
            <div class="card-body" style="overflow-x:auto;font-size: 15px;">
              <table id="example1" class="display dataTable" style="width:100%;font-size: 13px;" >
                <thead>
                <tr>
                  <th>Name</th>
                  <th>Email</th>
                  <th>Roles</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                    @if (count($users) > 0)
                        @foreach ($users as $user)
                            <tr data-entry-id="{{ $user->id }}">
                                <td>{{ $user->name }}</td>
                                <td>{{ $user->email }}</td>
                                <?php
                                $rname = \Spatie\Permission\Models\Role::where('id',$user->role_id)->first();
                                ?>
                                <td>{{$rname['name']}}</td>
                                <td>
                                    @if(in_array('users_create',$per))
                                        <a href="{{ route('admin.users.edit',[$user->id]) }}" class="btn btn-sm btn-secondary"><i class="fas fa-edit"></i></a>
                                    @endif
                                    @if(in_array('users_destroy',$per))
                                        {!! Form::open(array(
                                        'style' => 'display: inline-block;',
                                        'method' => 'DELETE',
                                        'onsubmit' => "return confirm('".trans("global.app_are_you_sure")."');",
                                        'route' => ['admin.users.destroy', $user->id])) !!}
                                    {!! Form::button('<i class="fas fa-trash"></i>', array('type'=>'submit','class' => 'btn btn-sm btn-danger')) !!}
                                    {!! Form::close() !!}
                                    @endif
                                </td>

                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="9">@lang('global.app_no_entries_in_table')</td>
                        </tr>
                    @endif
                </tbody>
              </table>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
</section>
    <!-- /.content -->

@endsection
@section('jsscript')

{{-- <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script> --}}
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>

@endsection
