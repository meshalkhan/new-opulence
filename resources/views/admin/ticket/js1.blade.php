<script>
    $("#selectedCustomer").select2({
        placeholder: "Select a customer"
    });

    var tableData = [];

    var selectedProcess1ID=0;
    var selectedProcess1Name="";

    var selectedProcess2ID=0;
    var selectedProcess2Name="";
    var selectedProcess2NameKey="";

    var selectedProcess3 = [];

    var selectedProcess4 = [];

    var description1 = "";
    var description2 = "";
    var description3 = "";
    var description4 = "";

    var editIndex = -1;

    function process1Select(selector,processID,name) {
        $(".process1-boxes").css('background-color', 'white');
        $(".process1-boxes").css('color', '#6c757d');
        selector.addClass("btn-secondary");
        selector.css('color', 'white');
        selector.css('background-color', '#6c757d');

        $(".process2-boxes").css( 'pointer-events', 'all' );

        selectedProcess1ID = processID;
        selectedProcess1Name = name;

        description1 =selectedProcess1Name.trim()+"\n";
        $("#description").val(description1);
    }

    function process2Select(selector,processID,name,nameKey) {
        $(".process2-boxes").css('background-color', 'white');
        $(".process2-boxes").css('color', '#6c757d');
        selector.addClass("btn-secondary");
        selector.css('color', 'white');
        selector.css('background-color', '#6c757d');

        $(".process3-boxes").css( 'pointer-events', 'all' );

        selectedProcess2ID = processID;
        selectedProcess2Name = name;
        selectedProcess2NameKey = nameKey;

        description2 =selectedProcess2Name.trim()+"\n";
        $("#description").val(description1+description2);
    }

    function process3Select(selector,processID,name) {

        $(".process4-boxes").css( 'pointer-events', 'all' );

        var checkIfExsist = false;
        for (var i =0; i < selectedProcess3.length; i++) {
            if (selectedProcess3[i].id === processID) {
                selectedProcess3.splice(i, 1);
                checkIfExsist = true
            }
        }

        if(!checkIfExsist){
            selectedProcess3.push({
                id: processID,
                name: name
            })
            selector.css('color', 'white');
            selector.css('background-color', '#6c757d');
        }else{
            selector.css('color', '#6c757d');
            selector.css('background-color', 'white');
        }

        description3 = "";
        for (var i=0;i<selectedProcess3.length;i++){
            if(i==0){
                description3 +=selectedProcess3[i].name.trim()+" ";
            }else{
                description3 +=", "+selectedProcess3[i].name.trim();
            }
        }
        description3 += "\n";
        $("#description").val(description1+description2+description3+description4);
    }

    function process4Select(selector,processID,name) {
        $(".process4-boxes").addClass("bg-box");

        var checkIfExsist = false;
        for (var i =0; i < selectedProcess4.length; i++) {
            if (selectedProcess4[i].id === processID) {
                selectedProcess4.splice(i, 1);
                checkIfExsist = true
            }
        }

        if(!checkIfExsist){
            selectedProcess4.push({
                id: processID,
                name: name
            });
            selector.css('color', 'white');
            selector.css('background-color', '#6c757d');
        }else{
            selector.css('color', '#6c757d');
            selector.css('background-color', 'white');
        }

        description4 = "";
        for (var i=0;i<selectedProcess4.length;i++){
            if(i==0){
                description4 +=selectedProcess4[i].name.trim()+" ";
            }else{
                description4 +=", "+selectedProcess4[i].name.trim();
            }
        }
        description4 += "\n";
        $("#description").val(description1+description2+description3+description4);
    }

    $(document).ready(function() {

        var selectedCustomerID = parseInt($("#selectedCustomerID").html());
        if(selectedCustomerID){
            $("#selectedCustomer").val(selectedCustomerID).select2();
        }

        $("#showTicketItemsForPrint").attr('hidden',true);

        $('#kt_datetimepicker_2_modal').datetimepicker({
            todayHighlight: true,
            autoclose: true,
            pickerPosition: 'bottom-left',
            format: 'yyyy-mm-dd hh:ii:ss'
        });

        $("form[id='ticketForm']").validate({
            rules: {
                selectedCustomer: {
                    required: true
                },
                description: "required",
                note: "required",
                reference: {
                    required: true
                },
                qunatity: {
                    required: true
                }
            },

            messages: {
                selectedCustomer: "Please select customer",
                description: "Please enter your description",
                note: "Please enter your note",
                reference: "Please enter your reference #",
                qunatity: "Please enter your quantity"

            },
            submitHandler: function () {

                if(editIndex > -1){
                    tableData[editIndex].description=$("#description").val()
                    tableData[editIndex].note=$("#note").val()
                    tableData[editIndex].reference=$("#reference").val()
                    tableData[editIndex].qunatity=$("#qunatity").val()
                    tableData[editIndex].total=$("#total").val()
                    tableData[editIndex].src=$("#pic").val()

                    editIndex =-1;
                }else{
                    tableData.push({
                        "description": $("#description").val(),
                        "note": $("#note").val(),
                        "reference": $("#reference").val(),
                        "qunatity": $("#qunatity").val(),
                        "total": $("#total").val(),
                        "src": $("#pic").val()

                    })
                }

                $("#description").val('')
                $("#note").val('')
                $("#reference").val('')
                $("#qunatity").val('')
                $("#total").val('')
                $("#pic").val('')

                $(".process1-boxes").removeClass("bg-active");
                $(".process2-boxes").removeClass("bg-active");
                $(".process3-boxes").removeClass("bg-active");
                $(".process4-boxes").removeClass("bg-active");

                populateTableData()

            }

        })

        $("#printDataButton").on('click',function (e) {
            e.preventDefault();

            $("#htmlDataForPrint").css('width', '160mm');
            $("#htmlDataForPrint table tr td").css('height', '20px');
            $("#htmlDataForPrint").css('position', 'relative');
            $("#htmlDataForPrint").css('left', '-20px');
            $("#htmlDataForPrint").css('background', 'white');

            $("#kt_header").css('display', 'none');
            $("#kt_header_mobile").css('display', 'none');
            $("#kt_subheader").css('display', 'none');
            $(".card-header").css('display', 'none');
            $(".card-footer").css('display', 'none');
            $(".card-body").css('overflow', 'hidden');
            $("#kt_footer")[0].style.setProperty('display', 'none', 'important');

            window.print();

            $("#htmlDataForPrint").css('width', 'auto');
            $("#htmlDataForPrint").css('height', 'auto');
            $("#htmlDataForPrint").css('position', 'unset');
            $("#htmlDataForPrint").css('left', 'unset');
            $("#htmlDataForPrint").css('background', 'none');

            $("#kt_header").css('display', 'block');
            $("#kt_header_mobile").css('display', '123');
            $("#kt_subheader").css('display', 'block');
            $(".card-header").css('display', 'block');
            $(".card-footer").css('display', 'block');
            $(".card-body").css('overflow', 'auto');
            $("#kt_footer")[0].style.setProperty('display', 'block', 'important');

        })
    });

    function addTicket(){
        var customerID = $("#selectedCustomer").val()
        if(customerID){
            if(tableData.length){

                var totalAmount = 0;
                for (var i=0;i<tableData.length;i++){
                    totalAmount = totalAmount + parseFloat(tableData[i].total);
                }
                $.ajax({
                    type: 'POST',
                    url: './php/ticket.php?function=addTicket',
                    data: {
                        "customerID": customerID,
                        "items": tableData,
                        "endDate": $("#kt_datetimepicker_2_modal").val(),
                        "price": totalAmount

                    },
                    dataType: 'json',
                    success: function (data) {
                        if (data) {

                            $("#barCodeDiv").html(data.barCodeGenerated)

                            //table.ajax.reload();
                            showTicketItems(data.barCodeStr,data.barCodeGenerated,data.customer,data.endDate)

                        } else {
                            toastr.error('Something went wrong try again');
                        }
                    },
                    error: function (data) {
                        console.log(data);
                    }
                });
            }else{
                toastr.error('Please add atleast one item');
            }
        }else{
            toastr.error('Please select a customer');
        }
    }

    function showTicketItems(barCodeStr,barCodeGenerated,customer,endDate) {
        $("#customerFirstName").html(customer.firstName)
        $("#barCodeGenerated").html(barCodeGenerated)
        $("#showbarCode").html(barCodeStr)
        $("#showEndDate").html(endDate)
        populateTableDataForPrint()
        //$("#showTicketItemsForPrint").modal('show')
        $("#showTicketItemsForPrint").attr('hidden',false);
        $("#addTicketContainer").attr('hidden',true);
        $("#showTicketContainer").attr('hidden',true);
    }

    function populateTableDataForPrint(){
        var result = "";
        if(tableData.length){
            var totalAmount = 0;
            var totalQuantity = 0;
            // $("#customerReferenceNumber").html(tableData[0].reference)
            for (var i=0;i<tableData.length;i++){
                result+="<tr><td style=\"font-size:20px;\" colspan=\"4\"><b>Customer Reference No:</b> "+tableData[i].reference+"</td></tr>";
                result+="<tr>";
                result+="<td>"+tableData[i].description+"<p> <b>Notes:</b> "+tableData[i].note+"</p></td>";
                result+="<td>"+tableData[i].qunatity+"</td>";
                result+="<td>"+tableData[i].total+"</td>";
                result+="<td><img id=\"image\" alt=\"\" src='"+tableData[i].src+"' style=\"height: 100px;\"></td>";
                result+="</tr>";
                totalAmount = totalAmount + parseFloat(tableData[i].total);
                totalQuantity = totalQuantity + parseFloat(tableData[i].qunatity);
            }
            result+="<tr>";
            result+="<th>Total</th>";
            result+="<th>"+totalQuantity+"</th>";
            result+="<th>"+totalAmount+"</th>";
            result+="<th></th>";
            result+="</tr>";
            $("#tableDataForPrint").html(result);
        }
    }

    function populateTableData(){
        var result = "";
        if(tableData.length){
            for (var i=0;i<tableData.length;i++){
                result+="<tr>";
                result+="<td>"+tableData[i].description+"</td>";
                result+="<td>"+tableData[i].note+"</td>";
                result+="<td>"+tableData[i].reference+"</td>";
                result+="<td>"+tableData[i].qunatity+"</td>";
                result+="<td>"+tableData[i].total+"</td>";
                result+="<td><img id=\"image\" alt=\"\" src='"+tableData[i].src+"' style=\"height: 100px;\"></td>";
                result+="<td><i class='fa fa-edit text-success mr-2' onclick='editRecord("+i+")'></i><i class='fa fa-trash text-danger' onclick='deleteRecord("+i+")'></i></td>";
                result+="</tr>";
            }

            $("#tableData").html(result);
        }
    }

    function deleteRecord(index) {

        tableData.splice(index, 1);

        populateTableData()
    }

    function editRecord(index) {

        editIndex = index;

        $("#description").val(tableData[index].description)
        $("#note").val(tableData[index].note)
        $("#reference").val(tableData[index].reference)
        $("#qunatity").val(tableData[index].qunatity)
        $("#total").val(tableData[index].total)
        $("#image").attr('src',tableData[index].src)

        $(".process1-boxes").removeClass("bg-active");
        $(".process2-boxes").removeClass("bg-active");
        $(".process3-boxes").removeClass("bg-active");
        $(".process4-boxes").removeClass("bg-active");

        //tableData.splice(index, 1);

        //populateTableData()
    }

</script>