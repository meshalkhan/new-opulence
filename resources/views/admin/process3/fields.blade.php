<div class="form-group  col-md-4 @if($errors->has('name')) has-error @endif">
    {!! Form::label('name', 'Name *', ['class' => 'control-label']) !!}
    {!! Form::text('name', old('name'), ['class' => 'form-control form-control-sm',  'placeholder' => 'Enter Name','required'=>'true','maxlength' =>100]) !!}
    {!! Form::text('update_id', null, ['class' => 'form-control form-control-sm', 'id'=>'update_id','hidden' => 'true']) !!}
    @if($errors->has('name'))
        <span class="help-block">
                                        {{ $errors->first('name') }}
                                    </span>
    @endif

</div>

<div class="form-group  col-md-4 @if($errors->has('nameKey')) has-error @endif">
    {!! Form::label('nameKey', 'Short Name *', ['class' => 'control-label']) !!}
    {!! Form::text('nameKey', old('nameKey'), ['class' => 'form-control form-control-sm',  'placeholder' => 'Enter Short Name','required'=>'true','maxlength' =>100]) !!}
    @if($errors->has('nameKey'))
        <span class="help-block">
                                        {{ $errors->first('nameKey') }}
                                    </span>
    @endif

</div>

<div class="form-group  col-md-4 @if($errors->has('code')) has-error @endif">
    {!! Form::label('code', 'Code *', ['class' => 'control-label']) !!}
    {!! Form::select('code', config('status.code'),old('code'), ['class' => 'form-control form-control-sm',  'placeholder' => 'Select Code','required'=>'true','maxlength' =>100]) !!}
    @if($errors->has('code'))
        <span class="help-block">
                                        {{ $errors->first('code') }}
                                    </span>
    @endif

</div>
