<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

//Route::post('login', 'API\UserController@login');
//Route::post('register', 'API\UserController@register');
//Route::post('register/user', 'API\UserController@registerUser');
//Route::post('forgot', 'API\UserController@forgot_pass');
//Route::post('update_pass', 'API\UserController@update_pass');


//    Route::put('update/profile/{id}', 'API\UserController@updateProfile');
//    Route::post('logout', 'API\UserController@logout');
//    Route::get('contract/index', 'API\ContractController@index');


    Route::post('/register', 'API\ApiController@register');
    Route::post('/login', 'API\ApiController@login');

    Route::group(['middleware' => 'auth:api'], function(){

        Route::post('/logout', 'API\ApiController@logout');
        Route::get('/contractindex', 'API\ApiController@contractindex');
        Route::post('/contractstore', 'API\ApiController@contractstore');
        Route::post('contractupdate/{id}', 'API\ApiController@contractupdate');

        Route::get('buyerorderview/{id}', 'API\ApiController@buyerorderview');
        Route::get('buyerorderactive/{id}', 'API\ApiController@buyerorderactive');
        Route::get('buyerorderinactive/{id}', 'API\ApiController@buyerorderinactive');


});
