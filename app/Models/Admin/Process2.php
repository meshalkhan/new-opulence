<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Process2 extends Model
{
    use SoftDeletes;
    //
    protected $fillable = ['id',
        'name',
        'nameKey',
        'metal_type',
        'unit',
        'per_gram',
        'labour_charges',
        'created_by',
        'updated_by', 'deleted_by'];
    protected $table = 'process2';
}

