<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class PricingSeeder extends Seeder
{
    public function run()
    {
        $MainPermission = Permission::create([
            'title' => 'Metal Price',
            'name' => 'pricing_manage',
            'guard_name' => 'web',
            'main_group' => 1,
            'parent_id' => 0,
        ]);
        Permission::insert([
            [
                'title' => 'Calculate Gold Price',
                'name' => 'calculate_gold_price',
                'guard_name' => 'web',
                'main_group' => 0,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
                'parent_id' => $MainPermission->id,
            ],
            [
                'title' => 'Calculate Silver Price',
                'name' => 'calculate_silver_price',
                'guard_name' => 'web',
                'main_group' => 0,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
                'parent_id' => $MainPermission->id,
            ],
            [
                'title' => 'Calculate Platinum Price',
                'name' => 'calculate_platinum_price',
                'guard_name' => 'web',
                'main_group' => 0,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
                'parent_id' => $MainPermission->id,
            ],

        ]);

        // Assign Permission to 'administrator' role
        $role = Role::findById(1, 'web');
        $role->givePermissionTo('pricing_manage');
        $role->givePermissionTo('calculate_gold_price');
        $role->givePermissionTo('calculate_silver_price');
        $role->givePermissionTo('calculate_platinum_price');

        \App\Models\Admin\MetalPrice::insert([
            [
                'goldPrice' => '0',
                'silverPrice' => '0',
                'platinumPrice' => '0',
                'user_id' => '1',
            ]
        ]);
    }
}
