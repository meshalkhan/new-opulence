<!DOCTYPE html>
<html lang="en">
<head>
    <title>Admin Panel</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!--===============================================================================================-->
{{--<link rel="icon" type="image/png" href="images/tax-time.png"/>--}}
<!--===============================================================================================-->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    <link rel="stylesheet" type="text/css" href="fonts/iconic/css/material-design-iconic-font.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="css/util.css">
    <link rel="stylesheet" type="text/css" href="css/main.css">
    <!--===============================================================================================-->
    <style>

        .container-login10 {

            width: 100%;
            display: -webkit-box;
            display: -webkit-flex;
            display: -moz-box;
            display: -ms-flexbox;
            display: flex;
            flex-wrap: wrap;
            justify-content: center;
            align-items: center;
            background-position: center;
            background-size: cover;
            position: relative;

        }
    </style>
</head>

<body style="background-image:url({{url('images/back-wall.jpg')}});background-repeat: no-repeat; background-attachment: fixed;background-size: cover;">

<br/>
<br/>
<div class="container" style="margin-top:100px;margin-left:80px;">
    <div class="row">
        <div class="col-md-3">
            <div style="opacity:0.98;background-color: white;width:450px;padding:20px;border-radius: 5%;">
                {{--@csrf--}}

                {!! Form::open(['method' => 'POST', 'route' => ['logins.store'], 'data-parsley-validate']) !!}
                <span class="login100-form-logo" style="width:100%" >
						 <img src="{{ asset('public/uploads') }}/tax-time.png" style="border-radius:2%; border:1px solid white;width:240px;margin-left:10px">
					</span>

                <div style="text-align: center">
                <span class=" p-b-33 p-t-20" style="color:#263C5B;font-size: 13px;text-align: center;float: inside">
                    <b>Sign In To Opulence Jewellery Services</b>
                    <br/>
                    <br/>

					</span>
                </div>
                @error('password')
                <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                @enderror

                <div class="wrap-input100 validate-input" style="background-color:#e3cb8f;" data-validate = "Invalid cnic or Password">
                    {{--<input class="input100 @error('cnic') is-invalid @enderror" type="cnic" name="cnic" value="{{ old('cnic') }}" placeholder="cnic Address">--}}
                    {!! Form::text('email', old('email'), ['class' => 'input100', 'placeholder' => 'Enter Email']) !!}
                    <span class="focus-input100" data-placeholder="&#xf207;"></span>
                    @error('cnic')
                    <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                    @enderror
                </div>

                <div class="wrap-input100" style="background-color:#e3cb8f;">
                    {{--<input class="input100 @error('password') is-invalid @enderror" type="password" name="password" required placeholder="Password" autocomplete="current-password">--}}
                    {!! Form::input('password','password', old('password'), ['class' => 'input100 ', 'placeholder' => 'Password']) !!}
                    <span class="focus-input100" data-placeholder="&#xf191;"></span>

                </div>

                {{--                <div class="contact100-form-checkbox">--}}
                {{--                    <input class="input-checkbox100" id="remember" type="checkbox" name="remember-me">--}}
                {{--                    <label class="label-checkbox100" style="color:#61728B;" for="remember" {{ old('remember') ? 'checked' : '' }}>--}}
                {{--                        Remember me--}}
                {{--                    </label>--}}
                {{--                </div>--}}

                <div class="container-login100-form-btn" style="">
                    {!! Form::submit('Login', ['class' => 'login100-form-btn', 'style'=>'color:white;']) !!}
                </div>
            <!--               @if (Route::has('password.request'))-->
                <!--<div class="text-center p-t-90">-->
            <!--	<a class="txt1" href="{{ route('password.request') }}">-->
                <!--		Forgot Password?-->
                <!--	</a>-->
                <!--               </div>-->
                <!--               @endif-->
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
</body>
</html>