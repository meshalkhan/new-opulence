@extends('layouts.admin')
@inject('request', 'Illuminate\Http\Request')
<?php $per = PermissionHelper::getUserPermissions();?>
@section('content')

    <?php

    $user_id = \App\User::orderBy('id','desc')->where('id',Auth::user()->id)->first();
    $role_id = \Spatie\Permission\Models\Role::orderBy('id','desc')->where('id',$user_id['role_id'])->first();
    $user = \App\User::orderBy('id','desc')->get();
    $usercount=count($user);

    $process1 = \App\Models\Admin\Process1::get();
    $process1count=count($process1);

    $process2 = \App\Models\Admin\Process2::get();
    $process2count=count($process2);

    $process3 = \App\Models\Admin\Process3::get();
    $process3count=count($process3);

    $process4 = \App\Models\Admin\Process4::get();
    $process4count=count($process4);

    $ticket = \App\Models\Admin\Ticket::get();
    $ticketcount=count($ticket);

    $customer = \App\Models\Admin\Customer::get();
    $customercount=count($customer);

            ?>

    <div class="content-header">
    </div>

        <section class="content">
            <div class="row">

                <section class="col-md-6 connectedSortable">
                    <div class="card card-widget widget-user">
                        <div class="widget-user-header text-white"
                             style="background-image:url({{url('images/qww.jpg')}})">
                            <h3 class="widget-user-username text-right"></h3>
                            <h5 class="widget-user-desc text-right" style="color: #0c5460">{{$role_id['name']}}</h5>
                        </div>
                        <div class="widget-user-image">
                                <img class="img-circle elevation-2" src="{{URL::asset('public/uploads/user-pic.png')}}">

                        </div>
                        <div class="card-footer">
                            <div class="row">
                                <div class="col-sm-6 border-right">
                                    <div class="description-block">
                                        <h5 class="description-header">{{$user_id['name']}}  </h5>
                                        <span class="description-text"> Name</span>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="description-block">
                                        <h5 class="description-header">{{$user_id['email']}}</h5>
                                        <span class="description-text">Email</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

                <div class="col-md-6">
                    <div class="card-danger">

                        <div class="card-header" style="background-color:#e7f0e6;;">
                            <h3 class="card-title" style="color: #0c5460"><b>Welcome To Your Profile</b> </h3>
                        </div>

                        <div class="card card-body">
                            <div class="row">
                                <div class="col-12 col-sm-6 col-md-3">
                                    <a href="{{ route('admin.users.index') }}">
                                        <div class="small-box" style="background-color:#734bbb">
                                            <div class="inner">
                                                <h3 style="padding-left: 12px; color:white">@if(isset($process1count)){{$process1count}} @else N/A @endif</h3>
                                                <p style="padding-left: 12px;color:white">Process 1</p>
                                            </div>
                                            <div class="icon">
                                                <i class=""></i>
                                            </div>

                                        </div>
                                    </a>
                                </div>

                                <div class="col-12 col-sm-6 col-md-3">
                                    <a href="">
                                        <div class="small-box bg-warning">
                                            <div class="inner">
                                                <h3 style="padding-left: 12px; color:white">@if(isset($process2count)){{$process2count}} @else N/A @endif</h3>
                                                <p>Process 2</p>
                                            </div>
                                            <div class="icon">
                                                <i class=""></i>
                                            </div>

                                        </div>
                                    </a>
                                </div>
                                <div class="col-12 col-sm-6 col-md-3">
                                    <a href="">
                                        <div class="small-box bg-success">
                                            <div class="inner">
                                                <h3 style="padding-left: 12px; color:white">@if(isset($process3count)){{$process3count}} @else N/A @endif</h3>
                                                <p>Process 3</p>
                                            </div>
                                            <div class="icon">
                                                <i class=""></i>
                                            </div>

                                        </div>
                                    </a>
                                </div>
                                <div class="col-12 col-sm-6 col-md-3">
                                    <a href="">
                                        <div class="small-box bg-danger">
                                            <div class="inner">
                                                <h3 style="padding-left: 12px; color:white">@if(isset($process4count)){{$process4count}} @else N/A @endif</h3>
                                                <p>Process 4</p>
                                            </div>
                                            <div class="icon">
                                                <i class=""></i>
                                            </div>

                                        </div>
                                    </a>
                                </div>
                            </div>

                            <br/>
                        </div>
                    </div>

                </div>
            </div>
            <div class="row">

                <div class="col-12 col-sm-6 col-md-4">
                    <div class="info-box mb-3">
                        <span class="info-box-icon bg-danger elevation-1"><i class="fas fa-user-check"></i></span>

                        <div class="info-box-content">
                            <span class="info-box-text">Users</span>
                            <span class="info-box-number">@if(isset($usercount)){{$usercount}} @else N/A @endif</span>
                    </div>
                </div>
                </div>
                <div class="clearfix hidden-md-up"></div>

                <div class="col-12 col-sm-6 col-md-4">
                    <div class="info-box mb-3">
                        <span class="info-box-icon bg-success elevation-1"><i class="fas fa-user-friends"></i></span>

                        <div class="info-box-content">
                            <span class="info-box-text">Customers</span>
                            <span class="info-box-number">@if(isset($customercount)){{$ticketcount}} @else N/A @endif</span>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4">
                    <div class="info-box">
                        <span class="info-box-icon bg-danger elevation-1"><i class="fas fa-calendar"></i></span>

                        <div class="info-box-content">
                            <span class="info-box-text">Tickets</span>
                            <span class="info-box-number">
                  @if(isset($ticketcount)){{$ticketcount}} @else N/A @endif
                  <small></small>
                </span>
                        </div>
                    </div>
                </div>
            </div>
        </section>

    <section class="content">
        <div class="container-fluid">

        </div>
    </section>

    <br/>
@endsection
