<?php

namespace App\Http\Controllers;

use App\User;
use Auth;
use Lang;
use Illuminate\Http\Request;

class LoginController
{
    public function index()
    {
        return view('auth.loginagain');
    }

    public function store(Request $request)
    {
        $data = $request->all();
        $email=$request->email;
        $email=trim( $email );

//        dd($data);


            if(Auth::attempt([ 'email'=> $email, 'password'=> $request->password]))
            {
                $user=User::where('email',$email)->first();
                if($user->is_admin())
                {
                    return redirect()->route('admin.home');
                }
                return redirect()->route('admin.home');
            }
        else
        {
            return redirect()->back()->withErrors([
                'password' => Lang::get('auth.failed'),
            ]);
        }
    }
}
