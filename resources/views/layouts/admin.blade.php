<!doctype html>
<html lang="{{ app()->getLocale() }}">

@include('partials.head')


<body class="hold-transition sidebar-mini layout-fixed">

<div class="wrapper">
@include('partials.leftsidebar-admin')
    <div class="content-wrapper" style="min-height: 900px;background-color: #faf8ff">
        @yield('content')
    </div>

</div>
@include('partials.script')
<script type="text/javascript">
    $(document).ready(function(){
        $('#datatable').DataTable()
    });
</script>
<script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/fullcalendar/2.2.7/fullcalendar.min.js"></script>
<script>
    @yield('pageScript')
</script>
<script src="https://www.gstatic.com/firebasejs/7.9.1/firebase-app.js"></script>
<script src="https://www.gstatic.com/firebasejs/7.9.1/firebase-messaging.js"></script>

<script>

    function sendTokenToServer(token){
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: 'home/admin_token/'+token,
            type: 'GET',
            success: function( data){
                console.log('data 1 : ', data);
            }
        });

    }
</script>
</body>

</html>
