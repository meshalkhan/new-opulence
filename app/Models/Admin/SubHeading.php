<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SubHeading extends Model
{
    use SoftDeletes;
    //
    protected $fillable = ['id',
        'subHeadingID',
        'name',
        'heading',
        'user_id',
        'created_by',
        'updated_by', 'deleted_by'];
    protected $table = 'subheading';
}

