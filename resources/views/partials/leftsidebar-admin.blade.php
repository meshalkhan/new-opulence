@inject('request', 'Illuminate\Http\Request')


<?php $per = PermissionHelper::getUserPermissions();
?>


<nav class="main-header navbar navbar-expand navbar-white navbar-light" style="color: #0a0302">
    <ul class="navbar-nav">
        <li class="nav-item">
            <a  style="color: #0a0302" class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
        </li>
        <li class="nav-item d-none d-sm-inline-block">
            <a  style="color: #0a0302" href="{{ route('admin.home') }}" class="nav-link">Home</a>
        </li>
    </ul>
</nav>

<aside class="main-sidebar sidebar-dark-primary elevation-4" style="background-color:rgba(0,0,0,0.85);">
    <div style="background-color: white">
        <a href="" target="_blank" class="brand-link" >
            <img src="{{ asset('public/uploads') }}/tax-time.png" style="width:180px;margin-left:10px">
        </a>
    </div>

    <div class="sidebar">

        <div class="user-panel mt-3 pb-3 mb-3 d-flex">
            <div class="info" style="color:white;">
                <div style="text-align:center;">
                    <i class="fas fa-user-alt" style="color:#fd7e14"></i>
                    <span style="font-family: inherit;font-variant-caps: small-caps; color:white; font-size: 18px;">
             &nbsp; <b>{{ucfirst(Auth::user()->name)}}</b>
            </span>
                </div>
                <div style="padding-top:10px">
                    <i class="fas fa-clock" style="color:white"></i>
                    {{$ldate = date('H:i:s')}} &nbsp &nbsp &nbsp
                    <i class="fas fa-calendar" style="color:white"></i>
                    {{$ldate = date('d/m/Y')}}
                </div>
            </div>
        </div>

        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">

                @if(in_array('pricing_manage',$per) )
                    <li class="nav-item {{ $request->segment(2) == 'pricing' ? 'active active-sub' : '' }} nav-link">
                        <a href="{{ route('admin.pricing.index') }}">
                            <i class="fa fa-money-bill nav-icon"></i>&nbsp;&nbsp;&nbsp;
                            <span >Metal Price </span>
                        </a>
                    </li>
                @endif

                @if(in_array('ticket_manage',$per) )
                    <li class="nav-item {{ $request->segment(2) == 'ticket' ? 'active active-sub' : '' }} nav-link">
                        <a href="{{ route('admin.ticket.index') }}">
                            <i class="fa fa-ticket-alt nav-icon"></i>&nbsp;&nbsp;&nbsp;
                            <span >Ticket </span>
                        </a>
                    </li>
                @endif

                @if(in_array('process1_manage',$per) || in_array('process2_manage',$per) ||
                in_array('process3_manage',$per) || in_array('process4_manage',$per) )
                    <li class="nav-item has-treeview {{ $request->segment(2) == 'process1' || $request->segment(2) == 'process2'
            || $request->segment(2) == 'process3' || $request->segment(2) == 'process4' ? 'menu-open' : ''}}">
                        <a href="#" class="nav-link">
                            <i class="nav-icon fas fa-recycle"></i>
                            <p>
                                &nbsp;&nbsp;&nbsp;Process
                                <i class="fas fa-angle-left right"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview ">
                            @if(in_array('process1_manage',$per) )
                                <li class="nav-item {{ $request->segment(2) == 'process1' ? 'active active-sub' : '' }} nav-link">
                                    <a href="{{ route('admin.process1.index') }}">
                                        <i class="fa fa-dice-one nav-icon"></i>&nbsp;&nbsp;&nbsp;
                                        <span >Item </span>
                                    </a>
                                </li>
                            @endif
                            @if(in_array('process2_manage',$per) )
                                <li class="nav-item {{ $request->segment(2) == 'process2' ? 'active active-sub' : '' }} nav-link">
                                    <a href="{{ route('admin.process2.index') }}">
                                        <i class="fa fa-dice-two nav-icon"></i>&nbsp;&nbsp;&nbsp;
                                        <span >Metal </span>
                                    </a>
                                </li>
                            @endif
                            @if(in_array('process3_manage',$per) )
                                <li class="nav-item {{ $request->segment(2) == 'process3' ? 'active active-sub' : '' }} nav-link">
                                    <a href="{{ route('admin.process3.index') }}">
                                        <i class="fa fa-dice-three nav-icon"></i>&nbsp;&nbsp;&nbsp;
                                        <span >Process </span>
                                    </a>
                                </li>
                            @endif
                            @if(in_array('process4_manage',$per) )
                                <li class="nav-item {{ $request->segment(2) == 'process4' ? 'active active-sub' : '' }} nav-link">
                                    <a href="{{ route('admin.process4.index') }}">
                                        <i class="fa fa-dice-four nav-icon"></i>&nbsp;&nbsp;&nbsp;
                                        <span >Further Process </span>
                                    </a>
                                </li>
                            @endif


                        </ul>
                    </li>
                @endif

                {{--<li class="nav-item has-treeview {{ $request->segment(2) == 'heading' || $request->segment(2) == 'subheading'--}}
                {{--|| $request->segment(2) == 'payment' ? 'menu-open' : ''}}">--}}
                {{--<a href="#" class="nav-link">--}}
                {{--<i class="nav-icon fas fa-money-bill-wave"></i>--}}
                {{--<p>--}}
                {{--&nbsp;&nbsp;&nbsp;Payment--}}
                {{--<i class="fas fa-angle-left right"></i>--}}
                {{--</p>--}}
                {{--</a>--}}
                {{--<ul class="nav nav-treeview ">--}}
                {{--@if(in_array('payment_manage',$per) )--}}
                {{--<li class="nav-item {{ $request->segment(2) == 'payment' ? 'active active-sub' : '' }} nav-link">--}}
                {{--<a href="{{ route('admin.payment.index') }}">--}}
                {{--<i class="fa fa-sort-amount-up-alt nav-icon"></i>&nbsp;&nbsp;&nbsp;--}}
                {{--<span >Payment </span>--}}
                {{--</a>--}}
                {{--</li>--}}
                {{--@endif--}}
                {{--@if(in_array('heading_manage',$per) )--}}
                {{--<li class="nav-item {{ $request->segment(2) == 'heading' ? 'active active-sub' : '' }} nav-link">--}}
                {{--<a href="{{ route('admin.heading.index') }}">--}}
                {{--<i class="fa fa-heading nav-icon"></i>&nbsp;&nbsp;&nbsp;--}}
                {{--<span >Heading</span>--}}
                {{--</a>--}}
                {{--</li>--}}
                {{--@endif--}}
                {{--@if(in_array('process3_manage',$per) )--}}
                {{--<li class="nav-item {{ $request->segment(2) == 'subheading' ? 'active active-sub' : '' }} nav-link">--}}
                {{--<a href="{{ route('admin.subheading.index') }}">--}}
                {{--<i class="fa fa-heading nav-icon"></i>&nbsp;&nbsp;&nbsp;--}}
                {{--<span >Sub-Heading</span>--}}
                {{--</a>--}}
                {{--</li>--}}
                {{--@endif--}}

                {{--</ul>--}}
                {{--</li>--}}

                @if(in_array('users_manage',$per) )
                    <li class="nav-item has-treeview {{ $request->segment(2) == 'users' || $request->segment(2) == 'customer'|| $request->segment(2) == 'roles' ? 'menu-open' : ''}}">
                        <a href="#" class="nav-link">
                            <i class="nav-icon fas fa-users"></i>
                            <p>
                                &nbsp;&nbsp;&nbsp;User Management
                                <i class="fas fa-angle-left right"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview ">


                            {{--@if(in_array('permissions_manage',$per) )--}}
                            {{--<li class="{{ $request->segment(2) == 'permissions' ? 'active active-sub' : '' }} nav-link">--}}
                            {{--<a href="{{ route('admin.permissions.index') }}">--}}
                            {{--<i class="fa fa-briefcase nav-icon"></i>--}}
                            {{--<span >Permissions</span>--}}
                            {{--</a>--}}
                            {{--</li>--}}
                            {{--@endif--}}

                            @if(in_array('customer_manage',$per) )
                                <li class="nav-item {{ $request->segment(2) == 'customer' ? 'active active-sub' : '' }} nav-link">
                                    <a href="{{ route('admin.customer.index') }}">
                                        <i class="fa fa-user-friends nav-icon"></i>&nbsp;&nbsp;&nbsp;
                                        <span >Customers</span>
                                    </a>
                                </li>
                            @endif

                            @if(in_array('users_manage',$per) )
                                <li class="nav-item {{ $request->segment(2) == 'users' ? 'active active-sub' : '' }} nav-link">
                                    <a href="{{ route('admin.users.index') }}">
                                        <i class="fa fa-user nav-icon"></i>&nbsp;&nbsp;&nbsp;
                                        <span >Users</span>
                                    </a>
                                </li>
                            @endif

                            @if(in_array('roles_manage',$per) )
                                <li class="nav-item {{ $request->segment(2) == 'roles' ? 'active active-sub' : '' }} nav-link">
                                    <a href="{{ route('admin.roles.index') }}">
                                        <i class="fa fa-briefcase nav-icon"></i>&nbsp;&nbsp;&nbsp;
                                        <span >Roles</span>
                                    </a>
                                </li>
                            @endif

                        </ul>
                    </li>
                @endif

                <li class="nav-item nav-link active" style="background-color: #f4f5ff;color: #0a0302">
                    <a href="{{ route('logout') }}" onclick="event.preventDefault();
                       document.getElementById('logout-form').submit();">
                        <i style="color:rgba(0,0,0,0.85);" class="fas fa-sign-out-alt nav-icon"></i>&nbsp;&nbsp;&nbsp;
                        <span style="color: rgba(0,0,0,0.85);" ><b>Logout</b></span>
                    </a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                </li>
            </ul>
        </nav>
    </div>
</aside>
