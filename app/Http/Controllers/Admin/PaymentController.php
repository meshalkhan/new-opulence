<?php


namespace App\Http\Controllers\Admin;

use App\Helpers\PermissionHelper as Per;
use App\Models\Admin\Heading;
use App\Models\Admin\Payment;
use App\Models\Admin\SubHeading;
use App\User;
use Illuminate\Http\Request;
use DB;
use Auth;
use Form;

class PaymentController
{
    public function index()
    {
        if (! Per::has_permission('payment_details')) {
            return redirect()->route('home');
        }

        $heading=Heading::get()->pluck('name','name');
        $subheading=SubHeading::get()->pluck('name','name');
        return view('admin.payment.index',compact('heading','subheading'));
    }

    public function create(Request $request)
    {
        if (! Per::has_permission('payment_create')) {
            return redirect()->route('home');
        }
            $data = $request->all();
            $current_date_time = \Carbon\Carbon::now();
            $current_date_time->timezone = config('global.time_zone');
            $data['created_by'] = Auth::user()->id;
            $data['created_at'] = $current_date_time;
            Payment::create($data);
            session()->flash('success', 'Record has been created successfully.');
            return redirect()->route('admin.payment.index');

    }

    public function fetchdata(Request $request)
    {
        $data = Payment::where('paymentID',$request['paymentID'])->get();
        return response()->json($data);
    }

    public function editrow(Request $request)
    {
        if (! Per::has_permission('payment_edit')) {
            return redirect()->route('home');
        }
        $data = $request->all();
        $paymentID = $request['paymentID'];
        $current_date_time = \Carbon\Carbon::now();
        $current_date_time->timezone = config('global.time_zone');
        $data['updated_by'] = Auth::user()->id;
        $data['updated_at'] = $current_date_time;

        if($data['paymentID']=='' || $data['heading']=='' || $data['subHeading']=='' || $data['reference']==''
            || $data['rate']=='' || $data['quantity']==''|| $data['amount']==''|| $data['payment_option']==''  )
        {
            $response='null';
        }
        else
        {
            $datas['updated_by'] = Auth::user()->id;
            Payment::where('paymentID',$paymentID)->update([
                'heading'=>$request['heading'],
                'subHeading'=>$request['subHeading'],
                'reference'=>$request['reference'],
                'rate'=>$request['rate'],
                'quantity'=>$request['quantity'],
                'amount'=>$request['amount'],
                'payment_option'=>$request['payment_option'],
            ]);
            $response='success';

        }
        return response()->json($response);

    }

    public function deleterow(Request $request)
    {
        if (! Per::has_permission('payment_delete')) {
            return redirect()->route('home');
        }

        $paymentID = $request->paymentID;
        $payments = Payment::where('paymentID',$paymentID)->forceDelete();
    }

    public function allpayment(Request $request,$paymentID)
    {
        $columns = array(
            0 =>'paymentID',
            1 =>'heading',
            2 =>'subHeading',
            3 =>'reference',
            4 =>'rate',
            5 =>'quantity',
            6 =>'amount',
            7 =>'payment_option',
            8=> 'options',
        );

        $totalData = Payment::count();

        $totalFiltered = $totalData;

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');

        if(empty($request->input('search.value')))
        {
            $lists = Payment::offset($start)
                ->limit($limit)
                ->orderBy('reference','asc')
                ->get();
        }
        else
        {
            $search = $request->input('search.value');
            $lists =  Payment::where('reference', 'LIKE',"%{$search}%")
                ->offset($start)
                ->limit($limit)
                ->orderBy($order,$dir)
                ->get();

            $totalFiltered = Payment::where('reference', 'LIKE',"%{$search}%")
                ->count();
        }

        $data = array();

        $i=1;
        if(!empty($lists)) {
            foreach ($lists as $list)
            {

                $nestedData['paymentID'] = "&nbsp&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;".$i;
                $nestedData['heading'] = $list->heading;
                $nestedData['subHeading'] = $list->subHeading;
                $nestedData['reference'] = $list->reference;
                $nestedData['rate'] = $list->rate;
                $nestedData['quantity'] = $list->quantity;
                $nestedData['amount'] = $list->amount;
                $nestedData['payment_option'] = $list->payment_option;
                $options="";
                if (Per::has_permission('payment_delete')) {

                    $options .= "<a class='btn btn-sm btn-danger' style='cursor:pointer;color:white;margin-left:2px;' onclick='rowdelete(\"" . $list->paymentID . "\",\"0\")' ><i class='fas fa-trash-alt' ></i></button>";

                }
                if (Per::has_permission('payment_edit')) {
                    $options .= "<a class='btn btn-sm btn-warning' role='button' data-toggle='modal' data-target='#modal-payment' style='cursor:pointer;margin-left:2px;' onclick='editrow(\"" . $list->paymentID . "\")' ><i class='fas fa-edit' ></i></button>";
                }

                $nestedData['options'] = $options;
                $data[] = $nestedData;
                $i++;

            }
        }

        $json_data = array(
            "draw"            => intval($request->input('draw')),
            "recordsTotal"    => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data"            => $data
        );
        echo json_encode($json_data);

    }
}
