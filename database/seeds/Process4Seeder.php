<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class Process4Seeder extends Seeder
{
    public function run()
    {
        $MainPermission = Permission::create([
            'title' => 'Process 4',
            'name' => 'process4_manage',
            'guard_name' => 'web',
            'main_group' => 1,
            'parent_id' => 0,
        ]);
        Permission::insert([
            [
                'title' => 'Create',
                'name' => 'process4_create',
                'guard_name' => 'web',
                'main_group' => 0,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
                'parent_id' => $MainPermission->id,
            ],
            [
                'title' => 'Edit',
                'name' => 'process4_edit',
                'guard_name' => 'web',
                'main_group' => 0,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
                'parent_id' => $MainPermission->id,
            ],


            [
                'title' => 'Details',
                'name' => 'process4_details',
                'guard_name' => 'web',
                'main_group' => 0,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
                'parent_id' => $MainPermission->id,
            ],

            [
                'title' => 'Delete',
                'name' => 'process4_delete',
                'guard_name' => 'web',
                'main_group' => 0,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
                'parent_id' => $MainPermission->id,
            ],

        ]);

        // Assign Permission to 'administrator' role
        $role = Role::findById(1, 'web');
        $role->givePermissionTo('process4_manage');
        $role->givePermissionTo('process4_create');
        $role->givePermissionTo('process4_edit');
        $role->givePermissionTo('process4_details');
        $role->givePermissionTo('process4_delete');

        \App\Models\Admin\Process4::insert([
            [
                'name' => '	Size Up',
                'nameKey' => '	Size Up'
               ,'user_id' => '1'],
            [
                'name' => '	Size Down',
                'nameKey' => '	Size Down'
               ,'user_id' => '1'],
            [
                'name' => 'Metal Supplied',
                'nameKey' => 'Metal Supplied'
               ,'user_id' => '1'],
            [
                'name' => '	Metal To Supply',
                'nameKey' => '	Metal To Supply'
               ,'user_id' => '1'],
            [
                'name' => 'Stone Suplied',
                'nameKey' => 'Stone Suplied'
               ,'user_id' => '1'],
            [
                'name' => '	Stone To Supply',
                'nameKey' => '	Stone To Supply'
               ,'user_id' => '1'],
            [
                'name' => 'Ring Size',
                'nameKey' => 'Ring Size'
               ,'user_id' => '1'],
            [
                'name' => 'Band Width',
                'nameKey' => 'Band Width'
               ,'user_id' => '1'],
            [
                'name' => '	Band Depth',
                'nameKey' => '	Band Depth'
               ,'user_id' => '1'],
            [
                'name' => 'Wedding Band Style',
                'nameKey' => 'Wedding Band Style'
               ,'user_id' => '1'],
            [
                'name' => '	Setting Style',
                'nameKey' => '	Setting Style'
               ,'user_id' => '1'],
        ]);
    }
}
