
<div class="row">
    <div class="form-group  col-md-3 @if($errors->has('date')) has-error @endif">
        {!! Form::label('date', 'Date *', ['class' => 'control-label']) !!}
        {!! Form::text('date', old('date'), ['class' => 'form-control form-control-sm','id'=>'kt_datetimepicker_2_modal',  'placeholder' => 'Select date &amp; time','readonly'=>'true','maxlength' =>100]) !!}
        @if($errors->has('date'))
            <span class="help-block">
                                        {{ $errors->first('date') }}
                                    </span>
        @endif
    </div>
    <div class="form-group  col-md-3 @if($errors->has('reference')) has-error @endif">
        {!! Form::label('reference', 'Customer Refernece # ', ['class' => 'control-label']) !!}
        {!! Form::text('reference', old('reference'), ['class' => 'form-control form-control-sm','id'=>'reference','maxlength' =>100]) !!}
        @if($errors->has('reference'))
            <span class="help-block">
                                        {{ $errors->first('reference') }}
                                    </span>
        @endif
    </div>
    <div class="form-group  col-md-3 @if($errors->has('quantity')) has-error @endif">
        {!! Form::label('qunatity', 'Quantity', ['class' => 'control-label']) !!}
        {!! Form::number('qunatity', old('qunatity'), ['class' => 'form-control form-control-sm','id'=>'qunatity','maxlength' =>100]) !!}
        @if($errors->has('qunatity'))
            <span class="help-block">
                                        {{ $errors->first('qunatity') }}
                                    </span>
        @endif
    </div>
    <div class="form-group  col-md-3 @if($errors->has('nameKey')) has-error @endif">
        {!! Form::label('total', 'Total Price ', ['class' => 'control-label']) !!}
        {!! Form::text('total', old('total'), ['class' => 'form-control form-control-sm','id'=>'total','maxlength' =>100]) !!}
        @if($errors->has('total'))
            <span class="help-block">
                                        {{ $errors->first('total') }}
                                    </span>
        @endif
    </div>
</div>
<div class="row">
    <div class="col-lg-3">
    </div>
    <div class="col-lg-3">
    </div>
    <div class="col-lg-3">
    </div>
<div class="col-lg-3">
    <button type="submit" class="btn btn-sm btn-secondary">Add Item</button>
    <button type="button" class="btn btn-sm btn-secondary mt-10 float-right" onclick="addTicket()">Add Ticket</button>
</div>

