@extends('layouts.admin')

@section('content')

{{--@if($message = Session::get('success'))--}}
{{--   <!--  <div class="alert alert-info">{{ Session::get('success') }}</div> -->--}}
{{--     <div class="card-body">--}}
{{--            <div class="alert alert-success alert-dismissible">--}}
{{--                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>--}}
{{--                  <h5><i class="icon fas fa-check"></i> Alert!</h5>--}}
{{--                 {{ Session::get('success') }}--}}
{{--            </div>--}}
{{--    </div>--}}
{{--@endif--}}
{{--<!-- Content Header (Page header) -->--}}
{{--<section class="content-header">--}}

{{--</section>--}}
<section class="content-header" style="height: 50px;margin-left:30px ; ">

    @if($message = Session::get('success'))
        <div class="toast alert-info" style="padding: 3px;text-align: center;">{{ Session::get('success') }}</div>
    @endif

</section>

<!-- Main content -->
<section class="content">
   <div class="card card-secondary">
            <div class="card-header">
              <h3 style="font-weight: bold;font-size: 16px;" class="card-title">Roles Management</h3>
              <div class="card-tools">
                <a style="color: white;cursor: pointer;background-color: #069ba9;border:1px solid #069ba9 " class="btn btn-sm"  href="{{route('admin.roles.create')}}" class="btn btn-danger btn-sm pull-right">New Role</a>
              </div>
            </div>
            <!-- /.card-header -->
            <div class="card-body" style="overflow-x:auto;font-size: 15px;">
              <table id="example1" class="display dataTable" style="width:100%;font-size: 13px;" >
                <thead >
                <tr>
                  <th>ID</th>
                  <th>Name</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                    @if (count($Roles) > 0)
                        @foreach ($Roles as $Role)
                            <tr data-entry-id="{{ $Role->id }}">
                                <td>{{ $Role->id }}</td>
                                <td>{{ $Role->name }}</td>

                                <td>
                                    {{--@if($Role->status && Gate::check('roles_inactive'))--}}
                                        {{--{!! Form::open(array(--}}
                                                {{--'style' => 'display: inline-block;',--}}
                                                {{--'method' => 'PATCH',--}}
                                                {{--'onsubmit' => "return confirm('Are you sure to inactivate this record?');",--}}
                                                {{--'route' => ['admin.roles.inactive', $Role->id])) !!}--}}
                                        {{--{!! Form::submit('Inactivate', array('class' => 'btn btn-xs btn-warning')) !!}--}}
                                        {{--{!! Form::close() !!}--}}
                                    {{--@elseif(!$Role->status && Gate::check('roles_active'))--}}
                                        {{--{!! Form::open(array(--}}
                                                {{--'style' => 'display: inline-block;',--}}
                                                {{--'method' => 'PATCH',--}}
                                                {{--'onsubmit' => "return confirm('Are you sure to activate this record?');",--}}
                                                {{--'route' => ['admin.roles.active', $Role->id])) !!}--}}
                                        {{--{!! Form::submit('Activate', array('class' => 'btn btn-xs btn-primary')) !!}--}}
                                        {{--{!! Form::close() !!}--}}
                                    {{--@endif--}}

                                    {{--@if(Gate::check('roles_edit'))--}}
                                        <a href="{{ route('admin.roles.edit',[$Role->id]) }}" class="btn btn-sm btn-info"><i class="fas fa-edit"></i></a>
                                    {{--@endif--}}

                                    {{--@if(Gate::check('roles_destroy'))--}}
                                        {{--{!! Form::open(array(--}}
                                            {{--'style' => 'display: inline-block;',--}}
                                            {{--'method' => 'DELETE',--}}
                                            {{--'onsubmit' => "return confirm('".trans("global.app_are_you_sure")."');",--}}
                                            {{--'route' => ['admin.roles.destroy', $Role->id])) !!}--}}
                                        {{--{!! Form::submit(trans('global.app_delete'), array('class' => 'btn btn-xs btn-danger')) !!}--}}
                                        {{--{!! Form::close() !!}--}}
                                    {{--@endif--}}
                                </td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="6">@lang('global.app_no_entries_in_table')</td>
                        </tr>
                    @endif
                    </tbody>
              </table>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
</section>
    <!-- /.content -->

@endsection
@section('jsscript')

{{-- <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script> --}}
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>

@endsection
