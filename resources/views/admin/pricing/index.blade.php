@inject('request', 'Illuminate\Http\Request')
<?php $per = PermissionHelper::getUserPermissions();?>
@extends('layouts.admin')

@section('content')

    <!-- Main content -->
    <section class="content">
        <div class="card-body container">
            <!--begin::Dashboard-->

            <div class="row">
                <div class="col-12">
                    <div class="row">

                        @if(in_array('calculate_gold_price',$per))
                        <div class="col-lg-4 mb-5">
                            <div class="card p-3">
                                <input class="form-group btn btn-danger mb-6" type="button" value="Calculate Gold price by api"
                                       onclick="getGoldPriceByApi()" id="getGoldPriceByApi">
                                <form id="goldForm" onsubmit="event.preventDefault();">
                                    <div class="form-group">
                                        <label class="control-label">24kt gold price</label>
                                        <input type="number" id="goldPrice" name="goldPrice"
                                               placeholder="Enter 24kt gold price" class="form-control form-control-sm mb-2"
                                               value="{{$metalPrices['goldPrice']}}" step=".00001">
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label" l>Gold add %</label>
                                        <input type="number" id="goldAdd" name="goldAdd" placeholder="Enter Add %"
                                               class="form-control form-control-sm mb-2" value="{{$metalPrices['goldAdd']}}">
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label">Gold Vat %</label>
                                        <input type="number" id="goldVat" name="goldVat" placeholder="Enter Vat %"
                                               class="form-control form-control-sm mb-2" value="{{$metalPrices['goldVat']}}">
                                    </div>
                                    <div class="form-group text-center" style="margin-bottom:0;">
                                        <input class="btn btn-secondary" onclick="calculategold()" type="submit" value="Calculate gold per gram">
                                    </div>
                                </form>
                            </div>
                        </div>
                        @endif

                        @if(in_array('calculate_silver_price',$per))
                        <div class="col-lg-4 mb-5">
                            <div class="card p-3 pt-20">

                                <input class="form-group btn btn-danger mb-6" type="button"
                                       id="">
                                <form id="platinumForm" onsubmit="event.preventDefault();">
                                    <div class="form-group">
                                        <label class="control-label">Platinum price</label>
                                        <input type="number" id="platinumPrice" name="platinumPrice"
                                               placeholder="Enter 24kt platinum price" class="form-control form-control-sm mb-2"
                                               value="{{ $metalPrices['platinumPrice']}}" step=".00001">
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label" l>Platinum add %</label>
                                        <input type="number" id="platinumAdd" name="platinumAdd"
                                               placeholder="Enter Add %" class="form-control form-control-sm mb-2"
                                               value="{{ $metalPrices['platinumAdd']}}">
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label">Platinum Vat %</label>
                                        <input type="number" id="platinumVat" name="platinumVat"
                                               placeholder="Enter Vat %" class="form-control form-control-sm mb-2"
                                               value="{{ $metalPrices['platinumVat']}}">
                                    </div>
                                    <div class="form-group text-center" style="margin-bottom:0;">
                                        <input class="btn btn-secondary"  onclick="calculateplatinum()"  type="submit"
                                               value="Calculate platinum per gram">
                                    </div>
                                </form>
                            </div>
                        </div>
                        @endif

                        @if(in_array('calculate_platinum_price',$per))
                        <div class="col-lg-4 mb-5">
                            <div class="card p-3">
                                <input class="form-group btn btn-danger mb-6" type="button" value="Calculate Silver price by api"
                                       onclick="getSilverPriceByApi()"  id="getSilverPriceByApi">
                                <form id="silverForm" onsubmit="event.preventDefault();">
                                    <div class="form-group">
                                        <label class="control-label">Silver price</label>
                                        <input type="number" id="silverPrice" name="silverPrice"
                                               placeholder="Enter 24kt silver price" class="form-control form-control-sm mb-2"
                                               value="{{ $metalPrices['silverPrice']}}" step=".00001">
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label" l>Silver add %</label>
                                        <input type="number" id="silverAdd" name="silverAdd" placeholder="Enter Add %"
                                               class="form-control form-control-sm mb-2" value="{{ $metalPrices['silverAdd']}}">
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label">Silver Vat %</label>
                                        <input type="number" id="silverVat" name="silverVat" placeholder="Enter Vat %"
                                               class="form-control form-control-sm mb-2" value="{{ $metalPrices['silverVat']}}">
                                    </div>
                                    <div class="form-group text-center" style="margin-bottom:0;">
                                        <input class="btn btn-secondary"  onclick="calculatesilver()"  type="submit" value="Calculate silver per gram">
                                    </div>
                                </form>
                            </div>
                        </div>
                        @endif
                    </div>
                </div>
            </div>

            <div class="row" style="margin-top: -36px;">
                <div class="col-lg-4 mb-5">
                    <div class="card">
                        <table class="table">
                            <tr class="bg-dark text-white">
                                <th></th>
                                <th>Gold</th>
                                <th></th>
                            </tr>
                            <tr>
                                <td>24kt</td>
                                <td id="showGoldOunceToGram"></td>
                                <td>£ &nbsp;<span id="showGoldPrice"></span></td>

                            </tr>
                            <tr>
                                <td>Add %</td>
                                <td><span id="showGoldAddPercentage"></span>%</td>
                                <td>£ &nbsp;<span id="showGoldAddPrice"></span></td>
                            </tr>
                            <tr>
                                <td>Gross Amount</td>
                                <td></td>
                                <td>£ &nbsp;<span id="showGoldGrossAmount"></span></td>
                            </tr>
                            <tr>
                                <td>VAT</td>
                                <td><span id="showGoldVatPercentage"></span>%</td>
                                <td>£ &nbsp;<span id="showGoldVatPrice"></span></td>
                            </tr>
                            <tr>
                                <td>Net Rate</td>
                                <td></td>
                                <td>£ &nbsp;<span id="showGoldNetPrice"></span></td>
                            </tr>
                            <tr class="bg-dark text-white">
                                <th>Per Gram</th>
                                <th></th>
                                <th>£ &nbsp;<span id="showGoldPerGramPrice"></span></th>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="col-lg-4 mb-5">
                    <div class="card">
                        <table class="table">
                            <tr class="bg-dark text-white">
                                <th></th>
                                <th>Platinum</th>
                                <th></th>
                            </tr>
                            <tr>
                                <td>24kt</td>
                                <td id="showPlatinumOunceToGram"></td>
                                <td>£ &nbsp;<span id="showPlatinumPrice"></span></td>

                            </tr>
                            <tr>
                                <td>Add %</td>
                                <td><span id="showPlatinumAddPercentage"></span>%</td>
                                <td>£ &nbsp;<span id="showPlatinumAddPrice"></span></td>
                            </tr>
                            <tr>
                                <td>Gross Amount</td>
                                <td></td>
                                <td>£ &nbsp;<span id="showPlatinumGrossAmount"></span></td>
                            </tr>
                            <tr>
                                <td>VAT</td>
                                <td><span id="showPlatinumVatPercentage"></span>%</td>
                                <td>£ &nbsp;<span id="showPlatinumVatPrice"></span></td>
                            </tr>
                            <tr>
                                <td>Net Rate</td>
                                <td></td>
                                <td>£ &nbsp;<span id="showPlatinumNetPrice"></span></td>
                            </tr>
                            <tr class="bg-dark text-white">
                                <th>Per Gram</th>
                                <th></th>
                                <th>£ &nbsp;<span id="showPlatinumPerGramPrice"></span></th>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="col-lg-4 mb-5">
                    <div class="card">
                        <table class="table">
                            <tr class="bg-danger text-white">
                                <th></th>
                                <th>Silver</th>
                                <th></th>
                            </tr>
                            <tr>
                                <td>24kt</td>
                                <td id="showSilverOunceToGram"></td>
                                <td>£ &nbsp;<span id="showSilverPrice"></span></td>

                            </tr>
                            <tr>
                                <td>Add %</td>
                                <td><span id="showSilverAddPercentage"></span>%</td>
                                <td>£ &nbsp;<span id="showSilverAddPrice"></span></td>
                            </tr>
                            <tr>
                                <td>Gross Amount</td>
                                <td></td>
                                <td>£ &nbsp;<span id="showSilverGrossAmount"></span></td>
                            </tr>
                            <tr>
                                <td>VAT</td>
                                <td><span id="showSilverVatPercentage"></span>%</td>
                                <td>£ &nbsp;<span id="showSilverVatPrice"></span></td>
                            </tr>
                            <tr>
                                <td>Net Rate</td>
                                <td></td>
                                <td>£ &nbsp;<span id="showSilverNetPrice"></span></td>
                            </tr>
                            <tr class="bg-dark text-white">
                                <th>Per Gram</th>
                                <th></th>
                                <th>£ &nbsp;<span id="showSilverPerGramPrice"></span></th>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>

            <div class="row" style="margin-top: -36px;">
                <div class="col-lg-12 mb-5">
                    <div class="card">
                        <table class="table">
                            <thead>
                            <tr class="bg-danger">
                                <th>Mixing</th>
                                <th>Particular</th>
                                <th>Rate</th>
                                <th>Rate (Rounded)</th>
                                <th>Per Gram</th>
                                <th>Labour Charges</th>
                                <th>Casting</th>
                                <th>Rounded</th>
                            </tr>
                            </thead>
                            <tbody id="showPricebyUnit"></tbody>
                        </table>
                    </div>
                </div>
            </div>

            <!--end::Dashboard-->
        </div>
    <!-- /.card -->
    </section>
    <!-- /.content -->

    @include('admin.pricing.js')
@endsection

