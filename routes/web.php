<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('cache', function () {

    /* php artisan migrate */
    \Artisan::call('cache:clear');
    \Artisan::call('config:clear');
    \Artisan::call('view:clear');
    dd("Done");
});
Route::get('/clear-cache', function() {
    $exitCode = Artisan::call('cache:clear');
    dd("Done");
});
Route::get('pass', function () {
    dd(\Artisan::call());
});

 Route::resource('/registration','RequestController');
 Route::resource('/logins','LoginController');
 Route::get('/logins','LoginController@index')->name('login');
 Route::get('/home', function(){
    return view('index');
})->name('home');

    Route::get('/', function () { return redirect('/admin/home'); });
    Auth::routes();
    Route::group(['middleware' => ['auth'], 'prefix' => 'admin', 'as' => 'admin.'], function () {

    Route::get('/', 'HomeController@index');
    Route::resource('/','EventController');
    Route::get('/home', 'HomeController@index')->name('home');
    Route::get('/dasboard', 'HomeController@admin_index')->name('admin.home');

    // Route Resource for Permission
    Route::patch('permissions/active/{id}', ['uses' => 'Admin\PermissionsController@active', 'as' => 'permissions.active']);
    Route::patch('permissions/inactive/{id}', ['uses' => 'Admin\PermissionsController@inactive', 'as' => 'permissions.inactive']);
    Route::resource('permissions', 'Admin\PermissionsController');
    Route::post('permissions_mass_destroy', ['uses' => 'Admin\PermissionsController@massDestroy', 'as' => 'permissions.mass_destroy']);

    // Route Resource for Role
    Route::patch('roles/active/{id}', ['uses' => 'Admin\RolesController@active', 'as' => 'roles.active']);
    Route::patch('roles/inactive/{id}', ['uses' => 'Admin\RolesController@inactive', 'as' => 'roles.inactive']);
    Route::resource('roles', 'Admin\RolesController');

    // Route Resource for User
    Route::resource('users', 'Admin\UsersController');


    // Route Resource for Home
    Route::get('home/admin_token/{token}', ['uses' => 'HomeController@admin_token', 'as' => 'home.admin_token']);

    // Pricing
    Route::get('pricing/index', ['uses' => 'Admin\DashboardController@index', 'as' => 'pricing.index']);
    Route::post('pricing/getMetalPrices', ['uses' => 'Admin\DashboardController@getMetalPrices', 'as' => 'pricing.getMetalPrices']);
    Route::post('pricing/saveGoldData', ['uses' => 'Admin\DashboardController@saveGoldData', 'as' => 'pricing.saveGoldData']);
    Route::post('pricing/savePlatinumData', ['uses' => 'Admin\DashboardController@savePlatinumData', 'as' => 'pricing.savePlatinumData']);
    Route::post('pricing/saveSilverData', ['uses' => 'Admin\DashboardController@saveSilverData', 'as' => 'pricing.saveSilverData']);
    Route::post('pricing/saveGoldPerGramPrice', ['uses' => 'Admin\DashboardController@saveGoldPerGramPrice', 'as' => 'pricing.saveGoldPerGramPrice']);
    Route::post('pricing/savePlatinumPerGramPrice', ['uses' => 'Admin\DashboardController@savePlatinumPerGramPrice', 'as' => 'pricing.savePlatinumPerGramPrice']);
    Route::post('pricing/saveSilverPerGramPrice', ['uses' => 'Admin\DashboardController@saveSilverPerGramPrice', 'as' => 'pricing.saveSilverPerGramPrice']);
    Route::post('pricing/getGoldPriceByApi', ['uses' => 'Admin\DashboardController@getGoldPriceByApi', 'as' => 'pricing.getGoldPriceByApi']);
    Route::post('pricing/getSilverPriceByApi', ['uses' => 'Admin\DashboardController@getSilverPriceByApi', 'as' => 'pricing.getSilverPriceByApi']);
    Route::post('pricing/getPriceByMetalUnit', ['uses' => 'Admin\DashboardController@getPriceByMetalUnit', 'as' => 'pricing.getPriceByMetalUnit']);



    //  Process 1
    Route::post('process1/create', ['uses' => 'Admin\Process1Controller@create', 'as' => 'process1.create']);
    Route::get('process1/index', ['uses' => 'Admin\Process1Controller@index', 'as' => 'process1.index']);
    Route::get('process1/allprocess1/{id}', ['uses' => 'Admin\Process1Controller@allprocess1', 'as' => 'process1.allprocess1']);
    Route::post('process1/editrow', ['uses' => 'Admin\Process1Controller@editrow', 'as' => 'process1.editrow']);
    Route::post('process1/fetchdata', ['uses' => 'Admin\Process1Controller@fetchdata', 'as' => 'process1.fetchdata']);
    Route::post('process1/deleterow', ['uses' => 'Admin\Process1Controller@deleterow', 'as' => 'process1.deleterow']);

    //  Process 2
    Route::post('process2/create', ['uses' => 'Admin\Process2Controller@create', 'as' => 'process2.create']);
    Route::get('process2/index', ['uses' => 'Admin\Process2Controller@index', 'as' => 'process2.index']);
    Route::get('process2/allprocess2/{id}', ['uses' => 'Admin\Process2Controller@allprocess2', 'as' => 'process2.allprocess2']);
    Route::post('process2/editrow', ['uses' => 'Admin\Process2Controller@editrow', 'as' => 'process2.editrow']);
    Route::post('process2/fetchdata', ['uses' => 'Admin\Process2Controller@fetchdata', 'as' => 'process2.fetchdata']);
    Route::post('process2/deleterow', ['uses' => 'Admin\Process2Controller@deleterow', 'as' => 'process2.deleterow']);

    //  Process 3
    Route::post('process3/create', ['uses' => 'Admin\Process3Controller@create', 'as' => 'process3.create']);
    Route::get('process3/index', ['uses' => 'Admin\Process3Controller@index', 'as' => 'process3.index']);
    Route::get('process3/allprocess3/{id}', ['uses' => 'Admin\Process3Controller@allprocess3', 'as' => 'process3.allprocess3']);
    Route::post('process3/editrow', ['uses' => 'Admin\Process3Controller@editrow', 'as' => 'process3.editrow']);
    Route::post('process3/fetchdata', ['uses' => 'Admin\Process3Controller@fetchdata', 'as' => 'process3.fetchdata']);
    Route::post('process3/deleterow', ['uses' => 'Admin\Process3Controller@deleterow', 'as' => 'process3.deleterow']);

    //  Process 3
    Route::post('process4/create', ['uses' => 'Admin\Process4Controller@create', 'as' => 'process4.create']);
    Route::get('process4/index', ['uses' => 'Admin\Process4Controller@index', 'as' => 'process4.index']);
    Route::get('process4/allprocess4/{id}', ['uses' => 'Admin\Process4Controller@allprocess4', 'as' => 'process4.allprocess4']);
    Route::post('process4/editrow', ['uses' => 'Admin\Process4Controller@editrow', 'as' => 'process4.editrow']);
    Route::post('process4/fetchdata', ['uses' => 'Admin\Process4Controller@fetchdata', 'as' => 'process4.fetchdata']);
    Route::post('process4/deleterow', ['uses' => 'Admin\Process4Controller@deleterow', 'as' => 'process4.deleterow']);

    //  Code
    Route::post('code/create', ['uses' => 'Admin\CodeController@create', 'as' => 'code.create']);
    Route::get('code/index', ['uses' => 'Admin\CodeController@index', 'as' => 'code.index']);
    Route::get('code/allcode/{id}', ['uses' => 'Admin\CodeController@allcode', 'as' => 'code.allcode']);
    Route::post('code/editrow', ['uses' => 'Admin\CodeController@editrow', 'as' => 'code.editrow']);
    Route::post('code/fetchdata', ['uses' => 'Admin\CodeController@fetchdata', 'as' => 'code.fetchdata']);
    Route::post('code/deleterow', ['uses' => 'Admin\CodeController@deleterow', 'as' => 'code.deleterow']);

    //  Heading
    Route::post('heading/create', ['uses' => 'Admin\HeadingController@create', 'as' => 'heading.create']);
    Route::get('heading/index', ['uses' => 'Admin\HeadingController@index', 'as' => 'heading.index']);
    Route::get('heading/allheading/{id}', ['uses' => 'Admin\HeadingController@allheading', 'as' => 'heading.allheading']);
    Route::post('heading/editrow', ['uses' => 'Admin\HeadingController@editrow', 'as' => 'heading.editrow']);
    Route::post('heading/fetchdata', ['uses' => 'Admin\HeadingController@fetchdata', 'as' => 'heading.fetchdata']);
    Route::post('heading/deleterow', ['uses' => 'Admin\HeadingController@deleterow', 'as' => 'heading.deleterow']);

    //  Sub Heading
    Route::post('subheading/create', ['uses' => 'Admin\SubHeadingController@create', 'as' => 'subheading.create']);
    Route::get('subheading/index', ['uses' => 'Admin\SubHeadingController@index', 'as' => 'subheading.index']);
    Route::get('subheading/allsubheading/{id}', ['uses' => 'Admin\SubHeadingController@allsubheading', 'as' => 'subheading.allsubheading']);
    Route::post('subheading/editrow', ['uses' => 'Admin\SubHeadingController@editrow', 'as' => 'subheading.editrow']);
    Route::post('subheading/fetchdata', ['uses' => 'Admin\SubHeadingController@fetchdata', 'as' => 'subheading.fetchdata']);
    Route::post('subheading/deleterow', ['uses' => 'Admin\SubHeadingController@deleterow', 'as' => 'subheading.deleterow']);

    //  Payment
    Route::post('payment/create', ['uses' => 'Admin\PaymentController@create', 'as' => 'payment.create']);
    Route::get('payment/index', ['uses' => 'Admin\PaymentController@index', 'as' => 'payment.index']);
    Route::get('payment/allpayment/{id}', ['uses' => 'Admin\PaymentController@allpayment', 'as' => 'payment.allpayment']);
    Route::post('payment/editrow', ['uses' => 'Admin\PaymentController@editrow', 'as' => 'payment.editrow']);
    Route::post('payment/fetchdata', ['uses' => 'Admin\PaymentController@fetchdata', 'as' => 'payment.fetchdata']);
    Route::post('payment/deleterow', ['uses' => 'Admin\PaymentController@deleterow', 'as' => 'payment.deleterow']);

    //  Customer
    Route::post('customer/create', ['uses' => 'Admin\CustomerController@create', 'as' => 'customer.create']);
    Route::get('customer/index', ['uses' => 'Admin\CustomerController@index', 'as' => 'customer.index']);
    Route::get('customer/allcustomer/{id}', ['uses' => 'Admin\CustomerController@allcustomer', 'as' => 'customer.allcustomer']);
    Route::post('customer/editrow', ['uses' => 'Admin\CustomerController@editrow', 'as' => 'customer.editrow']);
    Route::post('customer/fetchdata', ['uses' => 'Admin\CustomerController@fetchdata', 'as' => 'customer.fetchdata']);
    Route::post('customer/deleterow', ['uses' => 'Admin\CustomerController@deleterow', 'as' => 'customer.deleterow']);

    //  Ticket
    Route::post('ticket/create', ['uses' => 'Admin\TicketController@create', 'as' => 'ticket.create']);
    Route::get('ticket/index', ['uses' => 'Admin\TicketController@index', 'as' => 'ticket.index']);
    Route::get('ticket/add', ['uses' => 'Admin\TicketController@add', 'as' => 'ticket.add']);
    Route::get('ticket/allticket/{id}', ['uses' => 'Admin\TicketController@allticket', 'as' => 'ticket.allticket']);
    Route::post('ticket/editrow', ['uses' => 'Admin\TicketController@editrow', 'as' => 'ticket.editrow']);
    Route::post('ticket/fetchdata', ['uses' => 'Admin\TicketController@fetchdata', 'as' => 'ticket.fetchdata']);
    Route::post('ticket/deleterow', ['uses' => 'Admin\TicketController@deleterow', 'as' => 'ticket.deleterow']);

    });

