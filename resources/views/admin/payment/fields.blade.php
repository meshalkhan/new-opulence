<div class="form-group  col-md-3 @if($errors->has('heading')) has-error @endif">
    {!! Form::label('heading', 'Heading *', ['class' => 'control-label']) !!}
    {!! Form::select('heading', $heading,old('heading'), ['class' => 'form-control form-control-sm',  'placeholder' => 'Select heading','required'=>'true','maxlength' =>100]) !!}
    {!! Form::text('update_id', null, ['class' => 'form-control form-control-sm', 'id'=>'update_id','hidden' => 'true']) !!}
    @if($errors->has('heading'))
        <span class="help-block">
                                        {{ $errors->first('heading') }}
                                    </span>
    @endif

</div>

<div class="form-group  col-md-3 @if($errors->has('subHeading')) has-error @endif">
    {!! Form::label('subHeading', 'Sub Heading *', ['class' => 'control-label']) !!}
    {!! Form::select('subHeading',$subheading, old('subHeading'), ['class' => 'form-control form-control-sm',  'placeholder' => 'Select Sub Heading','required'=>'true','maxlength' =>100]) !!}
    @if($errors->has('subHeading'))
        <span class="help-block">
                                        {{ $errors->first('subHeading') }}
                                    </span>
    @endif

</div>

<div class="form-group  col-md-3 @if($errors->has('reference')) has-error @endif">
    {!! Form::label('reference', 'Reference *', ['class' => 'control-label']) !!}
    {!! Form::text('reference', old('reference'), ['class' => 'form-control form-control-sm',  'placeholder' => 'Enter Reference','required'=>'true','maxlength' =>100]) !!}
    @if($errors->has('reference'))
        <span class="help-block">
                                        {{ $errors->first('reference') }}
                                    </span>
    @endif

</div>
<div class="form-group  col-md-3 @if($errors->has('rate')) has-error @endif">
    {!! Form::label('rate', 'Rate *', ['class' => 'control-label']) !!}
    {!! Form::number('rate', old('rate'), ['class' => 'form-control form-control-sm',  'placeholder' => 'Enter Rate','required'=>'true','maxlength' =>100]) !!}
    @if($errors->has('rate'))
        <span class="help-block">
                                        {{ $errors->first('rate') }}
                                    </span>
    @endif

</div>
<div class="form-group  col-md-3 @if($errors->has('quantity')) has-error @endif">
    {!! Form::label('quantity', 'Quantity *', ['class' => 'control-label']) !!}
    {!! Form::number('quantity', old('quantity'), ['class' => 'form-control form-control-sm',  'placeholder' => 'Enter Quantity','required'=>'true','maxlength' =>100]) !!}
    @if($errors->has('quantity'))
        <span class="help-block">
                                        {{ $errors->first('quantity') }}
                                    </span>
    @endif

</div>
<div class="form-group  col-md-3 @if($errors->has('amount')) has-error @endif">
    {!! Form::label('amount', 'Amount *', ['class' => 'control-label']) !!}
    {!! Form::number('amount', old('amount'), ['class' => 'form-control form-control-sm',  'placeholder' => 'Enter Amount','required'=>'true','maxlength' =>100]) !!}
    @if($errors->has('amount'))
        <span class="help-block">
                                        {{ $errors->first('amount') }}
                                    </span>
    @endif

</div>
<div class="form-group  col-md-3 @if($errors->has('payment_option')) has-error @endif">
    {!! Form::label('payment_option', 'Payment Option *', ['class' => 'control-label']) !!}
    {!! Form::select('payment_option',config('status.payment_option'), old('payment_option'), ['class' => 'form-control form-control-sm',  'placeholder' => 'Select Payment Option','required'=>'true','maxlength' =>100]) !!}
    @if($errors->has('payment_option'))
        <span class="help-block">
                                        {{ $errors->first('payment_option') }}
                                    </span>
    @endif

</div>



