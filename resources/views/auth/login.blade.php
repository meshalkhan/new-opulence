<!DOCTYPE html>
<html lang="en">
<head>
	<title>Tax Time</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!--===============================================================================================-->
	<link rel="icon" type="image/png" href="images/tax-time.png"/>
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="fonts/font-awesome-4.7.0/css/font-awesome.min.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="fonts/iconic/css/material-design-iconic-font.min.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/animate/animate.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/css-hamburgers/hamburgers.min.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/animsition/css/animsition.min.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/select2/select2.min.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/daterangepicker/daterangepicker.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="css/util.css">
	<link rel="stylesheet" type="text/css" href="css/main.css">
	<!--===============================================================================================-->
</head>
<body>

<br/>
<div class="limiter">
	<div class="container-login100">
		<div>
			<form style="background-color: #198187;width:500px;padding: 20px;margin-top: -10px" class="login100-form validate-form" method="POST" action="{{ route('login') }}">
				@csrf
				<span class="login100-form-logo" style="background-color: white">
						 <img src="{{ asset('public/uploads') }}/tax-time.png" style="border-radius:2%; border:1px solid white;width:240px;height:60px;margin-left:10px">
					</span>

				<span class="login100-form-title p-b-34 p-t-27">
						Welcome Back
					</span>

				<div class="wrap-input100 validate-input" data-validate = "Invalid Email or Password">
					<input class="input100 @error('email') is-invalid @enderror" type="email" name="email" value="{{ old('email') }}" placeholder="Email Address">
					<span class="focus-input100" data-placeholder="&#xf207;"></span>
					@error('email')
					<span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
					@enderror
				</div>

				<div class="wrap-input100 validate-input" data-validate="Enter password">
					<input class="input100 @error('password') is-invalid @enderror" type="password" name="password" required placeholder="Password" autocomplete="current-password">
					<span class="focus-input100" data-placeholder="&#xf191;"></span>
					@error('password')
					<span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
					@enderror
				</div>

				<div class="contact100-form-checkbox">
					<input class="input-checkbox100" id="remember" type="checkbox" name="remember-me">
					<label class="label-checkbox100" for="remember" >
						Remember me
					</label>
				</div>

				<div class="container-login100-form-btn">
					<button type="submit" class="login100-form-btn">
						Login
					</button>
				</div>
				@if (Route::has('password.request'))
					<div class="text-center p-t-90">
						<a class="txt1" href="{{ route('password.request') }}">
							Forgot Password?
						</a>
					</div>
				@endif
			</form>
		</div>
	</div>
</div>


<div id="dropDownSelect1"></div>

<!--===============================================================================================-->
<script src="vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
<script src="vendor/animsition/js/animsition.min.js"></script>
<!--===============================================================================================-->
<script src="vendor/bootstrap/js/popper.js"></script>
<script src="vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
<script src="vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
<script src="vendor/daterangepicker/moment.min.js"></script>
<script src="vendor/daterangepicker/daterangepicker.js"></script>
<!--===============================================================================================-->
<script src="vendor/countdowntime/countdowntime.js"></script>
<!--===============================================================================================-->
<script src="js/main.js"></script>

</body>
</html>
<!DOCTYPE html>
<html lang="en">
<head>
	<title>Tax Time</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!--===============================================================================================-->
	<link rel="icon" type="image/png" href="images/tax-time.png"/>
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="fonts/font-awesome-4.7.0/css/font-awesome.min.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="fonts/iconic/css/material-design-iconic-font.min.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/animate/animate.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/css-hamburgers/hamburgers.min.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/animsition/css/animsition.min.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/select2/select2.min.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/daterangepicker/daterangepicker.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="css/util.css">
	<link rel="stylesheet" type="text/css" href="css/main.css">
	<!--===============================================================================================-->
</head>
<body>

<br/>
<div class="limiter">
	<div class="container-login100">
		<div>
			<form style="background-color: #198187;width:500px;padding: 20px;margin-top: -10px" class="login100-form validate-form" method="POST" action="{{ route('login') }}">
				@csrf
				<span class="login100-form-logo" style="background-color: white">
						 <img src="{{ asset('public/uploads') }}/tax-time.png" style="border-radius:2%; border:1px solid white;width:240px;height:60px;margin-left:10px">
					</span>

				<span class="login100-form-title p-b-34 p-t-27">
						Welcome Back
					</span>

				<div class="wrap-input100 validate-input" data-validate = "Invalid Email or Password">
					<input class="input100 @error('email') is-invalid @enderror" type="email" name="email" value="{{ old('email') }}" placeholder="Email Address">
					<span class="focus-input100" data-placeholder="&#xf207;"></span>
					@error('email')
					<span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
					@enderror
				</div>

				<div class="wrap-input100 validate-input" data-validate="Enter password">
					<input class="input100 @error('password') is-invalid @enderror" type="password" name="password" required placeholder="Password" autocomplete="current-password">
					<span class="focus-input100" data-placeholder="&#xf191;"></span>
					@error('password')
					<span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
					@enderror
				</div>

				<div class="contact100-form-checkbox">
					<input class="input-checkbox100" id="remember" type="checkbox" name="remember-me">
					<label class="label-checkbox100" for="remember">
						Remember me
					</label>
				</div>

				<div class="container-login100-form-btn">
					<button type="submit" class="login100-form-btn">
						Login
					</button>
				</div>
				@if (Route::has('password.request'))
					<div class="text-center p-t-90">
						<a class="txt1" href="{{ route('password.request') }}">
							Forgot Password?
						</a>
					</div>
				@endif
			</form>
		</div>
	</div>
</div>


<div id="dropDownSelect1"></div>

<!--===============================================================================================-->
<script src="vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
<script src="vendor/animsition/js/animsition.min.js"></script>
<!--===============================================================================================-->
<script src="vendor/bootstrap/js/popper.js"></script>
<script src="vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
<script src="vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
<script src="vendor/daterangepicker/moment.min.js"></script>
<script src="vendor/daterangepicker/daterangepicker.js"></script>
<!--===============================================================================================-->
<script src="vendor/countdowntime/countdowntime.js"></script>
<!--===============================================================================================-->
<script src="js/main.js"></script>

</body>
</html>
