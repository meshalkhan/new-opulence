<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Billing extends Model
{
    use SoftDeletes;
    //
    protected $fillable = ['id',
        'ticket_id',
        'customer_id',
        'price',
        'amount',
        'remainingAmount',
        'week',
        'createdDate',
        'user_id',
        'created_by',
        'updated_by', 'deleted_by'];
    protected $table = 'billings';
}

