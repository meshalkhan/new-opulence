<?php

namespace App\Http\Controllers\Admin;

use App\Models\Admin\MetalPrice;
use App\Models\Admin\Process2;
use Illuminate\Http\Request;
use Auth;

class DashboardController
{
    public function index()
    {
        $metalPrices=MetalPrice::first();

        return view('admin.pricing.index',compact('metalPrices'));
    }

    public function getMetalPrices(Request $request)
    {
        $metalPrices=MetalPrice::first();
        return response()->json($metalPrices);

    }

    public function saveGoldData(Request $request)
    {
        $current_date_time = \Carbon\Carbon::now();
        $current_date_time->timezone = config('global.time_zone');
        $goldPrice=$request['goldPrice'];
        $goldAdd=$request['goldAdd'];
        $goldVat=$request['goldVat'];

        MetalPrice::where('id','1')->update([
            'goldPrice'=>$goldPrice,
            'goldAdd'=>$goldAdd,
            'goldVat'=>$goldVat,
            'updated_at'=>$current_date_time,
            'updated_by'=>Auth::user()->id
        ]);

        return response()->json('1');

    }

    public function savePlatinumData(Request $request)
    {
        $current_date_time = \Carbon\Carbon::now();
        $current_date_time->timezone = config('global.time_zone');
        $platinumPrice=$request['platinumPrice'];
        $platinumAdd=$request['platinumAdd'];
        $platinumVat=$request['platinumVat'];

        MetalPrice::where('id','1')->update([
            'platinumPrice'=>$platinumPrice,
            'platinumAdd'=>$platinumAdd,
            'platinumVat'=>$platinumVat,
            'updated_at'=>$current_date_time,
            'updated_by'=>Auth::user()->id
        ]);

        return response()->json('1');

    }

    public function saveSilverData(Request $request)
    {
        $current_date_time = \Carbon\Carbon::now();
        $current_date_time->timezone = config('global.time_zone');
        $silverPrice=$request['silverPrice'];
        $silverAdd=$request['silverAdd'];
        $silverVat=$request['silverVat'];

        MetalPrice::where('id','1')->update([
            'silverPrice'=>$silverPrice,
            'silverAdd'=>$silverAdd,
            'silverVat'=>$silverVat,
            'updated_at'=>$current_date_time,
            'updated_by'=>Auth::user()->id
        ]);

        return response()->json('1');

    }

    public function saveGoldPerGramPrice(Request $request)
    {
        $current_date_time = \Carbon\Carbon::now();
        $current_date_time->timezone = config('global.time_zone');
        $perGramGoldPrice=$request['perGramGoldPrice'];
        MetalPrice::where('id','1')->update([
            'perGramGoldPrice'=>$perGramGoldPrice,
            'updated_at'=>$current_date_time,
            'updated_by'=>Auth::user()->id
        ]);

        return response()->json('1');

    }

    public function savePlatinumPerGramPrice(Request $request)
    {
        $current_date_time = \Carbon\Carbon::now();
        $current_date_time->timezone = config('global.time_zone');
        $perGramPlatinumPrice=$request['perGramPlatinumPrice'];
        MetalPrice::where('id','1')->update([
            'perGramPlatinumPrice'=>$perGramPlatinumPrice,
            'updated_at'=>$current_date_time,
            'updated_by'=>Auth::user()->id
        ]);

        return response()->json('1');

    }

    public function saveSilverPerGramPrice(Request $request)
    {
        $current_date_time = \Carbon\Carbon::now();
        $current_date_time->timezone = config('global.time_zone');
        $perGramSilverPrice=$request['perGramSilverPrice'];
        MetalPrice::where('id','1')->update([
            'perGramSilverPrice'=>$perGramSilverPrice,
            'updated_at'=>$current_date_time,
            'updated_by'=>Auth::user()->id
        ]);

        return response()->json('1');

    }

    public function getGoldPriceByApi(Request $request)
    {
        $ch = curl_init();
        $headers = array(
            'x-access-token: goldapi-1c1bzykbkskogy-io',
            'Content-Type: application/json',

        );
        curl_setopt($ch, CURLOPT_URL, 'https://www.goldapi.io/api/XAU/GBP');
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        $body = '{}';

        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
        curl_setopt($ch, CURLOPT_POSTFIELDS,$body);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        // Timeout in seconds
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);

        $authToken = curl_exec($ch);

        $metalPrices = json_decode($authToken);

        $goldPrice = $metalPrices->price;
        MetalPrice::where('id','1')->update([
            'goldPrice'=>$goldPrice,
        ]);
        return response()->json($goldPrice);
    }

    public function getSilverPriceByApi(Request $request)
    {
        $ch = curl_init();
        $headers = array(
            'x-access-token: goldapi-1c1bzykbkskogy-io',
            'Content-Type: application/json',

        );
        curl_setopt($ch, CURLOPT_URL, 'https://www.goldapi.io/api/XAG/GBP');
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        $body = '{}';

        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
        curl_setopt($ch, CURLOPT_POSTFIELDS,$body);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        // Timeout in seconds
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);

        $authToken = curl_exec($ch);

        $metalPrices = json_decode($authToken);

        $silverPrice = $metalPrices->price;

        MetalPrice::where('id','1')->update([
            'silverPrice'=>$silverPrice,
        ]);
        return response()->json($silverPrice);

    }

    public function getPriceByMetalUnit(Request $request)
    {
        $metalPrices=MetalPrice::first();
        $metalUnit=Process2::get();

        $result['metalPrices']= $metalPrices;
        $result['metalUnit']= $metalUnit;

        echo json_encode($result);
    }

}
