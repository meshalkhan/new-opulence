<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class Process2Seeder extends Seeder
{
    public function run()
    {
        $MainPermission = Permission::create([
            'title' => 'Process 2',
            'name' => 'process2_manage',
            'guard_name' => 'web',
            'main_group' => 1,
            'parent_id' => 0,
        ]);
        Permission::insert([
            [
                'title' => 'Create',
                'name' => 'process2_create',
                'guard_name' => 'web',
                'main_group' => 0,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
                'parent_id' => $MainPermission->id,
            ],
            [
                'title' => 'Edit',
                'name' => 'process2_edit',
                'guard_name' => 'web',
                'main_group' => 0,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
                'parent_id' => $MainPermission->id,
            ],


            [
                'title' => 'Details',
                'name' => 'process2_details',
                'guard_name' => 'web',
                'main_group' => 0,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
                'parent_id' => $MainPermission->id,
            ],

            [
                'title' => 'Delete',
                'name' => 'process2_delete',
                'guard_name' => 'web',
                'main_group' => 0,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
                'parent_id' => $MainPermission->id,
            ],

        ]);

        // Assign Permission to 'administrator' role
        $role = Role::findById(1, 'web');
        $role->givePermissionTo('process2_manage');
        $role->givePermissionTo('process2_create');
        $role->givePermissionTo('process2_edit');
        $role->givePermissionTo('process2_details');
        $role->givePermissionTo('process2_delete');

        \App\Models\Admin\Process2::insert([
            [
                'name' => '18kt White Gold',
                'nameKey' => '18WG',
                'metal_type' => 'Gold',
                'unit' => '18',
                'per_gram' => '0',
                'labour_charges' => '5',
                'user_id' => '1',
            ],
            [
                'name' => '18kt Yellow Gold',
                'nameKey' => '22YG',
                'metal_type' => 'Gold',
                'unit' => '22',
                'per_gram' => '0',
                'labour_charges' => '5',
                'user_id' => '1',
            ],
            [
                'name' => '	21 Kt Yellow Gold',
                'nameKey' => '21YG',
                'metal_type' => 'Gold',
                'unit' => '21',
                'per_gram' => '0',
                'labour_charges' => '3',
                'user_id' => '1',
            ],
            [
                'name' => '	18Kt Yellow Gold',
                'nameKey' => '18YG',
                'metal_type' => 'Gold',
                'unit' => '18',
                'per_gram' => '0',
                'labour_charges' => '3',
                'user_id' => '1',
            ],
            [
                'name' => '18 Kt White Gold (Palladium)',
                'nameKey' => '18WGP',
                'metal_type' => 'Gold',
                'unit' => '18',
                'per_gram' => '0',
                'labour_charges' => '5',
                'user_id' => '1',
            ],
            [
                'name' => '18Kt Red Gold',
                'nameKey' => '18RG',
                'metal_type' => 'Gold',
                'unit' => '18',
                'per_gram' => '0',
                'labour_charges' => '5',
                'user_id' => '1',
            ],
            [
                'name' => '9kt Yellow Gold',
                'nameKey' => '9YG',
                'metal_type' => 'Gold',
                'unit' => '18',
                'per_gram' => '0',
                'labour_charges' => '2',
                'user_id' => '1',
            ],
            [
                'name' => '9kt White Gold',
                'nameKey' => '9WG',
                'metal_type' => 'Gold',
                'unit' => '9',
                'per_gram' => '0',
                'labour_charges' => '2',
                'user_id' => '1',
            ],
            [
                'name' => '9kt Red Gold',
                'nameKey' => '9RG',
                'metal_type' => 'Gold',
                'unit' => '9',
                'per_gram' => '0',
                'labour_charges' => '2',
                'user_id' => '1',
            ],
            [
                'name' => 'Silver',
                'nameKey' => 'XAG',
                'metal_type' => 'Silver',
                'unit' => '925',
                'per_gram' => '0',
                'labour_charges' => '5',
                'user_id' => '1',
            ],
            [
                'name' => 'Platinum',
                'nameKey' => 'Plat',
                'metal_type' => 'Platinum',
                'unit' => '950',
                'per_gram' => '0',
                'labour_charges' => '3',
                'user_id' => '1',
            ],
            [
                'name' => 'Palladium',
                'nameKey' => 'PD',
                'metal_type' => 'Platinum',
                'unit' => '950',
                'per_gram' => '5',
                'labour_charges' => '5',
                'user_id' => '1',
            ],
            [
                'name' => '	Brass',
                'nameKey' => 'BRs',
                'metal_type' => 'Gold',
                'unit' => '0',
                'per_gram' => '0',
                'labour_charges' => '0',
                'user_id' => '1',
            ],
            [
                'name' => '22ct Yellow Gold',
                'nameKey' => '22ct yellow gold',
                'metal_type' => 'Gold',
                'unit' => '916',
                'per_gram' => '0',
                'labour_charges' => '10',
                'user_id' => '1',
            ],
        ]);
    }
}
