<div class="form-group  col-md-5 @if($errors->has('name')) has-error @endif">
    {!! Form::label('name', 'Name *', ['class' => 'control-label']) !!}
    {!! Form::text('name', old('name'), ['class' => 'form-control form-control-sm',  'placeholder' => 'Enter Name','required'=>'true','maxlength' =>100]) !!}
    {!! Form::text('update_id', null, ['class' => 'form-control form-control-sm', 'id'=>'update_id','hidden' => 'true']) !!}
    @if($errors->has('name'))
        <span class="help-block">
                                        {{ $errors->first('name') }}
                                    </span>
    @endif

</div>

<div class="form-group  col-md-5 @if($errors->has('nameKey')) has-error @endif">
    {!! Form::label('nameKey', 'Short Name *', ['class' => 'control-label']) !!}
    {!! Form::text('nameKey', old('nameKey'), ['class' => 'form-control form-control-sm',  'placeholder' => 'Enter Short Name','required'=>'true','maxlength' =>100]) !!}
    @if($errors->has('nameKey'))
        <span class="help-block">
                                        {{ $errors->first('nameKey') }}
                                    </span>
    @endif

</div>

<div class="form-group  col-md-5 @if($errors->has('metal_type')) has-error @endif">
    {!! Form::label('metal_type', 'Metal Type *', ['class' => 'control-label']) !!}
    {!! Form::select('metal_type',config('status.metal_type'), old('metal_type'), ['class' => 'form-control form-control-sm',  'placeholder' => 'Select Metal Type','required'=>'true','maxlength' =>100]) !!}
    @if($errors->has('metal_type'))
        <span class="help-block">
                                        {{ $errors->first('metal_type') }}
                                    </span>
    @endif

</div>

<div class="form-group  col-md-5 @if($errors->has('unit')) has-error @endif">
    {!! Form::label('unit', 'Unit *', ['class' => 'control-label']) !!}
    {!! Form::number('unit', old('unit'), ['class' => 'form-control form-control-sm',  'placeholder' => 'Enter Unit','required'=>'true','maxlength' =>100]) !!}
    @if($errors->has('unit'))
        <span class="help-block">
                                        {{ $errors->first('unit') }}
                                    </span>
    @endif

</div>

<div class="form-group  col-md-5 @if($errors->has('per_gram')) has-error @endif">
    {!! Form::label('per_gram', 'Per Gram *', ['class' => 'control-label']) !!}
    {!! Form::number('per_gram', old('per_gram'), ['class' => 'form-control form-control-sm',  'placeholder' => 'Enter Name Key','required'=>'true','maxlength' =>100]) !!}
    @if($errors->has('per_gram'))
        <span class="help-block">
                                        {{ $errors->first('per_gram') }}
                                    </span>
    @endif

</div>

<div class="form-group  col-md-5 @if($errors->has('labour_charges')) has-error @endif">
    {!! Form::label('labour_charges', 'Labour Charges *', ['class' => 'control-label']) !!}
    {!! Form::number('labour_charges', old('labour_charges'), ['class' => 'form-control form-control-sm',  'placeholder' => 'Enter Labour Charges','required'=>'true','maxlength' =>100]) !!}
    @if($errors->has('labour_charges'))
        <span class="help-block">
                                        {{ $errors->first('labour_charges') }}
                                    </span>
    @endif

</div>
