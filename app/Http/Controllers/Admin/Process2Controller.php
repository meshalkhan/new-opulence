<?php


namespace App\Http\Controllers\Admin;

use App\Helpers\PermissionHelper as Per;
use App\Models\Admin\Process2;
use App\User;
use Illuminate\Http\Request;
use DB;
use Auth;
use Form;

class Process2Controller
{
    public function index()
    {
        if (! Per::has_permission('process2_details')) {
            return redirect()->route('home');
        }

        return view('admin.process2.index');
    }

    public function create(Request $request)
    {
        if (! Per::has_permission('process2_create')) {
            return redirect()->route('home');
        }

        $name=$request->name;
        $process2 = Process2::where('name',$name)->get();
        if(count($process2)>0)
        {
            session()->flash('success', 'Metal Already Exists.');
            return redirect()->route('admin.process2.index');
        }
        else
        {
            $data = $request->all();
            $current_date_time = \Carbon\Carbon::now();
            $current_date_time->timezone = config('global.time_zone');
            $data['created_by'] = Auth::user()->id;
            $data['user_id'] = Auth::user()->id;
            $data['created_at'] = $current_date_time;
            Process2::create($data);
            session()->flash('success', 'Record has been created successfully.');
            return redirect()->route('admin.process2.index');
        }

    }

    public function fetchdata(Request $request)
    {
        $data = Process2::where('id',$request['id'])->get();
        return response()->json($data);
    }

    public function editrow(Request $request)
    {
        if (! Per::has_permission('process2_edit')) {
            return redirect()->route('home');
        }
        $data = $request->all();
        $id = $request['id'];
        $current_date_time = \Carbon\Carbon::now();
        $current_date_time->timezone = config('global.time_zone');
        $data['updated_by'] = Auth::user()->id;
        $data['updated_at'] = $current_date_time;
        $process2_name=$request->name;
        $process2count = Process2::where('id','!=',$id)->where('name',$process2_name)->get();
        if(count($process2count)>0)
        {
            $response='repeat';
        }
        else if($data['name']=='' || $data['nameKey']==''|| $data['metal_type']==''|| $data['unit']==''|| $data['per_gram']==''|| $data['labour_charges']=='' )
        {
            $response='null';
        }
        else
        {
            $datas['updated_by'] = Auth::user()->id;
            Process2::where('id',$id)->update([
                'name'=>$request['name'],
                'nameKey'=>$request['nameKey'],
                'metal_type'=>$request['metal_type'],
                'unit'=>$request['unit'],
                'per_gram'=>$request['per_gram'],
                'labour_charges'=>$request['labour_charges'],
            ]);
            $response='success';

        }
        return response()->json($response);

    }

    public function deleterow(Request $request)
    {
        if (! Per::has_permission('process2_delete')) {
            return redirect()->route('home');
        }

        $id = $request->id;
        $process2s = Process2::where('id',$id)->forceDelete();
    }

    public function allprocess2(Request $request,$id)
    {
        $columns = array(
            0 =>'id',
            1 =>'name',
            2 =>'nameKey',
            3 =>'metal_type',
            4 =>'unit',
            5 =>'per_gram',
            6 =>'labour_charges',
            7=> 'options',
        );

        $totalData = Process2::count();

        $totalFiltered = $totalData;

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');

        if(empty($request->input('search.value')))
        {
            $lists = Process2::offset($start)
                ->limit($limit)
                ->orderBy('name','asc')
                ->get();
        }
        else
        {
            $search = $request->input('search.value');
            $lists =  Process2::where('name', 'LIKE',"%{$search}%")
                ->offset($start)
                ->limit($limit)
                ->orderBy($order,$dir)
                ->get();

            $totalFiltered = Process2::where('name', 'LIKE',"%{$search}%")
                ->count();
        }

        $data = array();

        $i=1;
        if(!empty($lists)) {
            foreach ($lists as $list) {


                $nestedData['id'] = "&nbsp&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;".$i;

                $nestedData['name'] = $list->name;
                $nestedData['nameKey'] = $list->nameKey;
                $nestedData['metal_type'] = $list->metal_type;
                $nestedData['unit'] = $list->unit;
                $nestedData['per_gram'] = $list->per_gram;
                $nestedData['labour_charges'] = $list->labour_charges;
                $options="";
                if (Per::has_permission('process2_delete')) {

                    $options .= "<a class='btn btn-sm btn-danger' style='cursor:pointer;color:white;margin-left:2px;' onclick='rowdelete(\"" . $list->id . "\",\"0\")' ><i class='fas fa-trash-alt' ></i></button>";

                }
                if (Per::has_permission('process2_edit')) {
                    $options .= "<a class='btn btn-sm btn-warning' role='button' data-toggle='modal' data-target='#modal-process2' style='cursor:pointer;margin-left:2px;' onclick='editrow(\"" . $list->id . "\")' ><i class='fas fa-edit' ></i></button>";
                }

                $nestedData['options'] = $options;
                $data[] = $nestedData;
                $i++;

            }
        }

        $json_data = array(
            "draw"            => intval($request->input('draw')),
            "recordsTotal"    => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data"            => $data
        );
        echo json_encode($json_data);

    }
}
