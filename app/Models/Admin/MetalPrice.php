<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MetalPrice extends Model
{
    use SoftDeletes;
    //
    protected $fillable = ['id',
        'goldPrice',
        'goldAdd',
        'goldVat',
        'perGramGoldPrice',
        'silverPrice',
        'silverAdd',
        'silverVat',
        'perGramSilverPrice',
        'platinumPrice',
        'platinumAdd',
        'platinumVat',
        'perGramPlatinumPrice',
        'date',
        'status',
        'user_id',
        'created_by',
        'updated_by', 'deleted_by'];
    protected $table = 'metalprices';
}
