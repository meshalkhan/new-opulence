@inject('request', 'Illuminate\Http\Request')
<?php $per = PermissionHelper::getUserPermissions();?>
@extends('layouts.user')

@section('content')
    <!-- Main content -->
    <section class="content" style="padding-top: 40px;">
        <div class="card">

            <div class="card-header"  style=" background-color: rgba(231,240,230,0.65)">

                {{--<h3 class="card-title" style="font-weight: bold;color: #0c5460">Add New Ticket</h3>--}}
                <h3 style="color: white;cursor: pointer;background-color: #069ba9;border:1px solid #069ba9 " class="btn btn-sm">Account</h3>
                <h3 style="color: white;cursor: pointer;background-color: #069ba9;border:1px solid #069ba9 " class="btn btn-sm">Cash</h3>
                <h3 style="color: white;cursor: pointer;background-color: #069ba9;border:1px solid #069ba9 " class="btn btn-sm">Online</h3>
                <div class="form-group float-right col-md-5 @if($errors->has('cutomer_id')) has-error @endif">
                    {!! Form::text('cutomer_id',old('cutomer_id'), ['class' => 'form-control form-control-sm',  'placeholder' => 'Customer','required'=>'true','maxlength' =>100]) !!}
                    @if($errors->has('cutomer_id'))
                        <span class="help-block">
                                        {{ $errors->first('cutomer_id') }}
                                    </span>
                    @endif

                </div>

            </div>

            <div class="card-body">
                <div class="card-body" style="align-items: center">
                    <form class="form" id="ticketForm">
                            <div class="row">
                                <div class="col-xl-12">
                                    <div class="row">
                                        <div class="col-lg-3">
                                            <div class="card">
                                                <div class="card-header bg-gradient-teal">
                                                    <text>Item</text>
                                                </div>
                                                <div class="card-body" style="height: 250px;overflow-y: auto;">
                                                    <div class="row">
                                                        @foreach ($processes1 as $process1)
                                                        {{--<div class=">--}}
                                                            <div class=" col-md-4 card btn btn-sm process1-boxes" style="cursor:pointer;" onclick="process1Select($(this),'{{$process1['id']}}','{{$process1['name']}}')">
                                                                    <text style="font-size: 12px;">{{$process1['name']}}</text>
                                                            </div>
                                                        {{--</div>--}}
                                                        @endforeach
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-3">
                                            <div class="card">
                                                <div class="card-header bg-gradient-teal">
                                                    <text>Metal</text>
                                                </div>
                                                <div class="card-body" style="height: 250px;overflow-y: auto;">
                                                    <div class="row">
                                                        <?php foreach ($processes2 as $process2){ ?>
                                                            <div class="col-md-4 card btn btn-sm process2-boxes" style="cursor:pointer;" onclick="process2Select($(this),'<?php echo $process2['id']?>','<?php echo $process2['name']?>')">
                                                                <text style="font-size: 12px;"><?php echo $process2['name'];?></text>
                                                            </div>
                                                        <?php } ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-3">
                                            <div class="card">
                                                <div class="card-header bg-gradient-teal">
                                                    <text>Process</text>
                                                </div>
                                                <div class="card-body" style="height: 250px;overflow-y: auto;">
                                                    <div class="row">
                                                        <?php foreach ($processes3 as $process3){ ?>

                                                            <div class="col-md-4 card btn btn-sm process3-boxes" style="cursor:pointer;" onclick="process3Select($(this),'<?php echo $process3['id']?>','<?php echo $process3['name']?>')">
                                                                <text style="font-size: 12px;"><?php echo $process3['name'];?></text>
                                                            </div>

                                                        <?php } ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-3">
                                            <div class="card">
                                                <div class="card-header bg-gradient-teal">
                                                    <text>Further Process</text>
                                                </div>
                                                <div class="card-body" style="height: 250px;overflow-y: auto;">
                                                    <div class="row">
                                                        <?php foreach ($processes4 as $process4){ ?>
                                                            <div class="col-md-4 card btn btn-sm process4-boxes" style="cursor:pointer;" onclick="process4Select($(this),'<?php echo $process4['id']?>','<?php echo $process4['name']?>')">
                                                                <text style="font-size: 12px;"><?php echo $process4['name'];?></text>
                                                            </div>
                                                        <?php } ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="card">
                                                <div class="card-header bg-gradient-teal">
                                                    <text>Description</text>
                                                </div>
                                                <div class="card-body custom-card-body" style="height: 250px;overflow-y: auto;">
                                                    <textarea name="description" id="description" class="form-control" rows="10"></textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="card">
                                                <div class="card-header bg-gradient-teal">
                                                    <text>Notes</text>
                                                </div>
                                                <div class="card-body custom-card-body" style="height: 250px;overflow-y: auto;">
                                                    <textarea name="note" id="note" class="form-control" rows="10"></textarea>
                                                </div>
                                            </div>

                                        </div>
                                        {{--<div class="col-lg-3">--}}
                                            {{--<video id="video" width="360" height="260" autoplay></video>--}}
                                            {{--<button id="snap" type="button" class="btn btn-primary">Capture</button>--}}
                                        {{--</div>--}}
                                        {{--<div class="col-lg-3">--}}
                                            {{--<canvas id="canvas" width="360" height="260"></canvas>--}}
                                            {{--<input type="hidden" id="pic">--}}
                                        {{--</div>--}}



                                    </div>
                                    @include('admin.ticket.fields')

                                </div>
                            </div>
                    </form>
                </div>
                <div>
                    @if(in_array('ticket_details',$per))
                        <table id="add_ticket" class="display dataTable" style="word-wrap: break-word;width:100%;font-size: 13px;" >
                            <thead>
                            <tr  style="background-color: #878ebf;color: white">


                                <th width="">Description</th>
                                <th width="">Note</th>
                                <th width="">Reference</th>
                                <th width="">Qunatity</th>
                                <th width="">Total Price</th>
                                <th width="">Image</th>
                                <th width="">Actions</th>
                            </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    @endif
                </div>
            </div>
        </div>

    @include('admin.ticket.js')
    @include('admin.ticket.js1')
    <!-- /.card -->
    </section>
    <br/>
    <!-- /.content -->

@endsection

