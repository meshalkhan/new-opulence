<div class="form-group  col-md-4 @if($errors->has('nickName')) has-error @endif">
    {!! Form::label('nickName', 'Nick Name *', ['class' => 'control-label']) !!}
    {!! Form::text('nickName',old('nickName'), ['class' => 'form-control form-control-sm',  'placeholder' => 'Enter Nick Name','required'=>'true','maxlength' =>100]) !!}
    {!! Form::text('update_id', null, ['class' => 'form-control form-control-sm', 'id'=>'update_id','hidden' => 'true']) !!}
    @if($errors->has('nickName'))
        <span class="help-block">
                                        {{ $errors->first('nickName') }}
                                    </span>
    @endif

</div>

<div class="form-group  col-md-4 @if($errors->has('firstName')) has-error @endif">
    {!! Form::label('firstName', 'First Name *', ['class' => 'control-label']) !!}
    {!! Form::text('firstName', old('firstName'), ['class' => 'form-control form-control-sm',  'placeholder' => 'Enter First Name','required'=>'true','maxlength' =>100]) !!}
    @if($errors->has('firstName'))
        <span class="help-block">
                                        {{ $errors->first('firstName') }}
                                    </span>
    @endif

</div>

<div class="form-group  col-md-4 @if($errors->has('lastName')) has-error @endif">
    {!! Form::label('lastName', 'Last Name *', ['class' => 'control-label']) !!}
    {!! Form::text('lastName', old('lastName'), ['class' => 'form-control form-control-sm',  'placeholder' => 'Enter Last Name','required'=>'true','maxlength' =>100]) !!}
    @if($errors->has('lastName'))
        <span class="help-block">
                                        {{ $errors->first('lastName') }}
                                    </span>
    @endif

</div>
<div class="form-group  col-md-4 @if($errors->has('companyName')) has-error @endif">
    {!! Form::label('companyName', 'Company Name *', ['class' => 'control-label']) !!}
    {!! Form::text('companyName', old('companyName'), ['class' => 'form-control form-control-sm',  'placeholder' => 'Enter Company Name','required'=>'true','maxlength' =>100]) !!}
    @if($errors->has('companyName'))
        <span class="help-block">
                                        {{ $errors->first('companyName') }}
                                    </span>
    @endif

</div>
<div class="form-group  col-md-4 @if($errors->has('vatNumber')) has-error @endif">
    {!! Form::label('vatNumber', 'Vat Number *', ['class' => 'control-label']) !!}
    {!! Form::text('vatNumber', old('vatNumber'), ['class' => 'form-control form-control-sm',  'placeholder' => 'Enter Vat Number','required'=>'true','maxlength' =>100]) !!}
    @if($errors->has('vatNumber'))
        <span class="help-block">
                                        {{ $errors->first('vatNumber') }}
                                    </span>
    @endif

</div>
<div class="form-group  col-md-4 @if($errors->has('email')) has-error @endif">
    {!! Form::label('email', 'Email *', ['class' => 'control-label']) !!}
    {!! Form::text('email', old('email'), ['class' => 'form-control form-control-sm',  'placeholder' => 'Enter Email','required'=>'true','maxlength' =>100]) !!}
    @if($errors->has('email'))
        <span class="help-block">
                                        {{ $errors->first('email') }}
                                    </span>
    @endif

</div>
<div class="form-group  col-md-4 @if($errors->has('password')) has-error @endif">
    {!! Form::label('password', 'Password *', ['class' => 'control-label']) !!}
    {!! Form::input('password','password', old('password'), ['class' => 'form-control form-control-sm',  'placeholder' => 'Enter Password','required'=>'true','maxlength' =>100]) !!}
    @if($errors->has('password'))
        <span class="help-block">
                                        {{ $errors->first('password') }}
                                    </span>
    @endif

</div>

<div class="form-group  col-md-4 @if($errors->has('phone')) has-error @endif">
    {!! Form::label('phone', 'Phone *', ['class' => 'control-label']) !!}
    {!! Form::text('phone',old('phone'), ['class' => 'form-control form-control-sm phone','required'=>'true','maxlength' =>100]) !!}
    @if($errors->has('phone'))
        <span class="help-block">
                                        {{ $errors->first('phone') }}
                                    </span>
    @endif

</div>

<div class="form-group  col-md-4 @if($errors->has('mobile')) has-error @endif">
    {!! Form::label('mobile', 'Mobile *', ['class' => 'control-label']) !!}
    {!! Form::text('mobile', old('mobile'), ['class' => 'form-control form-control-sm mobile',  'placeholder' => 'Enter Mobile','required'=>'true','maxlength' =>100]) !!}
    @if($errors->has('mobile'))
        <span class="help-block">
                                        {{ $errors->first('mobile') }}
                                    </span>
    @endif

</div>
<div class="form-group  col-md-6 @if($errors->has('address')) has-error @endif">
    {!! Form::label('address', 'Address *', ['class' => 'control-label']) !!}
    {!! Form::text('address', old('address'), ['class' => 'form-control form-control-sm',  'placeholder' => 'Enter Address','required'=>'true','maxlength' =>100]) !!}
    @if($errors->has('address'))
        <span class="help-block">
                                        {{ $errors->first('address') }}
                                    </span>
    @endif

</div>
<div class="form-group  col-md-4 @if($errors->has('creditLimit')) has-error @endif">
    {!! Form::label('creditLimit', 'Credit Limit *', ['class' => 'control-label']) !!}
    {!! Form::number('creditLimit', old('creditLimit'), ['class' => 'form-control form-control-sm',  'placeholder' => 'Enter Credit Limit','required'=>'true','maxlength' =>100]) !!}
    @if($errors->has('creditLimit'))
        <span class="help-block">
                                        {{ $errors->first('creditLimit') }}
                                    </span>
    @endif

</div>


