<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script type="text/javascript">

    $(".select2").select2({
        //placeholder: "Select a Name",
        allowClear: true
    });

</script>
<script type="text/javascript">

    $(document).ready(function ()
    {
        document.getElementById('add_process2').style.display = "block";
        document.getElementById('edit_process2').style.display = "none";
        $('#clearall').show();

        var table= $('#process2_datatables').DataTable({
            "pageLength": 25,
            "processing": true,
            "serverSide": true,
            "ajax":{
                "type": "GET",
                "url": "{{ route('admin.process2.allprocess2',['archive']) }}'",
                "dataType": "json",
                "data":{ _token: "{{csrf_token()}}"}

            },
            "columnDefs": [{
                orderable: false,
                "targets": '_all',
                "createdCell": function (td, cellData, rowData, row, col) {
                    $(td).css('padding-top', '2px')
                    $(td).css('padding-left', '8px')
                    $(td).css('padding-bottom', '2px')
                    $(td).css('padding-right', '8px')
                }
            }],
            "columns": [
                {
                    "className":      'details-control',
                    "data": "id"
                },
                { "data": "name" },
                { "data": "nameKey" },
                { "data": "metal_type" },
                { "data": "unit" },
                { "data": "per_gram" },
                { "data": "labour_charges" },
                {
                    "data": "options"
                },
            ],
            "order": [[1, 'asc']]

        });
    });

    var clearall = function ()
    {
        $('#save').attr('disabled', false);
        document.getElementById('add_process2').style.display = "block";
        $('#clearall').show();
        document.getElementById('edit_process2').style.display = "none";
        $('#process2_datatables').DataTable().ajax.reload();
        $('#name').val('');
        $('#nameKey').val('');
        $('#metal_type').val('');
        $('#unit').val('');
        $('#labour_charges').val('');
        $('#per_gram').val('');
    }

    // Delete row

    var rowdelete = function (id,find)
    {
        if(find=='0')
        {
            var title='You will not be able to revert this !';

        }
        swal({
            title: "Are you sure?",
            text: title,
            icon: "warning",
            buttons: [
                'No, cancel it!',
                'Yes, I am sure!'
            ],
            dangerMode: true,
        }).then(function(isConfirm) {
            if (isConfirm) {

                $.ajax({
                    type: 'POST',
                    url: '{{ route('admin.process2.deleterow') }}',
                    data: {
                        _token: '{{ csrf_token() }}',
                        id: id,
                    },
                    success: function (response) {
                        swal({
                            title: 'Deleted!',
                            text: 'Record has been Deleted Successfully.',
                            icon: 'success',
                            timer: 2000,
                            button: false,
                        }).then(function() {
                        });
                        clearall();
                    }
                });
            } else {
                swal("Cancelled", "Your data is safe :)", "error");
            }
        });
    };

    // Edit Row
    var updaterow = function ()
    {
        var id = $('#update_id').val();
        var nameKey=$('#nameKey').val();
        var name=$('#name').val();
        var metal_type=$('#metal_type').val();
        var unit=$('#unit').val();
        var per_gram=$('#per_gram').val();
        var labour_charges=$('#labour_charges').val();

        $.ajax({
            type: 'POST',
            url: '{{ route("admin.process2.editrow") }}',
            data: {
                _token: '{{ csrf_token() }}',
                id: id,
                nameKey: nameKey,
                name: name,
                metal_type: metal_type,
                unit: unit,
                per_gram: per_gram,
                labour_charges: labour_charges
            },

            success: function (response)
            {
                if(response=='success')
                {
                    swal({
                        title: 'Data Updated',
                        type: 'success',
                        text: 'Record has been Updated Successfully',
                        icon: 'success',
                        timer: 2000,
                        button: false,
                    }).then(
                        function () {},
                        // handling the promise rejection
                        function (dismiss) {
                            if (dismiss === 'timer') {
                            }
                        }
                    );
                    location.reload();
                }
                else if(response=='repeat')
                {
                    var title='Metal Already Exists.!';
                    swal("Type Again", title, "error");
                }
                else if(response=='null')
                {
                    var title='Some Fields Empty. Try Again!';
                    swal("Type Again", title, "error");
                }


                $('#process2_datatables').DataTable().ajax.reload();
            }
        });
    };

    var editrow = function (id) {
        $('#save').attr('disabled', true);

        $.ajax({
            type: 'POST',
            url: '{{ route('admin.process2.fetchdata') }}',
            data: {
                _token: '{{ csrf_token() }}',
                id: id,
            },

            success: function (response) {
                jQuery('html,body').animate({scrollTop:0},0);
                document.getElementById('add_process2').style.display = "none";
                document.getElementById('edit_process2').style.display = "block";
                document.getElementById('clearall').style.display = "block";
                $('#update_id').val(response[0].id);
                $('#nameKey').val(response[0].nameKey);
                $('#name').val(response[0].name);
                $('#metal_type').val(response[0].metal_type);
                $('#unit').val(response[0].unit);
                $('#per_gram').val(response[0].per_gram);
                $('#labour_charges').val(response[0].labour_charges);

            }
        });

    };

    var status_filter= function ()
    {
        $('#process2_datatables').DataTable().ajax.reload();
    }

</script>
