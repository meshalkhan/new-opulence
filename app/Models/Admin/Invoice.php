<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Invoice extends Model
{
    use SoftDeletes;
    //
    protected $fillable = ['id',
        'customer_id',
        'amount',
        'remainingAmount',
        'method',
        'createdDate',
        'created_by',
        'updated_by', 'deleted_by'];
    protected $table = 'invoices';
}

