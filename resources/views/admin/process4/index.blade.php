@inject('request', 'Illuminate\Http\Request')
<?php $per = PermissionHelper::getUserPermissions();?>
@extends('layouts.admin')

@section('content')

    <section class="content-header" style="height: 50px;margin-left:40px ; ">
        @if($message = Session::get('success'))
            {{--<div class="toast alert-info" style="padding: 4px;text-align: center;">{{ Session::get('success') }}</div>--}}
        <input id="datareturn" value="{{ Session::get('success') }}" hidden/>
            <script>
                $(document).ready(function ()
                {
                    var datareturn = $('#datareturn').val();

                    if(datareturn=="Record has been created successfully.")
                    {
                        swal({
                            title: 'Data Saved',
                            type: 'success',
                            text: datareturn+'!',
                            icon: 'success',
                            timer: 2000,
                            button: false,
                        }).then(
                            function () {},
                            // handling the promise rejection
                            function (dismiss) {
                                if (dismiss === 'timer') {
                                }
                            }
                        )

                    }else
                    {
                        swal({
                            title: 'Data Not Saved',
                            type: 'error',
                            text: datareturn+'!',
                            icon: 'error',
                            timer: 2000,
                            button: false,
                        }).then(
                            function () {},
                            // handling the promise rejection
                            function (dismiss) {
                                if (dismiss === 'timer') {
                                }
                            }
                        )
                    }

                });
            </script>

        @endif
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="card card-secondary">

            <div class="card-header">

                <h4 style="font-weight: bold;font-size: 16px;" class="card-title">Further Processes List</h4>


                    <div id="clearall" class="card-tools">
                        @if(in_array('process4_create',$per))
                        <a style="color: white;cursor: pointer;background-color: #069ba9;border:1px solid #069ba9 " class="btn btn-sm"  role="button" data-toggle="modal" data-target="#modal-process4" onclick="clearall()">Add New</a>
                        @endif
                    </div>

            </div>

            <div class="card-body">

            <!-- /.card-body -->
                <div>
                    @if(in_array('process4_details',$per))
                        <table id="process4_datatables" class="display dataTable" style="width:100%;font-size: 13px;" >
                            <thead>
                            <tr >
                                <th width="">ID</th>
                                <th width="">Name</th>
                                <th width="">Short Name</th>
                                <th width="">Action</th>
                            </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    @endif
                </div>
            </div>
        </div>

    @include('admin.process4.js')
    <!-- /.card -->
    </section>

    <div class="modal fade" id="modal-process4" tabIndex="-2" role="dialog" aria-hidden="true" style="margin-top: 70px;">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header btn" style="background-color: #b46b72;color: white">
                   <strong> <h5 class="modal-title">Process</h5></strong>
                </div>
                {!! Form::open(['method' => 'POST', 'route' => ['admin.process4.create'], 'autocomplete'=>'off' ,'data-parsley-validate']) !!}
                <div class="modal-body">
                    <div  class="row">

                            @include('admin.process4.fields')


                    </div>
                </div>
                <div class="modal-footer">

                    <button class="btn btn-sm btn-secondary" data-dismiss="modal" aria-hidden="true">Cancel</button>
                    <div id="add_process4">
                        {!! Form::submit(trans('Save'), ['id'=>'save','style'=>'color: white;cursor: pointer','class' => 'btn btn-sm btn-success']) !!}
                    </div>
                    <div id="edit_process4" style="display: none">
                        <a class="btn btn-sm btn-danger" style="color: white;cursor: pointer" onclick="updaterow()">Update</a>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
    <br/>
    <!-- /.content -->

@endsection

