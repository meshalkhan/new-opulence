@inject('request', 'Illuminate\Http\Request')
<?php $per = PermissionHelper::getUserPermissions();?>
@extends('layouts.user')

@section('content')

    <section class="content-header" style="height: 50px;margin-left:30px ; ">
        @if($message = Session::get('success'))
        <input id="datareturn" value="{{ Session::get('success') }}" hidden/>
            <script>
                $(document).ready(function ()
                {
                    var datareturn = $('#datareturn').val();
                    if(datareturn=="Record has been created successfully.")
                    {
                        swal({
                            title: 'Data Saved',
                            type: 'success',
                            text: datareturn+'!',
                            icon: 'success',
                            timer: 2000,
                            button: false
                        }).then(
                            function () {},
                            function (dismiss) {
                                if (dismiss === 'timer') {
                                }
                            }
                        )
                    }else
                    {
                        swal({
                            title: 'Data Not Saved',
                            type: 'error',
                            text: datareturn+'!',
                            icon: 'error',
                            timer: 2000,
                            button: false
                        }).then(
                            function () {},
                            function (dismiss) {
                                if (dismiss === 'timer') {
                                }
                            }
                        )
                    }

                });
            </script>

        @endif
    </section>

    <section class="content">
        <div class="card card-secondary">

            <div class="card-header">
                <h3 style="font-weight: bold;font-size: 16px;" class="card-title">Ticket List</h3>
                    <div id="clearall" class="card-tools">
                        @if(in_array('ticket_create',$per))
                        <a style="color: white;cursor: pointer;background-color: #069ba9;border:1px solid #069ba9 " class="btn btn-sm"  href="{{route('admin.ticket.add')}}" >Add New</a>
                        @endif
                    </div>
            </div>

            <div class="card-body">
                <div>
                    @if(in_array('ticket_details',$per))
                        <table id="ticket_datatables" class="display dataTable" style="word-wrap: break-word;width:100%;font-size: 13px;" >
                            <thead>
                            <tr>
                                <th width="">Barcode</th>
                                <th width="">Customer Name</th>
                                <th width="">Image</th>
                                <th width="">Total Amount</th>
                                <th width="">Status</th>
                                <th width="">Paid Status</th>
                                <th width="">Paid Date</th>
                                <th width="">Complete Job</th>
                                <th width="">Pay Now</th>
                                <th width="">Action</th>
                            </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    @endif
                </div>
            </div>
        </div>

    @include('admin.ticket.js')
    </section>
    <br/>
@endsection

