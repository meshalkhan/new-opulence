<?php


namespace App\Http\Controllers\Admin;

use App\Helpers\PermissionHelper as Per;
use App\Models\Admin\Heading;
use App\Models\Admin\Customer;
use App\Models\Admin\RolePermission;
use App\Models\Admin\SubHeading;
use App\User;
use Illuminate\Http\Request;
use DB;
use Auth;
use Form;
use Spatie\Permission\Models\Role;

class CustomerController
{
    public function index()
    {
        if (! Per::has_permission('customer_details')) {
            return redirect()->route('home');
        }

        return view('admin.customer.index');
    }

    public function create(Request $request)
    {
        if (! Per::has_permission('customer_create')) {
            return redirect()->route('home');
        }
        $data = $request->all();
        $current_date_time = \Carbon\Carbon::now();
        $current_date_time->timezone = config('global.time_zone');
        $data['created_by'] = Auth::user()->id;
        $data['user_id'] = Auth::user()->id;
        $data['created_at'] = $current_date_time;

        $email = $request->email;
        $emailcheck = Customer::where('email',$email)->get();
        if(count($emailcheck)>=1)
        {
            session()->flash('success', 'Email Already Exists.');
            return redirect()->route('admin.users.create');
        }
        else
        {

            $role_name = 'Customer';
            $role_id = Role::where('name', $role_name)->first()->id;

            $perm = RolePermission::where('id', $role_id)->pluck('permissions')->first();

            $datau['name'] =  $request->firstName.' '.$request->lastName;
            $datau['email'] = trim($request->email);
            $datau['role_id'] = $role_id;
            $datau['permissions'] = $perm;
            $datau['portal_id'] = '2';
            $datau['password'] = bcrypt($data['password']);
            $datau['created_by'] = Auth::user()->id;
            $datau['updated_by'] = Auth::user()->id;

            $user_id = User::create($datau)->id;
            $data['user_id'] = $user_id;
            Customer::create($data);
            session()->flash('success', 'Record has been created successfully.');
            return redirect()->route('admin.customer.index');
        }

    }

    public function fetchdata(Request $request)
    {
        $data = Customer::where('id',$request['id'])->get();
        return response()->json($data);
    }

    public function editrow(Request $request)
    {
        if (! Per::has_permission('customer_edit')) {
            return redirect()->route('home');
        }
        $data = $request->all();
        $id = $request['id'];
        $current_date_time = \Carbon\Carbon::now();
        $current_date_time->timezone = config('global.time_zone');
        $data['updated_by'] = Auth::user()->id;
        $data['updated_at'] = $current_date_time;

        if($data['nickName']=='' ||$data['firstName']=='' ||$data['lastName']=='' ||$data['companyName']=='' ||
            $data['vatNumber']=='' ||$data['email']=='' ||$data['password']=='' ||$data['phone']=='' ||
            $data['mobile']=='' ||$data['address']==''||$data['creditLimit']=='' )
        {
            $response='null';
        }
        else
        {
            $email = $request->email;
            $emailcheck = Customer::where('email',$email)->where('id','!=',$id)->get();
            if(count($emailcheck)>=1)
            {
                $response='repeat';
            }
            else
            {
                $datas['updated_by'] = Auth::user()->id;
                Customer::where('id', $id)->update([
                    'nickName' => $request['nickName'],
                    'lastName' => $request['lastName'],
                    'firstName' => $request['firstName'],
                    'companyName' => $request['companyName'],
                    'vatNumber' => $request['vatNumber'],
                    'email' => $request['email'],
                    'password' => $request['password'],
                    'phone' => $request['phone'],
                    'mobile' => $request['mobile'],
                    'address' => $request['address'],
                    'creditLimit' => $request['creditLimit']
                ]);
                $response = 'success';
            }

        }
        return response()->json($response);

    }

    public function deleterow(Request $request)
    {
        if (! Per::has_permission('customer_delete')) {
            return redirect()->route('home');
        }

        $id = $request->id;
        $customers = Customer::where('id',$id)->forceDelete();
    }

    public function allcustomer(Request $request,$id)
    {


        $columns = array(
            0 =>'id',
            1 =>'nickName',
            2 =>'firstName',
            3 =>'lastName',
            4 =>'companyName',
            5 =>'vatNumber',
            6 =>'email',
            7 =>'phone',
            8 =>'mobile',
            9 =>'address',
            10 =>'creditLimit',
            11 => 'options',
        );

        $totalData = Customer::count();

        $totalFiltered = $totalData;

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');

        if(empty($request->input('search.value')))
        {
            $lists = Customer::offset($start)
                ->limit($limit)
                ->orderBy('firstName','asc')
                ->get();
        }
        else
        {
            $search = $request->input('search.value');
            $lists =  Customer::where('firstName', 'LIKE',"%{$search}%")
                ->offset($start)
                ->limit($limit)
                ->orderBy($order,$dir)
                ->get();

            $totalFiltered = Customer::where('firstName', 'LIKE',"%{$search}%")
                ->count();
        }

        $data = array();

        $i=1;
        if(!empty($lists)) {
            foreach ($lists as $list)
            {

                $nestedData['id'] = "&nbsp&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;".$i;
                $nestedData['nickName'] = $list->nickName;
                $nestedData['firstName'] = $list->firstName;
                $nestedData['lastName'] = $list->lastName;
                $nestedData['companyName'] = $list->companyName;
                $nestedData['vatNumber'] = $list->vatNumber;
                $nestedData['email'] = $list->email;
                $nestedData['password'] = $list->password;
                $nestedData['phone'] = $list->phone;
                $nestedData['mobile'] = $list->mobile;
                $nestedData['address'] = $list->address;
                $nestedData['creditLimit'] = $list->creditLimit;
                $options="";
                if (Per::has_permission('customer_delete')) {

                    $options .= "<a class='btn btn-sm btn-danger' style='cursor:pointer;color:white;margin-left:2px;' onclick='rowdelete(\"" . $list->id . "\",\"0\")' ><i class='fas fa-trash-alt' ></i></button>";

                }
                if (Per::has_permission('customer_edit')) {
                    $options .= "<a class='btn btn-sm btn-warning' role='button' data-toggle='modal' data-target='#modal-customer' style='cursor:pointer;margin-left:2px;' onclick='editrow(\"" . $list->id . "\")' ><i class='fas fa-edit' ></i></button>";
                }

                $nestedData['options'] = $options;
                $data[] = $nestedData;
                $i++;

            }
        }

        $json_data = array(
            "draw"            => intval($request->input('draw')),
            "recordsTotal"    => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data"            => $data
        );
        echo json_encode($json_data);

    }
}
