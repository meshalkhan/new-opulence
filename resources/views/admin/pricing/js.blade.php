<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

<script type="text/javascript">

    $(document).ready(function ()
    {
        $.ajax({
        type: 'POST',
        url: '{{ route('admin.pricing.getMetalPrices') }}',
            data: {
                _token: '{{ csrf_token() }}'
            },
        success: function (data) {

            console.log(data);
            perGramGoldCalculation(data.goldPrice, data.goldAdd, data.goldVat);
            perPlatinumCalculation(data.platinumPrice, data.platinumAdd, data.platinumVat);
            perSilverCalculation(data.silverPrice, data.silverAdd, data.silverVat);
            getPriceByMetalUnit();

        },
        error: function (data) {
            console.log(data);
        }
    });

    });

    var perGramGoldCalculation = function (price, add, vat)
    {
        var ounceToGram = 31.100.toFixed(3);

        price = parseFloat(price).toFixed(2);
        add = parseFloat(add).toFixed(2);
        vat = parseFloat(vat).toFixed(2);

        $("#showGoldOunceToGram").html(ounceToGram);
        $("#showGoldAddPercentage").html(add);
        $("#showGoldVatPercentage").html(vat);
        $("#showGoldPrice").html(price);

        var addPrice = parseFloat(price) * (parseFloat(add) / 100);
        addPrice = addPrice.toFixed(2);
        $("#showGoldAddPrice").html(addPrice);

        var grossPrice = parseFloat(price) + parseFloat(addPrice);
        grossPrice = grossPrice.toFixed(2);
        $("#showGoldGrossAmount").html(grossPrice);

        var vatPrice = parseFloat(grossPrice) * (parseFloat(vat) / 100);
        vatPrice = vatPrice.toFixed(2);
        $("#showGoldVatPrice").html(vatPrice);

        var netPrice = parseFloat(vatPrice) + parseFloat(grossPrice);
        netPrice = netPrice.toFixed(2);
        $("#showGoldNetPrice").html(netPrice);

        var perGramPrice = parseFloat(netPrice) / parseFloat(ounceToGram);
        perGramPrice = perGramPrice.toFixed(2);
        $("#showGoldPerGramPrice").html(perGramPrice);

        $.ajax({
            type: 'POST',
            url: '{{ route('admin.pricing.saveGoldPerGramPrice') }}',
            data: {
                _token: '{{ csrf_token() }}',
                "perGramGoldPrice": $("#showGoldPerGramPrice").html()
            },
            dataType: 'text',
            success: function (data) {
                getPriceByMetalUnit();
            },
            error: function (data) {
                console.log(data);
            }
        });

    };

    var perPlatinumCalculation = function (price, add, vat) {

        var ounceToGram = 31.100.toFixed(3);

        price = parseFloat(price).toFixed(2);
        add = parseFloat(add).toFixed(2);
        vat = parseFloat(vat).toFixed(2);

        $("#showPlatinumOunceToGram").html(ounceToGram);
        $("#showPlatinumAddPercentage").html(add);
        $("#showPlatinumVatPercentage").html(vat);
        $("#showPlatinumPrice").html(price);

        var addPrice = parseFloat(price) * (parseFloat(add) / 100);
        addPrice = addPrice.toFixed(2);
        $("#showPlatinumAddPrice").html(addPrice);

        var grossPrice = parseFloat(price) + parseFloat(addPrice);
        grossPrice = grossPrice.toFixed(2);
        $("#showPlatinumGrossAmount").html(grossPrice);

        var vatPrice = parseFloat(grossPrice) * (parseFloat(vat) / 100);
        vatPrice = vatPrice.toFixed(2);
        $("#showPlatinumVatPrice").html(vatPrice);

        var netPrice = parseFloat(vatPrice) + parseFloat(grossPrice);
        netPrice = netPrice.toFixed(2);
        $("#showPlatinumNetPrice").html(netPrice);

        var perGramPrice = parseFloat(netPrice) / parseFloat(ounceToGram);
        perGramPrice = perGramPrice.toFixed(2);
        $("#showPlatinumPerGramPrice").html(perGramPrice);

        $.ajax({
            type: 'POST',
            url: '{{ route('admin.pricing.savePlatinumPerGramPrice') }}',
            data: {
                _token: '{{ csrf_token() }}',
                "perGramPlatinumPrice": $("#showPlatinumPerGramPrice").html()
            },
            dataType: 'text',
            success: function (data) {
                getPriceByMetalUnit();

            },
            error: function (data) {
                console.log(data);
            }
        });

    };

     var perSilverCalculation = function(price, add, vat) {

        var ounceToGram = 31.100.toFixed(3);

        price = parseFloat(price).toFixed(2);
        add = parseFloat(add).toFixed(2);
        vat = parseFloat(vat).toFixed(2);

        $("#showSilverOunceToGram").html(ounceToGram);
        $("#showSilverAddPercentage").html(add);
        $("#showSilverVatPercentage").html(vat);
        $("#showSilverPrice").html(price);

        var addPrice = parseFloat(price) * (parseFloat(add) / 100);
        addPrice = addPrice.toFixed(2);
        $("#showSilverAddPrice").html(addPrice);

        var grossPrice = parseFloat(price) + parseFloat(addPrice);
        grossPrice = grossPrice.toFixed(2);
        $("#showSilverGrossAmount").html(grossPrice);

        var vatPrice = parseFloat(grossPrice) * (parseFloat(vat) / 100);
        vatPrice = vatPrice.toFixed(2);
        $("#showSilverVatPrice").html(vatPrice);

        var netPrice = parseFloat(vatPrice) + parseFloat(grossPrice);
        netPrice = netPrice.toFixed(2);
        $("#showSilverNetPrice").html(netPrice);

        var perGramPrice = parseFloat(netPrice) / parseFloat(ounceToGram);
        perGramPrice = perGramPrice.toFixed(2);
        $("#showSilverPerGramPrice").html(perGramPrice);

        $.ajax({
            type: 'POST',
            url: '{{ route('admin.pricing.saveSilverPerGramPrice') }}',
            data: {
                _token: '{{ csrf_token() }}',
                "perGramSilverPrice": $("#showSilverPerGramPrice").html()
            },
            dataType: 'text',
            success: function (data) {
                getPriceByMetalUnit();
            },
            error: function (data) {
                console.log(data);
            }
        });

    };

    var calculategold = function () {

        if($("#goldPrice").val()=='')
        {
            swal({
                text: 'Gold Price Required.!',
                type: 'error',
                icon: "warning",
                timer: 2000,
                button: false
            }).then(
                function () {},
                // handling the promise rejection
                function (dismiss) {
                    if (dismiss === 'timer') {
                    }
                }
            )
        }
        else if ($("#goldAdd").val()=='')
            {
                swal({
                    text: 'Gold Add Required.!',
                    type: 'error',
                    icon: "warning",
                    timer: 2000,
                    button: false
                }).then(
                    function () {},
                    // handling the promise rejection
                    function (dismiss) {
                        if (dismiss === 'timer') {
                        }
                    }
                )
            }
        else if ($("#goldVat").val()=='')
            {
                swal({
                    text: 'Gold Vat Required.!',
                    type: 'error',
                    icon: "warning",
                    timer: 2000,
                    button: false
                }).then(
                    function () {},
                    // handling the promise rejection
                    function (dismiss) {
                        if (dismiss === 'timer') {
                        }
                    }
                )

            }
        else
        {
            $.ajax({
                type: 'POST',
                url: "{{ route('admin.pricing.saveGoldData') }}",
                data: {
                    _token: '{{ csrf_token() }}',
                    "goldPrice": $("#goldPrice").val(),
                    "goldAdd": $("#goldAdd").val(),
                    "goldVat": $("#goldVat").val()
                },
                dataType: 'text',
                success: function (response) {
                    perGramGoldCalculation($("#goldPrice").val(), $("#goldAdd").val(), $("#goldVat").val());

                },
                error: function (response) {
                    console.log(response);
                }
            });
        }


    };

    var calculateplatinum = function () {

        if($("#platinumPrice").val()=='')
        {
            swal({
                text: 'Platinum Price Required.!',
                type: 'error',
                icon: "warning",
                timer: 2000,
                button: false
            }).then(
                function () {},
                // handling the promise rejection
                function (dismiss) {
                    if (dismiss === 'timer') {
                    }
                }
            )
        }
        else if ($("#platinumAdd").val()=='')
        {
            swal({
                text: 'Platinum Add Required.!',
                type: 'error',
                icon: "warning",
                timer: 2000,
                button: false
            }).then(
                function () {},
                // handling the promise rejection
                function (dismiss) {
                    if (dismiss === 'timer') {
                    }
                }
            )
        }
        else if ($("#platinumVat").val()=='')
        {
            swal({
                text: 'Platinum Vat Required.!',
                type: 'error',
                icon: "warning",
                timer: 2000,
                button: false
            }).then(
                function () {},
                // handling the promise rejection
                function (dismiss) {
                    if (dismiss === 'timer') {
                    }
                }
            )

        }
        else
        {
            $.ajax({
                type: 'POST',
                url: "{{ route('admin.pricing.savePlatinumData') }}",
                data: {
                    _token: '{{ csrf_token() }}',
                    "platinumPrice": $("#platinumPrice").val(),
                    "platinumAdd": $("#platinumAdd").val(),
                    "platinumVat": $("#platinumVat").val()
                },
                dataType: 'text',
                success: function (response) {
                    perPlatinumCalculation($("#platinumPrice").val(), $("#platinumAdd").val(), $("#platinumVat").val());

                },
                error: function (response) {
                    console.log(response);
                }
            });
        }

    };

    var calculatesilver = function () {

        if($("#silverPrice").val()=='')
        {
            swal({
                text: 'Silver Price Required.!',
                type: 'error',
                icon: "warning",
                timer: 2000,
                button: false
            }).then(
                function () {},
                // handling the promise rejection
                function (dismiss) {
                    if (dismiss === 'timer') {
                    }
                }
            )
        }
        else if ($("#silverAdd").val()=='')
        {
            swal({
                text: 'Silver Add Required.!',
                type: 'error',
                icon: "warning",
                timer: 2000,
                button: false
            }).then(
                function () {},
                // handling the promise rejection
                function (dismiss) {
                    if (dismiss === 'timer') {
                    }
                }
            )
        }
        else if ($("#silverVat").val()=='')
        {
            swal({
                text: 'Silver Vat Required.!',
                type: 'error',
                icon: "warning",
                timer: 2000,
                button: false
            }).then(
                function () {},
                // handling the promise rejection
                function (dismiss) {
                    if (dismiss === 'timer') {
                    }
                }
            )

        }
        else
        {
            $.ajax({
                type: 'POST',
                url: "{{ route('admin.pricing.saveSilverData') }}",
                data: {
                    _token: '{{ csrf_token() }}',
                    "silverPrice": $("#silverPrice").val(),
                    "silverAdd": $("#silverAdd").val(),
                    "silverVat": $("#silverVat").val()
                },
                dataType: 'text',
                success: function (response) {
                    perSilverCalculation($("#silverPrice").val(), $("#silverAdd").val(), $("#silverVat").val());

                },
                error: function (response) {
                    console.log(response);
                }
            });
        }

    };

    var getGoldPriceByApi=function ()
    {
        $.ajax({
            type: 'POST',
            url: "{{ route('admin.pricing.getGoldPriceByApi') }}",
            data: {
                _token: '{{ csrf_token() }}'
            },
            dataType: 'text',
            success: function (response) {

                if (response > 1) {
                    $("#goldPrice").val(response);
                    perGramGoldCalculation(response, $("#goldAdd").val(), $("#goldVat").val())
                }

            },
            error: function (data) {
                console.log(data);
            }
        });

    };

    var getSilverPriceByApi=function ()
    {
        $.ajax({
            type: 'POST',
            url: "{{ route('admin.pricing.getSilverPriceByApi') }}",
            data: {
                _token: '{{ csrf_token() }}'
            },
            dataType: 'text',
            success: function (response) {

                if (response > 1) {
                    $("#silverPrice").val(response);
                    perGramGoldCalculation(response, $("#silverAdd").val(), $("#silverVat").val())
                }

            },
            error: function (data) {
                console.log(data);
            }
        });

    };

    var getPriceByMetalUnit =function ()
    {
        $.ajax({
            type: 'POST',
            url: "{{ route('admin.pricing.getPriceByMetalUnit') }}",
            data: {
                _token: '{{ csrf_token() }}'
            },
            dataType: 'json',
            success: function (response) {

                var metalPrices = response.metalPrices;
                var metalUnit = response.metalUnit;

                var result = "";
                for (var i = 0; i < metalUnit.length; i++) {

                    var perGramPrice = 0;
                    var rate = 0 ;
                    var metalGramUnit =parseFloat(metalUnit[i].unit);

                    if(metalUnit[i].metal_type == "Gold"){
                        perGramPrice=parseFloat(metalPrices.perGramGoldPrice);
                        rate = (perGramPrice/24)*metalGramUnit;
                    }else if(metalUnit[i].metal_type == "Silver"){
                        perGramPrice=parseFloat(metalPrices.perGramSilverPrice);
                        rate = perGramPrice;
                    }else if(metalUnit[i].metal_type == "Platinum"){
                        perGramPrice=parseFloat(metalPrices.perGramPlatinumPrice);
                        rate = perGramPrice;
                    }

                    rate = rate.toFixed(2);

                    var casting = parseFloat(rate) + parseFloat(metalUnit[i].per_gram) + parseFloat(metalUnit[i].labour_charges);
                    casting = parseFloat(casting).toFixed(2);

                    result += '<tr>'
                   +'<td>'+metalUnit[i].nameKey+'</td>'
                    +'<td>'+metalUnit[i].name+'</td>'
                    +'<td>'+rate+'</td>'
                    +'<td>'+Math.round(rate)+'</td>'
                    +'<td>£&nbsp;'+metalUnit[i].per_gram+'</td>'
                    +'<td>£&nbsp;'+metalUnit[i].labour_charges+'</td>'
                   + '<td>'+casting+'</td>'
                    +'<td>'+Math.round(casting)+'</td>'
                +'</tr>';
                }

                $("#showPricebyUnit").html(result);

            },
            error: function (data) {
                console.log(data);
            }
        });
    }

</script>
