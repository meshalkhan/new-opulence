@inject('request', 'Illuminate\Http\Request')
@extends('layouts.admin')

@section('content')
    <section class="content-header" style="height: 50px;margin-left:30px ; ">

        @if($message = Session::get('success'))
            <div class="toast alert-info" style="padding: 3px;text-align: center;">{{ Session::get('success') }}</div>
        @endif

    </section>
{{--@if($message = Session::get('success'))--}}
{{--   <!--  <div class="alert alert-info">{{ Session::get('success') }}</div> -->--}}
{{--     <div class="card-body">--}}
{{--            <div class="alert alert-success alert-dismissible">--}}
{{--                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>--}}
{{--                  <h5><i class="icon fas fa-check"></i> Alert!</h5>--}}
{{--                 {{ Session::get('success') }}--}}
{{--            </div>--}}
{{--    </div>--}}
{{--@endif--}}
{{--<!-- Content Header (Page header) -->--}}
{{--<section class="content-header">--}}
{{--    <div class="container-fluid">--}}
{{--    </div><!-- /.container-fluid -->--}}
{{--</section>--}}

<!-- Main content -->
<section class="content">
   <div class="card card-primary">
            <div class="card-header">
              <h3 class="card-title">Permissions Management</h3>
              <div class="card-tools">
                @if(App\Helpers\PermissionHelper::has_permission('permissions_create'))
                    <a href="{{route('admin.permissions.create')}}" class="btn btn-danger btn-sm pull-right">New Permission</a>
                @endif
                </div>
            </div>
            <!-- /.card-header -->
            <div class="card-body" style="overflow-x:auto;font-size: 15px;">
              <table id="example1" class="table table-bordered table-striped {{ count($Permissions) > 0 ? 'datatable' : '' }}">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Title</th>
                    <th>Name</th>
                    <th>Create At</th>
                    <th>Actions</th>
                </tr>
                </thead>
                <tbody>
                    @if (count($Permissions) > 0)
                        @foreach ($Permissions as $Permission)
                            <tr data-entry-id="{{ $Permission->id }}">
                                <td>{{ $Permission->id }}</td>
                                <td>{{ $Permission->title }}</td>
                                <td>{{ $Permission->name }}</td>
                                <td>{{ $Permission->created_at }}</td>
                                <td>
                                    @if($Permission->status && App\Helpers\PermissionHelper::has_permission('permissions_inactive'))
                                        {!! Form::open(array(
                                                'style' => 'display: inline-block;',
                                                'method' => 'PATCH',
                                                'onsubmit' => "return confirm('Are you sure to inactivate this record?');",
                                                'route' => ['admin.permissions.inactive', $Permission->id])) !!}
                                        {!! Form::submit('Inactivate', array('class' => 'btn btn-xs btn-warning', 'style'=>'font-size:12px;')) !!}
                                        {!! Form::close() !!}
                                    @elseif(!$Permission->status && App\Helpers\PermissionHelper::has_permission('permissions_active'))
                                        {!! Form::open(array(
                                                'style' => 'display: inline-block;',
                                                'method' => 'PATCH',
                                                'onsubmit' => "return confirm('Are you sure to activate this record?');",
                                                'route' => ['admin.permissions.active', $Permission->id])) !!}
                                        {!! Form::submit('Activate', array('class' => 'btn btn-xs btn-primary','style'=>'font-size:12px;')) !!}
                                        {!! Form::close() !!}
                                    @endif

                                    @if(App\Helpers\PermissionHelper::has_permission('permissions_edit'))
                                            <a href="{{ route('admin.permissions.edit',[$Permission->id]) }}" class="btn btn-xm btn-info"><i class="fas fa-edit"></i></a>
                                    @endif

                                    @if(App\Helpers\PermissionHelper::has_permission('permissions_destroy'))
                                        {!! Form::open(array(
                                            'style' => 'display: inline-block;',
                                            'method' => 'DELETE',
                                            'onsubmit' => "return confirm('".trans("global.app_are_you_sure")."');",
                                            'route' => ['admin.permissions.destroy', $Permission->id])) !!}
                                        {!! Form::submit('del', array('class' => 'btn btn-xs btn-danger')) !!}
                                        {!! Form::close() !!}
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="5">@lang('global.app_no_entries_in_table')</td>
                        </tr>
                    @endif
                </tbody>
              </table>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
</section>
    <!-- /.content -->

@endsection
