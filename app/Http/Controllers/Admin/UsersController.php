<?php

namespace App\Http\Controllers\Admin;

use App\Models\Admin\TaxPayer;
use App\User;
use Spatie\Permission\Models\Role;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\StoreUsersRequest;
use App\Http\Requests\Admin\UpdateUsersRequest;
use App\Models\Admin\RolePermission;
use App\Helpers\PermissionHelper as Per;
use Session;
use Auth;
use DB;
use App\Helpers\GeneralHelper;


class UsersController extends Controller
{
    /**
     * Display a listing of User.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        if (! Per::has_permission('users_manage')) {
            return abort(401);
        }
        $users = User::get();
        return view('admin.users.index', compact('users'));
    }

    /**
     * Show the form for creating new User.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */


    public function create()
    {
        if (! Per::has_permission('users_manage')) {
            return abort(401);
        }
        $roles = Role::where('name','<>', 'administrator')->get()->pluck('name', 'name');

        $roles->prepend('Select a Role', '');
        return view('admin.users.create', compact('roles'));
    }

    /**
     * Store a newly created User in storage.
     *
     * @param  \App\Http\Requests\StoreUsersRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (! Per::has_permission('users_manage'))
        {
            return abort(401);
        }
        $data = $request->all();

        $email = $request->email;
        $emailcheck = User::where('email',$email)->get();
        if(count($emailcheck)>=1)
        {
            session()->flash('success', 'Email Already Exists.');
            return redirect()->route('admin.users.create');
        }
        else
            {
            $roles = $request->input('roles') ? $request->input('roles') : [];
            $role_name = $roles[0];
            $role_id = Role::where('name', $role_name)->first()->id;

            $perm = RolePermission::where('id', $role_id)->pluck('permissions')->first();

            $data['role_id'] = $role_id;
            $data['permissions'] = $perm;
            $data['portal_id'] = '100';
            $data['password'] = bcrypt($data['password']);
            $data['created_by'] = Auth::user()->id;
            $data['updated_by'] = Auth::user()->id;

            $user = User::create($data);

            session()->flash('success', 'Record has been created successfully.');
            return redirect()->route('admin.users.index');
        }
    }


    /**
     * Show the form for editing User.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (! Per::has_permission('users_manage')) {
            return abort(401);
        }
        $roles = Role::where('name','<>', 'administrator')->get()->pluck('name', 'name');

        $user = User::findOrFail($id);
        return view('admin.users.edit', compact('user', 'roles'));
    }

    /**
     * Update User in storage.
     *
     * @param  \App\Http\Requests\UpdateUsersRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //dd('meshzal');
        if (! Per::has_permission('users_manage')) {
            return abort(401);
        }

        $user = User::findOrFail($id);

        $data = $request->all();
        $email = $request->email;

        $cniccheck = User::where('email',$email)->where('id', '!=', $id)->get();
        if(count($cniccheck)>=1)
        {
            session()->flash('success', 'Email Already Exists.');
            return redirect()->route('admin.users.index');
        }
        else
        {
            $roles = $request->input('roles') ? $request->input('roles') : [];
            $role_name = $roles[0];
            $role_id = Role::where('name', $role_name)->first()->id;

            $perm = RolePermission::where('id', $role_id)->pluck('permissions')->first();
            $data['role_id'] = $role_id;
            $data['permissions'] = $perm;
            $datas['password'] = $request->password;
            //dd($data['password']);
            if (isset($datas['password']))
            {
                $data['password'] = bcrypt($datas['password']);
            }
            $data['updated_by'] = Auth::user()->id;

            //dd($data);

            $user->update($data);
            // $roles = $request->input('roles') ? $request->input('roles') : [];
            // $user->syncRoles($roles);
            Session::flash('success', 'Record has been updated successfully.');
            return redirect()->route('admin.users.index');
        }
    }


    /**
     * Remove User from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (! Per::has_permission('users_manage')) {
            return abort(401);
        }
        $user = User::findOrFail($id);

        $user->delete();
        Session::flash('success', 'Record has been deleted successfully.');
        return redirect()->route('admin.users.index');
    }

    /**
     * Delete all selected User at once.
     *
     * @param Request $request
     */
}
