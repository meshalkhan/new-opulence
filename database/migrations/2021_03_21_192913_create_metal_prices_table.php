<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMetalPricesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('metalprices', function (Blueprint $table) {
            $table->increments('id');
            $table->string('goldPrice')->nullable();
            $table->string('goldAdd')->nullable();
            $table->string('goldVat')->nullable();
            $table->string('perGramGoldPrice')->nullable();
            $table->string('silverPrice')->nullable();
            $table->string('silverAdd')->nullable();
            $table->string('silverVat')->nullable();
            $table->string('perGramSilverPrice')->nullable();
            $table->string('platinumPrice')->nullable();
            $table->string('platinumAdd')->nullable();
            $table->string('platinumVat')->nullable();
            $table->string('perGramPlatinumPrice')->nullable();
            $table->string('date')->nullable();
            $table->integer('status')->default(0);
            $table->integer('user_id')->unsigned();
            $table->unsignedInteger('created_by')->nullable();
            $table->unsignedInteger('updated_by')->nullable();
            $table->unsignedInteger('deleted_by')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('metal_prices');
    }
}
