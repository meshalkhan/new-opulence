
<script src="{{ asset('public/AdminLTE/plugins/jquery') }}/jquery.min.js"></script>
<script src="{{ asset('public/AdminLTE/plugins/bootstrap/js') }}/bootstrap.bundle.min.js"></script>
<script src="{{ asset('public/js') }}/parsley.min.js"></script>
<script src="{{ asset('public/AdminLTE/plugins/overlayScrollbars/js') }}/jquery.overlayScrollbars.min.js"></script>
<script src="{{ asset('public/AdminLTE/dist/js') }}/adminlte.js"></script>
<script src="{{ asset('public/AdminLTE/plugins/datatables') }}/jquery.dataTables.js"></script>
<script src="{{ asset('public/AdminLTE/plugins/datatables') }}/dataTables.bootstrap4.js"></script>
<script src="{{ asset('public/AdminLTE/plugins/fastclick') }}/fastclick.js"></script>
<script src="{{ asset('public/AdminLTE/plugins/inputmask') }}/jquery.inputmask.bundle.js"></script>
<script src="{{ url('public/adminlte/plugins/moment') }}/moment.min.js"></script>
<script src="{{ asset('public/AdminLTE/plugins/daterangepicker') }}/daterangepicker.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.js"></script>
<script src="{{ asset('public/AdminLTE/plugins/jquery-mousewheel') }}/jquery.mousewheel.js"></script>
<script src="{{ asset('public/AdminLTE/plugins/raphael') }}/raphael.min.js"></script>
<script src="{{ asset('public/AdminLTE/plugins/jquery-mapael') }}/jquery.mapael.min.js"></script>
<script src="{{ asset('public/AdminLTE/plugins/jquery-mapael') }}/maps/world_countries.min.js"></script>
<script src="{{ asset('public/AdminLTE/plugins/chart.js') }}/Chart.min.js"></script>
<script src="{{ asset('public/AdminLTE/dist/js') }}/demo.js"></script>

<script>
  jQuery(function () {

    jQuery("#example1").DataTable({
        "pageLength": 50
    });
    jQuery("#example3").DataTable({

    });
    jQuery('#example2').DataTable({
      "paging": true,
      "pageLength": 15,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false
    });
  });


</script>

@yield('jsscript')