<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Ticket extends Model
{
    use SoftDeletes;
    //
    protected $fillable = ['id',
        'cutomer_id',
        'barcode',
        'items',
        'createdDate',
        'endDate',
        'price',
        'status',
        'discount',
        'paidStatus',
        'paidDue',
        'user_id',
        'created_by',
        'updated_by', 'deleted_by'];
    protected $table = 'tickets';
}

