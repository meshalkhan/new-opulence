<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $this->call(RoleSeed::class);
        $this->call(PermissionSeed::class);
        $this->call(UserSeed::class);
        $this->call(PricingSeeder::class);
        $this->call(Process1Seeder::class);
        $this->call(Process2Seeder::class);
        $this->call(Process3Seeder::class);
        $this->call(Process4Seeder::class);
        $this->call(HeadingSeeder::class);
        $this->call(SubHeadingSeeder::class);
        $this->call(PaymentSeeder::class);
        $this->call(CustomerSeeder::class);
        $this->call(BillingSeeder::class);
        $this->call(TicketSeeder::class);
        $this->call(InvoiceSeeder::class);
            /*
             * Whatever happens with the world
             * Let this seeder at the last of seeders list for the admin
             *
             * */
        $this->call(AdminPermissionSeeder::class);
    }
}
