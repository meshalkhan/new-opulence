<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Process4 extends Model
{
    use SoftDeletes;
    //
    protected $fillable = ['id',
        'name',
        'nameKey',
        'user_id',
        'created_by',
        'updated_by', 'deleted_by'];
    protected $table = 'process4';
}

