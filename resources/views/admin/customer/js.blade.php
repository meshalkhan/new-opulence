<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/3/jquery.inputmask.bundle.js"></script>
<script type="text/javascript">

    $(".select2").select2({
        //placeholder: "Select a Name",
        allowClear: true
    });
</script>
<script type="text/javascript">

    $(document).ready(function ()
    {
        document.getElementById('add_customer').style.display = "block";
        document.getElementById('edit_customer').style.display = "none";
        $('#clearall').show();

        var table= $('#customer_datatables').DataTable({
            "dom": 'lBfrtip',
            "destroy": true,
            "buttons": [
                'copy', 'csv', 'excel', 'pdf', 'print'
            ],
            "pageLength":5,
            "processing": true,
            "serverSide": true,
            "ajax":{
                "type": "GET",
                "url": "{{ route('admin.customer.allcustomer',['archive']) }}'",
                "dataType": "json",
                "data":{ _token: "{{csrf_token()}}"}

            },
            "columnDefs": [{
                orderable: false,
                "targets": '_all',
                "createdCell": function (td, cellData, rowData, row, col) {
                    $(td).css('padding-top', '2px')
                    $(td).css('padding-left', '8px')
                    $(td).css('padding-bottom', '2px')
                    $(td).css('padding-right', '8px')
                }
            }],
            "columns": [
                {
                    "data": "id"
                },
                { "data": "nickName" },
                { "data": "firstName" },
                { "data": "lastName" },
                { "data": "companyName" },
                { "data": "vatNumber" },
                { "data": "email" },
                { "data": "phone" },
                { "data": "mobile" },
                { "data": "address" },
                { "data": "creditLimit" },
                {
                    "data": "options"
                }
            ],
            "order": [[1, 'asc']],
        });
    });

    var clearall = function ()
    {

        $('#save').attr('disabled', false);
        document.getElementById('add_customer').style.display = "block";
        $('#clearall').show();
        document.getElementById('edit_customer').style.display = "none";
        $('#customer_datatables').DataTable().ajax.reload();
        $('#nickName').val('');
        $('#firstName').val('');
        $('#lastName').val('');
        $('#companyName').val('');
        $('#vatNumber').val('');
        $('#email').val('');
        $('#phone').val('');
        $('#mobile').val('');
        $('#address').val('');
        $('#creditLimit').val('');
    };

    // Delete row

    var rowdelete = function (id,find)
    {
        if(find=='0')
        {
            var title='You will not be able to revert this !';

        }
        swal({
            title: "Are you sure?",
            text: title,
            icon: "warning",
            buttons: [
                'No, cancel it!',
                'Yes, I am sure!'
            ],
            dangerMode: true,
        }).then(function(isConfirm) {
            if (isConfirm) {

                $.ajax({
                    type: 'POST',
                    url: '{{ route('admin.customer.deleterow') }}',
                    data: {
                        _token: '{{ csrf_token() }}',
                        id: id,
                    },
                    success: function (response) {
                        swal({
                            title: 'Deleted!',
                            text: 'Record has been Deleted Successfully.',
                            icon: 'success',
                            timer: 2000,
                            button: false,
                        }).then(function() {
                        });
                        clearall();
                    }
                });
            } else {
                swal("Cancelled", "Your data is safe :)", "error");
            }
        });
    };

    // Edit Row
    var updaterow = function ()
    {
        var id = $('#update_id').val();
        var nickName=$('#nickName').val();
        var firstName=$('#firstName').val();
        var lastName=$('#lastName').val();
        var companyName=$('#companyName').val();
        var vatNumber=$('#vatNumber').val();
        var email=$('#email').val();
        var password=$('#password').val();
        var phone=$('#phone').val();
        var mobile=$('#mobile').val();
        var address=$('#address').val();
        var creditLimit=$('#creditLimit').val();

        $.ajax({
            type: 'POST',
            url: '{{ route("admin.customer.editrow") }}',
            data: {
                _token: '{{ csrf_token() }}',
                id: id,
                lastName: lastName,
                nickName: nickName,
                firstName: firstName,
                companyName: companyName,
                vatNumber: vatNumber,
                email: email,
                password: password,
                phone: phone,
                mobile: mobile,
                address: address,
                creditLimit: creditLimit
            },

            success: function (response)
            {
                if(response=='success')
                {
                    swal({
                        title: 'Data Updated',
                        type: 'success',
                        text: 'Record has been Updated Successfully',
                        icon: 'success',
                        timer: 2000,
                        button: false,
                    }).then(
                        function () {},
                        // handling the promise rejection
                        function (dismiss) {
                            if (dismiss === 'timer') {
                            }
                        }
                    )
                }
                else if(response=='repeat')
                {
                    var title='Email Already Exists.!';
                    swal("Type Again", title, "error");
                }
                else if(response=='null')
                {
                    var title='Some Fields Empty. Try Again!';
                    swal("Type Again", title, "error");
                }


                $('#customer_datatables').DataTable().ajax.reload();
            }
        });
    };

    var editrow = function (id) {
        $('#save').attr('disabled', true);

        $.ajax({
            type: 'POST',
            url: '{{ route('admin.customer.fetchdata') }}',
            data: {
                _token: '{{ csrf_token() }}',
                id: id,
            },

            success: function (response) {
                jQuery('html,body').animate({scrollTop:0},0);
                document.getElementById('add_customer').style.display = "none";
                document.getElementById('edit_customer').style.display = "block";
                document.getElementById('clearall').style.display = "block";
                $('#update_id').val(response[0].id);
                $('#nickName').val(response[0].nickName);
                $('#firstName').val(response[0].firstName);
                $('#lastName').val(response[0].lastName);
                $('#companyName').val(response[0].companyName);
                $('#vatNumber').val(response[0].vatNumber);
                $('#email').val(response[0].email);
                $('#password').val('');
                $('#phone').val(response[0].phone);
                $('#mobile').val(response[0].mobile);
                $('#address').val(response[0].address);
                $('#creditLimit').val(response[0].creditLimit);

            }
        });

    };

    var status_filter= function ()
    {
        $('#customer_datatables').DataTable().ajax.reload();
    }

</script>
