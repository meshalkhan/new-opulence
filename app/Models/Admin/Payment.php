<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Payment extends Model
{
    use SoftDeletes;
    //
    protected $fillable = ['id',
        'heading_id',
        'sub_heading_id',
        'reference',
        'rate',
        'quantity',
        'amount',
        'payment_option',
        'user_id',
        'created_by',
        'updated_by', 'deleted_by'];
    protected $table = 'payment';
}

