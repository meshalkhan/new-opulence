<?php


namespace App\Http\Controllers\Admin;

use App\Helpers\PermissionHelper as Per;
use App\Models\Admin\Customer;
use App\Models\Admin\Heading;
use App\Models\Admin\Process1;
use App\Models\Admin\Process2;
use App\Models\Admin\Process3;
use App\Models\Admin\Process4;
use App\Models\Admin\Ticket;
use App\Models\Admin\SubHeading;
use App\User;
use Illuminate\Http\Request;
use DB;
use Auth;
use Form;

class TicketController
{
    public function index()
    {
        if (! Per::has_permission('ticket_details')) {
            return redirect()->route('home');
        }

        return view('admin.ticket.index');
    }

    public function add()
    {
        if (! Per::has_permission('ticket_create')) {
            return redirect()->route('home');
        }

        $customers=Customer::select(DB::raw('CONCAT(firstName,\' \',lastName ,\' - \',email) AS full_name'))->get()->pluck('full_name','id');
        $processes1=Process1::orderBy('name')->get();
        $processes2=Process2::get();
        $processes3=Process3::get();
        $processes4=Process4::get();
        return view('admin.ticket.create',compact('customers','processes1','processes2','processes3','processes4'));
    }

    public function create(Request $request)
    {
        if (! Per::has_permission('ticket_create')) {
            return redirect()->route('home');
        }
        $data = $request->all();
        $current_date_time = \Carbon\Carbon::now();
        $current_date_time->timezone = config('global.time_zone');
        $data['created_by'] = Auth::user()->id;
        $data['created_at'] = $current_date_time;

        $email = $request->email;
        $emailcheck = Ticket::where('email',$email)->get();
        if(count($emailcheck)>=1)
        {
            session()->flash('success', 'Email Already Exists.');
            return redirect()->route('admin.users.create');
        }
        else
        {
            $roles = $request->input('roles') ? $request->input('roles') : [];
            $role_name = $roles[0];
            $role_id = Role::where('name', $role_name)->first()->id;

            $perm = RolePermission::where('id', $role_id)->pluck('permissions')->first();

            $datau['role_id'] = $role_id;
            $datau['permissions'] = $perm;
            $datau['portal_id'] = '100';
            $datau['password'] = bcrypt($data['password']);
            $datau['created_by'] = Auth::user()->id;
            $datau['updated_by'] = Auth::user()->id;

//            $user = User::create($datau)->id;
            Ticket::create($data);
            session()->flash('success', 'Record has been created successfully.');
            return redirect()->route('admin.ticket.index');
        }

    }

    public function fetchdata(Request $request)
    {
        $data = Ticket::where('id',$request['id'])->get();
        return response()->json($data);
    }

    public function editrow(Request $request)
    {
        if (! Per::has_permission('ticket_edit')) {
            return redirect()->route('home');
        }
        $data = $request->all();
        $id = $request['id'];
        $current_date_time = \Carbon\Carbon::now();
        $current_date_time->timezone = config('global.time_zone');
        $data['updated_by'] = Auth::user()->id;
        $data['updated_at'] = $current_date_time;

        if($data['nickName']=='' ||$data['firstName']=='' ||$data['lastName']=='' ||$data['companyName']=='' ||
            $data['vatNumber']=='' ||$data['email']=='' ||$data['password']=='' ||$data['phone']=='' ||
            $data['mobile']=='' ||$data['address']==''||$data['creditLimit']=='' )
        {
            $response='null';
        }
        else
        {
            $email = $request->email;
            $emailcheck = Ticket::where('email',$email)->where('id','!=',$id)->get();
            if(count($emailcheck)>=1)
            {
                $response='repeat';
            }
            else
            {
                $datas['updated_by'] = Auth::user()->id;
                Ticket::where('id', $id)->update([
                    'nickName' => $request['nickName'],
                    'lastName' => $request['lastName'],
                    'firstName' => $request['firstName'],
                    'companyName' => $request['companyName'],
                    'vatNumber' => $request['vatNumber'],
                    'email' => $request['email'],
                    'password' => $request['password'],
                    'phone' => $request['phone'],
                    'mobile' => $request['mobile'],
                    'address' => $request['address'],
                    'creditLimit' => $request['creditLimit']
                ]);
                $response = 'success';
            }

        }
        return response()->json($response);

    }

    public function deleterow(Request $request)
    {
        if (! Per::has_permission('ticket_delete')) {
            return redirect()->route('home');
        }

        $id = $request->id;
        $tickets = Ticket::where('id',$id)->forceDelete();
    }

    public function allticket(Request $request,$id)
    {


        $columns = array(
            0 =>'barcode',
            1 =>'customer_id',
            2 =>'image',
            3 =>'total_amount',
            4 =>'status',
            5 => 'paidStatus',
            6 =>'paidDue',
            7 =>'complete_job',
            8 =>'pay_now',
            9 => 'options',
        );

        $totalData = Ticket::count();

        $totalFiltered = $totalData;

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');

        if(empty($request->input('search.value')))
        {
            $lists = Ticket::offset($start)
                ->limit($limit)
                ->orderBy('id','asc')
                ->get();
        }
        else
        {
            $search = $request->input('search.value');
            $lists =  Ticket::where('barcode', 'LIKE',"%{$search}%")
                ->offset($start)
                ->limit($limit)
                ->orderBy($order,$dir)
                ->get();

            $totalFiltered = Ticket::where('barcode', 'LIKE',"%{$search}%")
                ->count();
        }

        $data = array();

        $i=1;
        if(!empty($lists)) {
            foreach ($lists as $list)
            {
//                $nestedData['id'] = "&nbsp&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;".$i;
                $nestedData['barcode'] = $list->barcode;

                $customer_id=Customer::where('userID', $list->customer_id)->first();
                $nestedData['customer_id'] = $customer_id['firstName'].' '.$customer_id['lastName'];
                $nestedData['actions'] = '';
                $nestedData['image'] = '';
                $nestedData['total_amount'] = $list->price-$list->discount;
                if($list->status == 1)
                {
                    $nestedData['status']= '<p class="text-success">Close</p>';
                    $nestedData['complete_job'] ='<button class="btn btn-sm  btn-secondary">In Complete Job</button>';
                }
                else if($list->status == 0)
                {
                        $nestedData['status']= '<p class="text-danger">Open</p>';
                    $nestedData['complete_job']='<button class="btn btn-sm  btn-success">Complete Job</button>';
                }
                if($list->paidStatus == 1)
                {
                    $nestedData['paidStatus']= '<p class="text-success">Close</p>';
                    $nestedData['paidDue']= $list->paidDue;
                    $nestedData['pay_now']='<button class="btn btn-sm  btn-secondary">Un Pay</button>';
                }
                else if($list->paidStatus == 0)
                {
                        $nestedData['paidStatus']= '<p class="text-danger">Open</p>';
                    $nestedData['paidDue']= '';
                    $nestedData['pay_now']='<button class="btn btn-sm  btn-success">Pay Now</button>';
                }

                $options="";
                if (Per::has_permission('ticket_delete')) {

                    $options .= "<a class='btn btn-sm btn-danger' style='cursor:pointer;color:white;margin-left:2px;' onclick='rowdelete(\"" . $list->id . "\",\"0\")' ><i class='fas fa-trash-alt' ></i></button>";

                }
                if (Per::has_permission('ticket_edit')) {
                    $options .= "<a class='btn btn-sm btn-warning' role='button' style='cursor:pointer;margin-left:2px;' onclick='editrow(\"" . $list->id . "\")' ><i class='fas fa-edit' ></i></button>";
                }

                $nestedData['options'] = $options;
                $data[] = $nestedData;
                $i++;

            }
        }

        $json_data = array(
            "draw"            => intval($request->input('draw')),
            "recordsTotal"    => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data"            => $data
        );
        echo json_encode($json_data);

    }
}
