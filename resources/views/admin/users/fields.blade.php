<div class="form-group row">
    {!! Form::label('name', 'Name*', ['class' => 'control-label col-sm-2']) !!}
    <div class="col-sm-10">
        {!! Form::text('name', old('name'), ['class' => 'form-control', 'placeholder' => '', 'data-parsley-required' => 'true','maxlength' => 30]) !!}
        <p class="help-block"></p>
        @if($errors->has('name'))
            <p class="help-block">
                {{ $errors->first('name') }}
            </p>
        @endif
    </div>
</div>


 <div class="form-group row">
    {!! Form::label('email', 'Email*', ['class' => 'control-label col-sm-2']) !!}
    <div class="col-sm-10">
        {!! Form::email('email', old('email'), ['class' => 'form-control', 'placeholder' => '', 'data-parsley-required' => 'true','maxlength' => 50]) !!}
        <p class="help-block"></p>
        @if($errors->has('email'))
            <p class="help-block">
                {{ $errors->first('email') }}
            </p>
        @endif
    </div>
</div>

<div class="form-group row">
    {!! Form::label('password', 'Password*', ['class' => 'control-label col-sm-2']) !!}
    <div class="col-sm-10">
        {!! Form::text('password', '', ['class' => 'form-control', 'placeholder' => '','maxlength' => 50]) !!}
        <p class="help-block"></p>
        @if($errors->has('password'))
            <p class="help-block">
                {{ $errors->first('password') }}
            </p>
        @endif
    </div>
</div>

<div class="form-group row">
    {!! Form::label('roles', 'Roles*', ['class' => 'control-label col-sm-2']) !!}
    <div class="col-sm-10">
        {!! Form::select('roles[]', $roles, old('roles'), ['class' => 'form-control select2', 'placeholder' => 'Select Role', 'required' => 'true','maxlength' => 40]) !!}
        <p class="help-block"></p>
        @if($errors->has('roles'))
            <p class="help-block">
                {{ $errors->first('roles') }}
            </p>
        @endif
    </div>
</div>

<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>

<script type="text/javascript">
    $("#nameid6").select2({
        placeholder: "Select a Name",
        allowClear: true
    });
</script>